#pragma once
#include "PrefixParameter.h"
#include "systemc.h"

SC_MODULE(DEMUX)
{
	//input
	sc_in_clk clk;//clock
	sc_in<bool> reset;//reset signal
	sc_in<bool> enable_in;//enable DEMUX
	sc_in<bool> ready_in;//ready DEMUX - from VLD
	sc_in<bool> muxing_mode;//mode - 1 for muxing; only 1 working
	sc_in<int> fullness_in[UNITS_PER_GROUP];//fullness data - from VLD
	sc_in<unsigned char *> buf_input;//data - from rate buffer; GIve as file in testbench


	//output
	sc_out<sc_lv<1>> unit_out[UNITS_PER_GROUP];//bit stream - to VLD
	sc_out<bool> VLD_enable_out;//enable VLD - to VLD

	//local variables
		//state
	enum State
	{
		IDLE,
		RUN,//get input;
		INIT,
		TRANSMIT,
		WAIT
	};
	State state;
	bool transmit_done;
		//bitstream
	//unsigned char buf_in[CHUCK_SIZE*SLICE_HEIGHT];
	unsigned char buf_in[4512];//for test
	int mux_bit_counter;
	int transmit_iterator;
	int out[UNITS_PER_GROUP][48];
	int out_amount[UNITS_PER_GROUP];
	bool calculate_done;

	//functions
	void StateMachine();
	void Run();
	void Initialize();
	void Transmit();
		//fifo_out
	void GetBits(int unit, int size, unsigned char *buf, int *bit_count);
	int getbits(int size, unsigned char *buf, int *bit_count, int sign_extend);
	void AddBits(int cpnt, int value, int bit_num);
	void ProcessGroupDec(unsigned char *buf);
	void WriteEntryFromBitstream(unsigned char *buf);

	SC_CTOR(DEMUX)
	{
		SC_METHOD(StateMachine);
			dont_initialize();
			sensitive << clk.pos();
		SC_METHOD(Transmit);
			dont_initialize();
			sensitive << clk.pos();
	}


	//for clock counting
	int cl = 0;
};