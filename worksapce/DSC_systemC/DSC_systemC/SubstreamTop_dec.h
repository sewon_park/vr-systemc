#pragma once
#include "VLD.h"
#include "DEMUX.h"
#include "systemc.h"

SC_MODULE(SUBSTREAM_TOP_dec)
{
	//signal global
	sc_in_clk clk;				//clock
	sc_in<bool> reset;			//reset signal

	DEMUX *demux;
	//input file
	sc_in<unsigned char*> buf;	//input buffer
	//in controller
	sc_in<bool> enable_demux;	//data - from rate buffer; GIve as file in testbench
	sc_in<bool> muxing_mode;	//mode - 1 for muxing; only 1 working


	VLD *vld;
	//in RC
	sc_in<sc_uint<4>> qp;		//quantization parameter - from RC
	//in ICH
	sc_in<bool> ich_ready;		//ICH is ready to next value
	//out P_MODE
	sc_out<sc_uint<26>> residual[3];// residue - to Predictor module
	//out ICH
	sc_out<sc_uint<5>> ICH_index[3];// index - to ICH module
	sc_out<bool> mode_sel;			// 0:P-mode, 1:ICH_mode - to ICH module
	//out RC
	sc_out<int> Y_fullness;			//fullness information - to RC
	sc_out<int> Co_fullness;
	sc_out<int> Cg_fullness;


	//signal VLD - DEMUX
	sc_signal<bool> DEMUX_ready;

	//signal DEMUX - VLD
	sc_signal<bool> enable_vld;
	sc_signal<sc_lv<1>> Y_out;
	sc_signal<sc_lv<1>> Co_out;
	sc_signal<sc_lv<1>> Cg_out;


	SC_CTOR(SUBSTREAM_TOP_dec)
	{
		demux = new DEMUX("DEMUX");
		demux->clk(clk);
		demux->reset(reset);
		demux->enable_in(enable_demux);
		demux->ready_in(DEMUX_ready);
		demux->muxing_mode(muxing_mode);
		demux->fullness_in[0](Y_fullness);
		demux->fullness_in[1](Co_fullness);
		demux->fullness_in[2](Cg_fullness);
		demux->buf_input(buf);
		demux->unit_out[0](Y_out);
		demux->unit_out[1](Co_out);
		demux->unit_out[2](Cg_out);
		demux->VLD_enable_out(enable_vld);

		vld = new VLD("VLD");
		vld->clk(clk);
		vld->reset(reset);
		vld->enable(enable_vld);
		vld->qp_in(qp);
		vld->ich_done_in(ich_ready);
		vld->unit_in[0](Y_out);
		vld->unit_in[1](Co_out);
		vld->unit_in[2](Cg_out);
		vld->P_mod_out[0](residual[0]);
		vld->P_mod_out[1](residual[1]);
		vld->P_mod_out[2](residual[2]);
		vld->ICH_mod_out[0](ICH_index[0]);
		vld->ICH_mod_out[1](ICH_index[1]);
		vld->ICH_mod_out[2](ICH_index[2]);
		vld->fullness_out[0](Y_fullness);
		vld->fullness_out[1](Co_fullness);
		vld->fullness_out[2](Cg_fullness);
		vld->mode_select_out(mode_sel);
		vld->DEMUX_ready_out(DEMUX_ready);
	}
};