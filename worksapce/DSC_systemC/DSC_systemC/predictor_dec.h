#pragma once
#include "DefaultHeader.h"

SC_MODULE(Predictor_dec)
{
	//DECODER PARAMETER
	typedef enum { PT_MAP = 0, PT_LEFT, PT_BLOCK } PRED_TYPE;

	sc_in <sc_int<26>> residue[3]; //input residue from VLC
	sc_in <sc_uint<4>> qp;
	sc_out <sc_int<26>> recon_x_out[3]; //output reconstruct data to Line buffer
	sc_out <sc_int<26>> prevLine_out[SLICE_WIDTH]; // output prevLine to ICH

	int cpnt;
	int hPos;
	int vPos;
	int masterQp;
	int prevLinePred[3];
	int bpCount;
	int lastEdgeCount;
	int lastErr[3][3][10];
	int predErr[3][10];
	int quantizedResidualMid[3][3];
	int quantizedResidual[3][3];
	int edgeDetected;
	int midpointSelected[3];
	int midpointRecon[3][3];
	int range[3];
	int sampModCnt;
	int done;
	int orgiLine[3][SLICE_WIDTH + PADDING_LEFT + PADDING_RIGHT];
	int currLine[3][SLICE_WIDTH + PADDING_LEFT + PADDING_RIGHT];
	int prevLine[3][SLICE_WIDTH + PADDING_LEFT + PADDING_RIGHT];
	int cpntBitDepth[3] = { 8,9,9 };
	int bits_per_component = 8;
	int log_err_p_mode;
	int maxError[3];
	int maxMidError[3];
	int useMidpoint[3];
	int prevMasterQp;
	sc_int <8> Y;
	sc_int <9> Co;
	sc_int <9> Cg;

	int quantTableChroma[16] = { 0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 8, 8 };
	int quantTableLuma[16] = { 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 7 };
	const int QuantDivisor[16] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };
	const int QuantOffset[16] = { 0, 0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047 };

	//STATE PARAMETER
	sc_in_clk clk;
	sc_in<bool> reset;
	sc_in <bool> rc_update; // next group ready, signal from Line buffer 

	sc_signal<int> state;
	sc_signal<int> next_state;
	sc_signal <bool> CALCUL_done;
	sc_signal <bool> take; // dummy signal

						   //STATE FUNCTION
	void statemachine();
	void behavstate();
	void CALCUL_state();
	void CATCH();

	//DECODER FUNCTION
	//simple culculation functions
	void getInput(); // get input residue
	int MapQpToQlevel(int qp, int cpnt); //find qlevel using qp
	int QuantizeResidual(int e, int qlevel); //quantize a residual
	int ceil_log2(int val);//simple log calculation
	int FindMidpoint(int cpnt, int qlevel); //returns midpoint prediction predictor
	int SampToLineBuf(int x, int cpnt); //reduce the bits required for a previous reconstructed line sample (currently via rounding)
	int FindResidualSize(int eq);//finding residualsize, used in VLCUnit fuction

								 //decision making functions
	int SamplePredict(int prevLine[], int currLine[], int hPos, int predType, int qLevel, int cpnt);//prediction calculation between BT and MMAP(PT_LEFT used for first Line)
	void BlockPredSearch(int cpnt, int currLine[3][SLICE_WIDTH + PADDING_LEFT + PADDING_RIGHT], int hPos, int recon_x);//decide which prediction to use 
	void UpdateMidpoint(int currLine[3][SLICE_WIDTH + PADDING_LEFT + PADDING_RIGHT]); //update reconstructed pixels if midpoint prediction was selected
	int UsingMidpoint(int cpnt);//returns 1 if midpoint prediction will be used for the specified component
	void VLDUnit(int cpnt);//decide whether to use midpoint prediction

	void init();//initialize function

	void DSC_Algorithm();//main decoder function

	SC_CTOR(Predictor_dec)
	{
		SC_METHOD(statemachine);
		sensitive << clk.pos();

		SC_METHOD(behavstate);
		sensitive << rc_update;
		sensitive << state;

		SC_METHOD(CALCUL_state);
		sensitive << clk.pos();

		SC_METHOD(CATCH);
		sensitive << clk.pos();
	}
};

