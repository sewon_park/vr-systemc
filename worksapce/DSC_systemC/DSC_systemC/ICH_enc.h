#pragma once
#include "DefaultHeader.h"

SC_MODULE(ICH_enc)
{
	//In
	sc_in<bool>	clock;		  //for sync
	sc_in<bool> reset;		  //
	sc_in<sc_uint<4>> qp;									//from rate controller
	sc_in<bool> ICH_enable;		//from decision for group module //1 ICH use, 0 no ICH 
	sc_in<bool> mode;			//from decision for group module //1 P-mode, 0 ICH-mode
	sc_in<bool> newgroup;		//from controller //1 new group 0 no
	sc_in<int> group;			//from controller //number of group vpos
	sc_in<int> linenumber;		//from controller //line number     hpo
	sc_in<sc_uint<26>> ycocg_in[PIXELS_PER_GROUP];	//from buffer
	sc_in<sc_uint<26>> recon[PIXELS_PER_GROUP];		//from decision for group module
	//sc_in<int> index[3];							
	sc_in<sc_uint<5>> index[PIXELS_PER_GROUP];		//from decision for group module
	sc_in<sc_int<26>>preline[SLICE_WIDTH];			//from predictor
													//												
	sc_out<int> err[PIXELS_PER_GROUP];				//to decision for group module, max over pixels in group,for abs form 0 = y, 1 = co, 2 = cg
	sc_out<sc_uint<5>> index_o[PIXELS_PER_GROUP];			//to decision for group module//max over pixels in group,for abs form 0 = y, 1 = co, 2 = cg
	sc_out<bool> ICH_decision;						//to decision for group module
													
	sc_out<bool>ichmode_end;							//to predictor
	sc_out<bool>pmode_end;							//to predictor
	//sc_out<int>state_o;

	//local variable
	enum State
	{
		IDLE,
		NEWGROUP,
		PMODE,
		ICHMODE
	};
	sc_signal<int> state;
	int index_buff[PIXELS_PER_GROUP];
	sc_int<26>recon_buff[PIXELS_PER_GROUP];
	sc_int<26>table[ICH_SIZE];	//include prefixparameter.h


	int reset_r;
	int ICH_enable_r;
	int mode_r, newgroup_r;
	// valid[32] => check index
	// 1 => ICHmode index , 0 else
	int mask[ICH_SIZE] = { 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0 };
	int valid[ICH_SIZE] = { 0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0 };

	int group_r, linenumber_r;
	//
	const int QuantDivisor[13] = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };
	//const int QuantOffset[] = {0, 0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047 };
	int qlevel_luma_8bpc[16] = { 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 7 };
	int qlevel_chroma_8bpc[16] = { 0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 8, 8 };
	//int qlevel_luma_10bpc[] = {0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 7, 8, 9 };
	//int qlevel_chroma_10bpc[] = {0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 10, 10, 10 };
	//int qlevel_luma_12bpc[] = {0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 9, 10, 11 };
	//int qlevel_chroma_12bpc[] = {0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 12, 12, 12 }; 

	//function
	void statemachine();
	int behavstate();
	void idle();

	int pmode();
	int ichmode();

	int newgroup_errsearch();
	int newgroup_update();

	SC_CTOR(ICH_enc)
	{
		SC_METHOD(statemachine);
			sensitive << clock.pos();

	}
};