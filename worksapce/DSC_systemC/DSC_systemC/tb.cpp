#define DEC_SUBSTREAM_TOP

#include "systemc.h"
#include "PrefixParameter.h"

#ifdef ENC_VLC_MUX
#include "VLC.h"
#include "MUX.h"
#endif // ENC_VLC_MUX

#ifdef DEC_DEMUX_VLD
#include "VLD.h"
#include "DEMUX.h"
#endif // ENC_VLC_MUX

#ifdef ENC_SUBSTREAM_TOP
#include "SubstreamTop_enc.h"
#endif // ENC_SUBSTREAM_TOP

#ifdef DEC_SUBSTREAM_TOP
#include "SubstreamTop_dec.h"
#endif //DEC_SUBSTREAM_TOP

// tb for 
#ifdef ENC
int sc_main(int argc, char* argv[])
{
	//signal global
	sc_clock clk;
	sc_signal<bool> reset;
	sc_signal<bool> enable_vlc;
	sc_signal<bool> enable_mux;
	sc_signal<int> group_counter;
	group_counter.write(100);

	//signal LINE_BUFFER - P_MODE
	sc_signal<bool> line_ready;
	sc_signal<sc_uint<26>> buffer_data[SLICE_WIDTH];

	//signal P_MODE - LINE_BUFFER
	sc_signal<bool> line_taken;
	sc_signal<bool> line_need;

	//signal ICH - P_MODE
	sc_signal<bool> ICH_done;

	//signal P_MODE - ICH
	sc_signal<sc_uint<26>> prev_buffer_data[SLICE_WIDTH];
	sc_signal<sc_uint<26>> curr_buffer_data[SLICE_WIDTH];

	//signal P_MODE - VLC
	sc_signal<sc_uint<26>> residual[3];
	sc_signal<int> p_err;

	//signal ICH -VLC
	sc_signal<sc_uint<5>> ICH_index[3];
	ICH_index[0].write(1);
	ICH_index[1].write(4);
	ICH_index[2].write(31);
	sc_signal<bool> ich_select_bit;
	ich_select_bit.write(0);
	sc_signal<int> ich_err;
	ich_err = 1;

	//signal RC
	sc_signal<sc_uint<4>> qp;
	qp.write(16);

	//signal VLC - ICH
	sc_signal<bool> ich_en;
	sc_signal<bool> mode_sel;

	//signal VLC - MUX
	sc_signal<sc_lv<1>> Y_out;
	sc_signal<sc_lv<1>> Co_out;
	sc_signal<sc_lv<1>> Cg_out;

	//signal MUX
	sc_signal<bool> muxing_mode;
	muxing_mode.write(true);

	//signal MUX - VLC
	sc_signal<bool> VLC_ready;

	//signal MUX - RC
	sc_signal<int> Y_fifio_size;
	sc_signal<int> Co_fifo_size;
	sc_signal<int> Cg_fifo_size;
	
	Predictor_enc pred("Predictor");
	{
		pred.clk(clk);
		pred.reset(reset);
		pred.rc_update_in(line_ready);
		pred.end_update_in(ICH_done);
		for (int i = 0; i < SLICE_WIDTH; i++)
		{
			pred.Line_in[i](buffer_data[i]);
		}
		pred.qp_in(qp);
		pred.residue_out[0](residual[0]);
		pred.residue_out[1](residual[1]);
		pred.residue_out[2](residual[2]);
		pred.p_err_out(p_err);
		for (int i = 0; i < SLICE_WIDTH; i++)
		{
			pred.prevLine_out[i](prev_buffer_data[i]);
			pred.currLine_out[i](curr_buffer_data[i]);
		}
		pred.end_out(line_taken);
		pred.ready_out(line_need);
	}
	
	ICH_enc ich("ICH");
	{
		ich.clock(clk);
		ich.reset(reset);
		ich.qp(qp);
		ich.ICH_enable(ich_en);
		ich.mode(mode_sel);
		sc_signal<bool> newgroup;
		sc_signal<int> group;
		sc_signal<int> linenumber;
		ich.newgroup(newgroup);
		ich.group(group);
		ich.linenumber(linenumber);
		ich.ICH_decision(ich_select_bit);
		for (int i = 0; i < PIXELS_PER_GROUP; i++)
		{
			ich.ycocg_in[i](buffer_data[i]);
			ich.recon[i](curr_buffer_data[i]);
			ich.index[i](ICH_index[i]);
			ich.index_o[i](ICH_index[i]);

		}
		ich.ichmode_end(ICH_done);
		ich.pmode_end(ICH_done);
	}


	VLC vlc("VLC");
	{
		vlc.clk(clk);
		vlc.reset(reset);
		vlc.enable_in(enable_vlc);
		vlc.VLC_ready_in(VLC_ready);
		vlc.P_mod_in[0](residual[0]);
		vlc.P_mod_in[1](residual[1]);
		vlc.P_mod_in[2](residual[2]);
		vlc.p_err_in(p_err);
		vlc.ICH_mod_in[0](ICH_index[0]);
		vlc.ICH_mod_in[1](ICH_index[1]);
		vlc.ICH_mod_in[2](ICH_index[2]);
		vlc.ich_use_in(ich_select_bit);
		vlc.ich_err_in(ich_err);
		vlc.qp_in(qp);
		vlc.unit_out[0](Y_out);
		vlc.unit_out[1](Co_out);
		vlc.unit_out[2](Cg_out);
		vlc.ich_enable_out(ich_en);
		vlc.mode_select_out(mode_sel);
		vlc.MUX_enable_out(enable_mux);
	}

	MUX mux("MUX");
	{
		mux.clk(clk);
		mux.reset(reset);
		mux.enable(enable_mux);
		mux.muxing_mode(muxing_mode);
		mux.group_count(group_counter);
		mux.fifo_in[0](Y_out);
		mux.fifo_in[1](Co_out);
		mux.fifo_in[2](Cg_out);
		mux.fifo_size_out[0](Y_fifio_size);
		mux.fifo_size_out[1](Co_fifo_size);
		mux.fifo_size_out[2](Cg_fifo_size);
		sc_signal<unsigned char*> b;
		mux.bitstream(b);
		mux.VLC_ready_out(VLC_ready);
	}

	sc_trace_file *wf = sc_create_vcd_trace_file("VLC");
	{
		sc_trace(wf, clk, "CLK");
		sc_trace(wf, reset, "reset");
		sc_trace(wf, enable_mux, "en_MUX");
		sc_trace(wf, enable_vlc, "en_VLC");
		sc_trace(wf, VLC_ready, "ready_VLC");

		sc_trace(wf, residual[0], "residual(0)");
		sc_trace(wf, residual[1], "residual(1)");
		sc_trace(wf, residual[2], "residual(2)");
		sc_trace(wf, Y_fifio_size, "Y_fifo_size");
		sc_trace(wf, Co_fifo_size, "Co_fifo_size");
		sc_trace(wf, Cg_fifo_size, "Cg_fifo_size");
		sc_trace(wf, Y_out, "Y_VLC2fifo");
		sc_trace(wf, Co_out, "Co_VLC2fifo");
		sc_trace(wf, Cg_out, "Cg_VLC2fifo");
		sc_trace(wf, mode_sel, "Mode");
		sc_trace(wf, ich_en, "ich_en");
	}

	reset.write(0);
	sc_start(1, SC_NS);
	reset.write(1);
	sc_start(1, SC_NS);
	enable_vlc.write(1);
	LOG("-----vlc_enalbe------" << endl);
	for (int i = 0; i < 4; i++)
	{
		sc_start(50, SC_NS);
		ich_select_bit.write(i % 2);
		LOG("-------ich_changed--------------------------------" << endl);
		sc_start(50, SC_NS);
	}
	ich_select_bit.write(0);
	sc_start(5000, SC_NS);

	return 0;
}
#endif //ENC

// tb for VLC_MUX
#ifdef ENC_VLC_MUX
int sc_main(int argc, char* argv[])
{
	//signal global
	sc_clock clk;
	sc_signal<bool> reset;
	sc_signal<bool> enable_vlc;
	sc_signal<bool> enable_mux;
	sc_signal<int> group_counter;
	group_counter.write(100);
	
	//signal P_MODE - VLC
	sc_signal<sc_uint<26>> residual[3];
	residual[0].write(53687091);
	residual[1].write(15793215);
	residual[2].write(63160800);
	sc_signal<int> p_err;
	p_err = 10;

	//signal ICH -VLC
	sc_signal<sc_uint<5>> ICH_index[3];
	ICH_index[0].write(1);
	ICH_index[1].write(4);
	ICH_index[2].write(31);
	sc_signal<bool> ich_select_bit;
	ich_select_bit.write(0);
	sc_signal<int> ich_err;
	ich_err = 1;

	//signal RC
	sc_signal<sc_uint<4>> qp;
	qp.write(16);

	//signal VLC - MUX
	sc_signal<sc_lv<1>> Y_out;
	sc_signal<sc_lv<1>> Co_out;
	sc_signal<sc_lv<1>> Cg_out;

	//signal MUX
	sc_signal<bool> muxing_mode;
	muxing_mode.write(true);

	//signal MUX - VLC
	sc_signal<bool> VLC_ready;

	VLC vlc("VLC");
		vlc.clk(clk);
		vlc.reset(reset);
		vlc.enable_in(enable_vlc);
		vlc.VLC_ready_in(VLC_ready);
		vlc.P_mod_in[0](residual[0]);
		vlc.P_mod_in[1](residual[1]);
		vlc.P_mod_in[2](residual[2]);
		vlc.p_err_in(p_err);
		vlc.ICH_mod_in[0](ICH_index[0]);
		vlc.ICH_mod_in[1](ICH_index[1]);
		vlc.ICH_mod_in[2](ICH_index[2]);
		vlc.ich_use_in(ich_select_bit);
		vlc.ich_err_in(ich_err);
		vlc.qp_in(qp);
		vlc.unit_out[0](Y_out);
		vlc.unit_out[1](Co_out);
		vlc.unit_out[2](Cg_out);
		sc_signal<bool> ich_en;
		vlc.ich_enable_out(ich_en);
		sc_signal<bool> mode_sel;
		vlc.mode_select_out(mode_sel);
		vlc.MUX_enable_out(enable_mux);

	MUX mux("MUX");
		mux.clk(clk);
		mux.reset(reset);
		mux.enable(enable_mux);
		mux.muxing_mode(muxing_mode);
		mux.group_count(group_counter);
		mux.fifo_in[0](Y_out);
		mux.fifo_in[1](Co_out);
		mux.fifo_in[2](Cg_out);
		sc_signal<int> Y_fifio_size;
		sc_signal<int> Co_fifo_size;
		sc_signal<int> Cg_fifo_size;
		mux.fifo_size_out[0](Y_fifio_size);
		mux.fifo_size_out[1](Co_fifo_size);
		mux.fifo_size_out[2](Cg_fifo_size);
		sc_signal<unsigned char*> b;
		mux.bitstream(b);
		mux.VLC_ready_out(VLC_ready);

		sc_trace_file *wf = sc_create_vcd_trace_file("VLC");
		sc_trace(wf, clk, "CLK");
		sc_trace(wf, reset, "reset");
		sc_trace(wf, enable_mux, "en_MUX");
		sc_trace(wf, enable_vlc, "en_VLC");
		sc_trace(wf, VLC_ready, "ready_VLC");

		sc_trace(wf, residual[0], "residual(0)");
		sc_trace(wf, residual[1], "residual(1)");
		sc_trace(wf, residual[2], "residual(2)");
		sc_trace(wf, Y_fifio_size, "Y_fifo_size");
		sc_trace(wf, Co_fifo_size, "Co_fifo_size");
		sc_trace(wf, Cg_fifo_size, "Cg_fifo_size");
		sc_trace(wf, Y_out, "Y_VLC2fifo");
		sc_trace(wf, Co_out, "Co_VLC2fifo");
		sc_trace(wf, Cg_out, "Cg_VLC2fifo");
		sc_trace(wf, mode_sel, "Mode");
		sc_trace(wf, ich_en, "ich_en");

		reset.write(0);
		sc_start(1, SC_NS);
		reset.write(1);
		sc_start(1, SC_NS);
		enable_vlc.write(1);
		LOG("-----vlc_enalbe------" << endl);
		for (int i = 0; i < 4; i++)
		{
			sc_start(50, SC_NS);
			ich_select_bit.write(i%2);
			LOG("-------ich_changed--------------------------------" << endl);
			sc_start(50, SC_NS);
		}
		ich_select_bit.write(0);
		sc_start(5000, SC_NS);

	return 0;
}
#endif //ENC_VLC_MUX

// tb for DEMUX_VLD
#ifdef DEC_DEMUX_VLD
int sc_main(int argc, char* argv[])
{
	FILE *fp = NULL;
	unsigned char buf[4512];

	if (fp == NULL)
	{
		fp = fopen("rate_buffer.txt", "rb");
		if (fp == NULL)
		{
			LOG("err" << endl);
		}
		else
		{
			fread(buf, 1, 4512, fp);
		}
	}

	//signal global
	sc_clock clk;
	sc_signal<bool> reset;
	sc_signal<bool> enable_vld;
	sc_signal<bool> enable_demux;

	//signal VLD - P_MODE
	sc_signal<sc_uint<26>> residual[3];

	//signal VLD - ICH
	sc_signal<sc_uint<5>> ICH_index[3];
	sc_signal<bool> mode_sel;

	//signal ICH - VLD
	sc_signal<bool> ich_ready;
	ich_ready.write(true);

	//signal VLD - DEMUX
	sc_signal<int> Y_fullness;
	sc_signal<int> Co_fullness;
	sc_signal<int> Cg_fullness;
	sc_signal<bool> DEMUX_ready;

	//signal RC
	sc_signal<sc_uint<4>> qp;
	qp.write(16);
	sc_signal<int> fullness;

	//signal DEMUX - VLD
	sc_signal<sc_lv<1>> Y_out;
	sc_signal<sc_lv<1>> Co_out;
	sc_signal<sc_lv<1>> Cg_out;

	//signal DEMUX
	sc_signal<bool> muxing_mode;
	muxing_mode.write(true);
	sc_signal<unsigned char*> buf_in;
	buf_in.write(buf);

	DEMUX demux("DEMUX");
	demux.clk(clk);
	demux.reset(reset);
	demux.enable_in(enable_demux);
	demux.ready_in(DEMUX_ready);
	demux.muxing_mode(muxing_mode);
	demux.fullness_in[0](Y_fullness);
	demux.fullness_in[1](Co_fullness);
	demux.fullness_in[2](Cg_fullness);
	demux.buf_input(buf_in);
	demux.unit_out[0](Y_out);
	demux.unit_out[1](Co_out);
	demux.unit_out[2](Cg_out);
	demux.VLD_enable_out(enable_vld);

	sc_signal<unsigned char*> b;

	VLD vld("VLD");
	vld.clk(clk);
	vld.reset(reset);
	vld.enable(enable_vld);
	vld.qp_in(qp);
	vld.ich_done_in(ich_ready);
	vld.unit_in[0](Y_out);
	vld.unit_in[1](Co_out);
	vld.unit_in[2](Cg_out);
	vld.P_mod_out[0](residual[0]);
	vld.P_mod_out[1](residual[1]);
	vld.P_mod_out[2](residual[2]);
	vld.ICH_mod_out[0](ICH_index[0]);
	vld.ICH_mod_out[1](ICH_index[1]);
	vld.ICH_mod_out[2](ICH_index[2]);
	vld.fullness_out[0](Y_fullness);
	vld.fullness_out[1](Co_fullness);
	vld.fullness_out[2](Cg_fullness);
	vld.mode_select_out(mode_sel);
	vld.DEMUX_ready_out(DEMUX_ready);


	sc_trace_file *wf = sc_create_vcd_trace_file("VLD");
	sc_trace(wf, clk, "CLK");
	sc_trace(wf, reset, "reset");
	sc_trace(wf, DEMUX_ready, "reaady_DEMUX");
	sc_trace(wf, enable_demux, "en_DEMUX");
	sc_trace(wf, enable_vld, "en_VLD");

	sc_trace(wf, Y_fullness, "Y_fullness");
	sc_trace(wf, Co_fullness, "Co_fullness");
	sc_trace(wf, Cg_fullness, "Cg_fullness");
	sc_trace(wf, Y_out, "Y_VLD");
	sc_trace(wf, Co_out, "Co_VLD");
	sc_trace(wf, Cg_out, "Cg_VLD");
	sc_trace(wf, mode_sel, "Mode");
	sc_trace(wf, residual[0], "P_mod_out(0)");
	sc_trace(wf, residual[1], "P_mod_out(1)");
	sc_trace(wf, residual[2], "P_mod_out(2)");
	sc_trace(wf, ICH_index[0], "ICH_mod_out(0)");
	sc_trace(wf, ICH_index[1], "ICH_mod_out(1)");
	sc_trace(wf, ICH_index[2], "ICH_mod_out(2)");

	reset.write(0);
	sc_start(1, SC_NS);
	reset.write(1);
	sc_start(1, SC_NS);

	enable_demux.write(true);
	LOG("-----vld_enalbe------" << endl);
	sc_start(1, SC_NS);


	sc_start(100, SC_NS);
	sc_start(1000, SC_NS);

	return 0;
}

#endif // DEC_DEMUX_VLD

// tb for enc_TOP
#ifdef ENC_SUBSTREAM_TOP
int sc_main(int argc, char* argv[])
{
	//signal global
	sc_clock clk;
	sc_signal<bool> reset;
	reset.write(0);
	sc_signal<bool> enable_vlc;
	enable_vlc.write(1);
	sc_signal<int> group_counter;
	group_counter.write(100);

	//signal P_MODE - VLC
	sc_signal<sc_uint<26>> residual[3];
	residual[0].write(53687091);
	residual[1].write(15793215);
	residual[2].write(63160800);
	sc_signal<int> p_err;
	p_err = 10;

	//signal ICH -VLC
	sc_signal<sc_uint<5>> ICH_index[3];
	ICH_index[0].write(1);
	ICH_index[1].write(4);
	ICH_index[2].write(31);
	sc_signal<bool> ich_select_bit;
	ich_select_bit.write(0);
	sc_signal<int> ich_err;
	ich_err = 1;

	//signal RC
	sc_signal<sc_uint<4>> qp;
	qp.write(16);


	//signal MUX
	sc_signal<bool> muxing_mode;
	muxing_mode.write(true);


	//VLC
	sc_signal<bool> ich_en;
	sc_signal<bool> mode_sel;

	//MUX
	sc_signal<int> Y_fifio_size;
	sc_signal<int> Co_fifo_size;
	sc_signal<int> Cg_fifo_size;
	sc_signal<unsigned char*> b;

	sc_trace_file *wf = sc_create_vcd_trace_file("VLC");
	sc_trace(wf, clk, "CLK");
	sc_trace(wf, reset, "reset");
	sc_trace(wf, enable_vlc, "en_VLC");

	sc_trace(wf, residual[0], "residual(0)");
	sc_trace(wf, residual[1], "residual(1)");
	sc_trace(wf, residual[2], "residual(2)");
	sc_trace(wf, Y_fifio_size, "Y_fifo_size");
	sc_trace(wf, Co_fifo_size, "Co_fifo_size");
	sc_trace(wf, Cg_fifo_size, "Cg_fifo_size");
	sc_trace(wf, mode_sel, "Mode");
	sc_trace(wf, ich_en, "ich_en");

	SUBSTREAM_TOP_enc sub_top_enc("SUB_TOP_ENC");
	sub_top_enc.clk(clk);
	sub_top_enc.reset(reset);
	sub_top_enc.group_counter(group_counter);
	sub_top_enc.enable_vlc(enable_vlc);
	sub_top_enc.residual[0](residual[0]);
	sub_top_enc.residual[1](residual[1]);
	sub_top_enc.residual[2](residual[2]);
	sub_top_enc.p_err(p_err);
	sub_top_enc.ICH_index[0](ICH_index[0]);
	sub_top_enc.ICH_index[1](ICH_index[1]);
	sub_top_enc.ICH_index[2](ICH_index[2]);
	sub_top_enc.ich_select_bit(ich_select_bit);
	sub_top_enc.ich_err(ich_err);
	sub_top_enc.qp(qp);
	sub_top_enc.ich_en(ich_en);
	sub_top_enc.mode_sel(mode_sel);
	sub_top_enc.muxing_mode(muxing_mode);
	sub_top_enc.Y_fifio_size(Y_fifio_size);
	sub_top_enc.Co_fifo_size(Co_fifo_size);
	sub_top_enc.Cg_fifo_size(Cg_fifo_size);
	sub_top_enc.bitstream(b);



	reset.write(0);
	sc_start(1, SC_NS);
	reset.write(1);
	sc_start(1, SC_NS);
	enable_vlc.write(1);
	LOG("-----vlc_enalbe------" << endl);
	for (int i = 0; i < 4; i++)
	{
		sc_start(50, SC_NS);
		ich_select_bit.write(i % 2);
		LOG("-------ich_changed--------------------------------" << endl);
		sc_start(50, SC_NS);
	}
	ich_select_bit.write(0);
	sc_start(5000, SC_NS);

	return 0;
}
#endif //ENC_SUBSTREAM_TOP

// tb for dec_TOP
#ifdef DEC_SUBSTREAM_TOP
int sc_main(int argc, char* argv[])
{
	FILE *fp = NULL;
	unsigned char buf[15000];

	if (fp == NULL)
	{
		fp = fopen("rate_buffer.txt", "rb");
		if (fp == NULL)
		{
			LOG("err" << endl);
		}
		else
		{
			fread(buf, 1, 15000, fp);
		}
	}

	//signal global
	sc_clock clk;
	sc_signal<bool> reset;
	sc_signal<bool> enable_demux;
	sc_signal<int> group_counter;
	group_counter.write(100);
	sc_signal<unsigned char*> buf_in;
	buf_in.write(buf);

	//signal VLD - P_MODE
	sc_signal<sc_uint<26>> residual[3];

	//signal VLD - ICH
	sc_signal<sc_uint<5>> ICH_index[3];
	sc_signal<bool> mode_sel;

	//signal ICH - VLD
	sc_signal<bool> ich_ready;
	ich_ready.write(true);

	//signal VLD - DEMUX
	sc_signal<int> Y_fullness;
	sc_signal<int> Co_fullness;
	sc_signal<int> Cg_fullness;

	//signal RC
	sc_signal<sc_uint<4>> qp;
	qp.write(16);


	//signal DEMUX
	sc_signal<bool> muxing_mode;
	muxing_mode.write(true);



	SUBSTREAM_TOP_dec sub_top_dec("SUB_TOP_DEC");
	sub_top_dec.clk(clk);
	sub_top_dec.reset(reset);
	sub_top_dec.buf(buf_in);
	sub_top_dec.enable_demux(enable_demux);
	sub_top_dec.muxing_mode(muxing_mode);
	sub_top_dec.qp(qp);
	sub_top_dec.ich_ready(ich_ready);
	sub_top_dec.ICH_index[0](ICH_index[0]);
	sub_top_dec.ICH_index[1](ICH_index[1]);
	sub_top_dec.ICH_index[2](ICH_index[2]);
	sub_top_dec.residual[0](residual[0]);
	sub_top_dec.residual[1](residual[1]);
	sub_top_dec.residual[2](residual[2]);
	sub_top_dec.mode_sel(mode_sel);
	sub_top_dec.Y_fullness(Y_fullness);
	sub_top_dec.Co_fullness(Co_fullness);
	sub_top_dec.Cg_fullness(Cg_fullness);

	sc_trace_file *wf = sc_create_vcd_trace_file("VLD");
	sc_trace(wf, clk, "CLK");
	sc_trace(wf, reset, "reset");
	sc_trace(wf, enable_demux, "en_DEMUX");
	sc_trace(wf, Y_fullness, "Y_fullness");
	sc_trace(wf, Co_fullness, "Co_fullness");
	sc_trace(wf, Cg_fullness, "Cg_fullness");
	sc_trace(wf, mode_sel, "Mode");
	sc_trace(wf, residual[0], "P_mod_out(0)");
	sc_trace(wf, residual[1], "P_mod_out(1)");
	sc_trace(wf, residual[2], "P_mod_out(2)");
	sc_trace(wf, ICH_index[0], "ICH_mod_out(0)");
	sc_trace(wf, ICH_index[1], "ICH_mod_out(1)");
	sc_trace(wf, ICH_index[2], "ICH_mod_out(2)");

	reset.write(0);
	sc_start(1, SC_NS);
	reset.write(1);
	sc_start(1, SC_NS);

	enable_demux.write(true);
	LOG("-----vld_enalbe------" << endl);
	sc_start(1, SC_NS);


	sc_start(100, SC_NS);
	sc_start(1000, SC_NS);

	return 0;
}

#endif // DEC_SUBSTREAM_TOP