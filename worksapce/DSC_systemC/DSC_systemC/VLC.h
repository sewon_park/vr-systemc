#pragma once

#include "systemc.h"
#include "PrefixParameter.h"

SC_MODULE(VLC)
{
	//input
	sc_in_clk clk;//clock
	sc_in<bool> reset;//reset signal
	sc_in<bool> enable_in;//enable signal
	sc_in<sc_uint<26>> P_mod_in[PIXELS_PER_GROUP];//residue - from Predictor module
	sc_in<sc_uint<5>> ICH_mod_in[PIXELS_PER_GROUP];//index - from ICH module
	sc_in<int> p_err_in;//p-err value - from Predictor module
	sc_in<int> ich_err_in;//ich-er value - from ICH module
	sc_in<bool> ich_use_in;// 0:dont use ICH , 1:use ICH - from ICH module
	sc_in<sc_uint<4>> qp_in;// quantization parameter - from Rate Controller
	sc_in<bool> VLC_ready_in;// ready signal - from MUX

	//output
	sc_out<sc_lv<1>> unit_out[UNITS_PER_GROUP];//bit stream data - to MUX
	sc_out<bool> ich_enable_out;// ICH update signal - to ICH module
	sc_out<bool> mode_select_out;// mode selection signal - to ICH module
	sc_out<bool> MUX_enable_out;// mux activator - to MUX

	//local variable
		//state
	enum State
	{
		IDLE,
		INIT,
		RUN,
		TRANSMIT,
		WAIT
	};
	State state = IDLE;
	int calculate_stage[PIXELS_PER_GROUP];
	bool calculate_done;
	bool transmit_done;
		//input
	int residue[PIXELS_PER_GROUP][NUM_COMPONENTS];
	int ich[PIXELS_PER_GROUP];
		//internal calculate
	int max_size;
	int prefix_value;
	int size;
	int previous_num_bits[PIXELS_PER_GROUP];
	int force_mpp = 0;
	int max_bits_per_group = ((3 * BIT_DEPTH + 15) / 16);
	int qp_;
	int qlevel[PIXELS_PER_GROUP], qlevel_old[PIXELS_PER_GROUP];
	bool prev_ich_selected, ich_selected;
	int adj_predicted_size[UNITS_PER_GROUP];
	int required_size[UNITS_PER_GROUP];
	int predicted_size[UNITS_PER_GROUP];
	int out[UNITS_PER_GROUP][40];
	int out_amount[UNITS_PER_GROUP];
	int transmit_iterator;
	int bits_ich_mode, bits_p_mode;

	//functions
		//state
	void StateMachine();
	void Init();
	void Run();
	void Transmit();
		//internal calculate
	int MapQpToQlevel(int qp, int cpnt);
	int GetQpAdjPredSize(int cpnt);
	int FindResidualSize(int eq);
	void AddBits(int cpnt, int value, int bit_num);
	int PredictSize(int *req_size);
	int EstimateBitsForGroup();

	SC_CTOR(VLC)
	{
		SC_METHOD(StateMachine);
			dont_initialize();
			sensitive << clk.pos();
			sensitive_neg << reset;
	}

	//for clock counting
	int cl = 0;
};