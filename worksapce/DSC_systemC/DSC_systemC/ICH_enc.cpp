#include "ICH_enc.h"
#include "fstream"
#include "iostream"

void ICH_enc::idle()
{
	int i;
	ICH_enable_r = ICH_enable.read();
	mode_r = mode.read();
	newgroup_r = newgroup.read();
	pmode_end.write(0);
	ichmode_end.write(0);

	for (i = 0; i<ICH_SIZE; i++)
		mask[i] = 0;
}

void ICH_enc::statemachine()
{
	int i;
	sc_int<26> ini;
	ini = 0;

	if (!reset.read())
	{
		for (i = 0; i < ICH_SIZE; i++)
		{
			valid[i] = 0;
			table[i] = ini; 	//initial setting
		}
		state = IDLE;

	}
	else
	{
		state = behavstate();
	}
}

int ICH_enc::behavstate()
{
	int next_state;

	if (!reset.read())
	{
		next_state = IDLE;
		return next_state;
	}
	else
	{
		if (state == IDLE)
		{
			idle();
			if (!ICH_enable_r)	//ICH_enable = 0, newgroup = 1
			{
				if (newgroup_r)
				{
					next_state = NEWGROUP;
					return next_state;
				}
				else
				{
					next_state = state;
					return next_state;
				}
			}
			else 				//ICH_enable = 1
			{
				if (mode_r)		//Pmode
				{
					next_state = PMODE;
					return next_state;
				}
				else 			//ICH_mode
				{
					next_state = ICHMODE;
					return next_state;
				}
			}
			return next_state = state;
		}
		else if (state == PMODE)
		{
			if (pmode())
			{
				next_state = IDLE;
				return next_state;
			}
			else
			{
				next_state = state;
				return next_state;
			}
		}
		else if (state == ICHMODE)
		{

			if (ichmode() == 1)
			{
				next_state = IDLE;
				return next_state;
			}
			else
			{
				next_state = state;
				return next_state;
			}
		}
		else	//NEWGROUP
		{
			group_r = group.read();


			if (linenumber.read() == 0) // first line
			{
				if (newgroup_errsearch())
				{
					next_state = IDLE;
					return next_state;
				}
				else
				{
					next_state = state;
					return next_state;
				}
			}
			else			   //no first line
			{
				if (newgroup_update())
				{
					if (newgroup_errsearch())
					{
						next_state = IDLE;
						return next_state;
					}
					else
					{
						next_state = state;
						return next_state;
					}
				}
				else
				{
					next_state = state;
					return next_state;
				}
			}
		}
	}

}

int ICH_enc::pmode()
{
	int line = 0;
	int i;

	line = linenumber.read();
	//recon val to buff
	for (i = 0; i < 3; i++)
		recon_buff[i] = recon[i].read();
	//shift		
	if (linenumber.read() % SLICE_WIDTH == 0)	//first line 	index 0~31
	{
		for (i = ICH_SIZE - 1 - 3; i >= 0; i--)
		{
			valid[i + 3] = valid[i];
			table[i + 3] = table[i];
		}
	}
	else			//no first line index 0~24
	{
		for (i = ICH_SIZE - 1 - 7 - 3; i >= 0; i--)
		{
			table[i + 3] = table[i];
			valid[i + 3] = valid[i];
		}

	}
	//update
	for (i = 0; i < 3; i++)
	{
		table[i] = recon_buff[i];
		valid[i] = 1;
	}
	/*
	//log
	cout << " Encoder Pmode table shift result" << endl;
	for (i = 0; i < ENTRY; i++)
	cout << table[i]<<",";
	cout << endl;
	for (i = 0; i < ENTRY; i++)
	cout << valid[i] << ",";
	cout << endl;
	//
	*/

	pmode_end.write(1);
	ichmode_end.write(0);
	return 1;
}

int ICH_enc::ichmode()
{
	int i, j;
	int count = 0;
	int same = 0;
	int SIZE;

	index_buff[0] = index[0].read().to_int();
	index_buff[1] = index[1].read().to_int();
	index_buff[2] = index[2].read().to_int();
	for (i = 0; i<PIXELS_PER_GROUP; i++)
		mask[index_buff[i]] = 1;
	//check same
	for (i = 0; i< PIXELS_PER_GROUP; i++)
	{
		for (j = i + 1; j< PIXELS_PER_GROUP; j++)
		{
			if (index_buff[i] == index_buff[j])
				same = same + 1;
		}
	}
	if (same == 3)
		same = 2;
	//table => buff
	for (i = 0; i< PIXELS_PER_GROUP; i++)
	{
		if (valid[index_buff[i]] == 1)
			recon_buff[i] = table[index_buff[i]];
		//else	
	}
	//shift		
	//1st line : no 1st line
	SIZE = (linenumber.read() % SLICE_WIDTH == 0) ? ICH_SIZE : ICH_SIZE - 7;	//1st line : no 1st line		
	if (index_buff[1] == index_buff[0])
	{
		index_buff[1] = index_buff[2];
		recon_buff[1] = recon_buff[2];
	}
	for (i = SIZE - 1; i >= 0; i--)
	{
		if (!mask[i])//if (is valid)      valid[index] = 1  : not excute
		{
			for (j = 0; j < PIXELS_PER_GROUP - same; j++)
			{
				if (i < index_buff[j])
					count = count + 1;
			}
		}
		//shift
		if (i + count < SIZE)
		{
			table[i + count] = table[i];
			valid[i + count] = valid[i];
		}
		count = 0;
	}
	for (i = 0; i < PIXELS_PER_GROUP - same; i++)
	{
		table[i] = recon_buff[i];
		valid[i] = 1;
	}
	same = 0;
	/*
	//log
	cout << " Encoder ICHmode table shift result" << endl;
	for (i = 0; i < ENTRY; i++)
	cout << table[i]<<",";
	cout << endl;
	for (i = 0; i < ENTRY; i++)
	cout << valid[i] << ",";
	cout << endl;
	//
	*/

	pmode_end.write(0);
	ichmode_end.write(1);

	return 1;
}
/////////////////////
//Need optimization//
/////////////////////
int ICH_enc::newgroup_errsearch()
{
	//new optimize code
	sc_int<8>y[PIXELS_PER_GROUP];
	sc_int<9>co[PIXELS_PER_GROUP], cg[PIXELS_PER_GROUP];
	//diff[# of pixel in group][entry][# of component]
	int i, j, k;
	int modified_qp;
	int qp_read;
	int hit = 1;
	int lowest[PIXELS_PER_GROUP] = { 99999,99999,99999 };   //store lowest weight sum
	int best[PIXELS_PER_GROUP] = { 99,99,99 };              //store index
	int weightsad[PIXELS_PER_GROUP] = { 0,0,0 };            //store weight sum
	int diff[PIXELS_PER_GROUP][ICH_SIZE][3];
	int max[3] = { 0,0,0 };                           //store max err value
	int maxerr[PIXELS_PER_GROUP];         //store maxQperr
									//calcuate qp
	qp_read = qp.read();
	modified_qp = MIN(15, qp_read + 2);
	maxerr[0] = QuantDivisor[qlevel_luma_8bpc[modified_qp]] / 2;
	maxerr[1] = maxerr[2] = QuantDivisor[qlevel_chroma_8bpc[modified_qp]] / 2;
	//input new group value
	for (i = 0; i< 3; i++)
	{
		y[i] = ycocg_in[i].read().range(25, 18);
		co[i] = ycocg_in[i].read().range(17, 9);
		cg[i] = ycocg_in[i].read().range(8, 0);
	}
	//cal err and store in array
	for (j = 0; j < PIXELS_PER_GROUP; j++)     //parallel?
	{
		for (i = 0; i< ICH_SIZE; i++)
		{
			if (valid[i] == 1)
			{
				diff[j][i][0] = (int)table[i].range(25, 18) - (int)y[j];		//pixel j y val
				diff[j][i][1] = (int)table[i].range(17, 9) - (int)co[j];		//pixel j co val
				diff[j][i][2] = (int)table[i].range(8, 0) - (int)cg[j];		//pixel j cg val
			}
		}
	}
	//decision and index_o cal    
	for (j = 0; j < PIXELS_PER_GROUP; j++)     //parallel?
	{
		for (i = 0; i< ICH_SIZE; i++)
		{
			if (valid[i] == 1)
			{
				//decise use or not
				if ((diff[j][i][0] < maxerr[0]) && (diff[j][i][1] < maxerr[1]) && (diff[j][i][2] < maxerr[2]))
					hit = 1;
				else
					hit = 0;
				//cal weight 
				weightsad[j] = 2 * abs(diff[j][i][0]) + abs(diff[j][i][1]) + abs(diff[j][i][2]);
				if (lowest[j] > weightsad[j])
				{
					lowest[j] = weightsad[j];
					best[j] = i;
				}

			}
		}
	}
	ICH_decision.write(hit);
	//find max y co cg err 
	for (j = 0; j < 3; j++)              //for each component
	{
		for (i = 0; i < ICH_SIZE; i++)       // for all entry
		{
			for (k = 0; k < PIXELS_PER_GROUP; k++)
			{
				//max[# of component][#of entry]
				if (max[j] < diff[k][i][j])
					max[j] = diff[k][i][j];
			}
		}
	}
	//write value
	for (i = 0; i < PIXELS_PER_GROUP; i++)
	{
		index_o[i].write(best[i]);
		err[i].write(max[i]);
	}
	return 1;
}

int ICH_enc::newgroup_update()
{
	sc_int<26>prebuff[SLICE_WIDTH];

	int groupnum = 0;
	int i;
	groupnum = group.read();
	for (i = 0; i< SLICE_WIDTH; i++)
		prebuff[i] = preline[i].read();

	for (i = 0 + (groupnum%GROUP_PER_LINE); i< 7 + (groupnum%GROUP_PER_LINE); i++)
	{
		table[25 + i - (groupnum % GROUP_PER_LINE)] = prebuff[i];
		valid[25 + i - groupnum%GROUP_PER_LINE] = 1;
	}
	return 1;
}