#pragma once

//Never Chagne
#define NUM_COMPONENTS 3
#define PADDING_LEFT 5
#define PADDING_RIGHT 5
#define PIXELS_PER_GROUP 3
#define ICH_BITS 5
#define ICH_SIZE (1 << ICH_BITS)
#define PRED_BLK_SIZE 3
#define GROUPS_PER_SUPERGROUP 4
#define SAMPLES_PER_UNIT 3
#define UNITS_PER_GROUP 3
#define SAMPLES_PER_UNIT 3

//Depends on CFG
#define BIT_DEPTH 8
#define CHROMA_BIT_DEPTH (BIT_DEPTH + 1)
#define SLICE_WIDTH 9
#define SLICE_HEIGHT 9
#define GROUP_PER_LINE (int)(SLICE_WIDTH/PIXELS_PER_GROUP)
#define MUX_WORD_SIZE 48
#define MAX_SE_SIZE (4*BIT_DEPTH+4)
#define CHUCK_SIZE (int)(((SLICE_WIDTH * BIT_DEPTH)+7)/8)

#define BP_RANGE 10
#define BP_SIZE 3
#define BP_EDGE_STRENGTH 32

//Functions
#define BIT_DEPTH_F(X) ((X==0) ? BIT_DEPTH : CHROMA_BIT_DEPTH)
#define CLAMP(X, MIN, MAX) ((X)>(MAX) ? (MAX) : ((X)<(MIN) ? (MIN) : (X)))
#define MAX(X, Y) ((X)>(Y) ? (X) : (Y))
#define MIN(X, Y) ((X)<(Y) ? (X) : (Y))
#define ABS(X) ((X)<0 ? (-1*(X)) : (X))
#define FMOD(X,Y) fmod(fmod(X,Y)+Y,Y)

#define LOG(X) (cout << X)
