#include "ICH_dec.h"
#include "string.h"

void ICH_dec::idle()
{
	int i;
	reset_r = reset.read();
	ICH_enable_r = ICH_enable.read();
	mode_r = mode.read();
	newgroup_r = newgroup.read();
	for (i = 0; i<ICH_SIZE; i++)
		mask[i] = 0;

}
void ICH_dec::statemachine()
{
	if (reset.read() == 0)
	{
		//cout<<"test"<<endl;
		state = IDLE;
	}
	else
	{
		state = next_state_d;
	}
}

void ICH_dec::behavstate()
{
	if (!reset.read())
	{
		next_state_d = IDLE;
	}
	else
	{

		if (state == IDLE)
		{
			idle();

			pmode_start.write(0);
			ichmode_start.write(0);
			newupdate_start.write(0);
			if (!ICH_enable.read())	//ICH_enable = 0, newgroup = 1
			{
				if (newgroup.read())
				{
					next_state_d = NEWGROUP;
					pmode_start.write(0);
					ichmode_start.write(0);
					newupdate_start.write(1);
				}
				else
				{
					next_state_d = state;

				}
			}
			else 				//ICH_enable = 1
			{
				if (mode.read())		//Pmode
				{
					next_state_d = PMODE;
					pmode_start.write(1);
					ichmode_start.write(0);
					newupdate_start.write(0);
				}
				else 			//ICH_mode
				{
					next_state_d = ICHMODE;
					pmode_start.write(0);
					ichmode_start.write(1);
					newupdate_start.write(0);
				}
			}
		}
		else if (state == PMODE)
		{
			if (pmode_table_shift_done.read() == 1)
			{
				next_state_d = IDLE;
				pmode_start.write(0);
			}
			else
			{
				next_state_d = state;
			}
		}
		else if (state == ICHMODE)
		{
			if ((ichmode_mask_reconbuff_update.read() == 1) && (ichmode_index_line_read_done.read() == 1) && (shift_update_ichmode.read() == 1))
			{
				next_state_d = IDLE;
				ichmode_start.write(0);
			}
			else
			{
				next_state_d = state;
			}
		}

		else if (state == NEWGROUP)	//NEWGROUP
		{
			if (update_table_new.read() == 1)
			{
				next_state_d = IDLE;
				newupdate_start.write(0);
			}
			else
			{
				next_state_d = state;
			}
		}
	}
}

void ICH_dec::line_recon_read_pmode()
{
	int i;
	if (!reset.read())
	{
		pmode_line_recon_read_done.write(0);
	}
	else
	{
		if (pmode_start.read() == 1)
		{
			line = linenumber.read();
			for (i = 0; i< PIXELS_PER_GROUP; i++)
				recon_buff[i] = recon[i].read();

			pmode_line_recon_read_done.write(1);

		}
		else
		{
			pmode_line_recon_read_done.write(0);
		}
	}
}

void ICH_dec::index_line_read()
{
	int i;
	if (!reset.read())
	{
		ichmode_index_line_read_done.write(0);
	}
	else
	{
		if (ichmode_start.read() == 1)
		{
			for (i = 0; i<PIXELS_PER_GROUP; i++)
				index_buff[i] = index[i].read().to_int();

			line = linenumber.read();
			ichmode_index_line_read_done.write(1);
		}
		else
		{
			for (i = 0; i<PIXELS_PER_GROUP; i++)
				index_buff[i] = 0;
			ichmode_index_line_read_done.write(0);
		}
	}
}

void ICH_dec::mask_reconbuff_update()
{
	int i;
	if (!reset.read())
	{
		ichmode_mask_reconbuff_update.write(0);
	}
	else
	{
		if ((ichmode_start.read() == 1) && (ichmode_index_line_read_done.read() == 1))
		{
			for (i = 0; i< PIXELS_PER_GROUP; i++)
				mask[index_buff[i]] = 1;
			for (i = 0; i <PIXELS_PER_GROUP; i++)
				recon_buff[i] = table[index_buff[i]];

			ichmode_mask_reconbuff_update.write(1);
		}
		else
		{
			for (i = 0; i< ICH_SIZE; i++)
				mask[i] = 0;
			ichmode_mask_reconbuff_update.write(0);
		}
	}
}

void ICH_dec::group_line_read()
{
	int i;
	if (!reset.read())
	{
		group_line_read_done.write(0);
	}
	else
	{
		if (newupdate_start.read() == 1)
		{
			groupnum = group.read();
			line = linenumber.read();

			for (i = 0; i< SLICE_WIDTH; i++)
				prebuff[i] = preline[i].read();    //prebuff must be global variable

			group_line_read_done.write(1);
		}
		else
		{
			group_line_read_done.write(0);
		}

	}
}

void ICH_dec::shift_table()
{
	int i;
	int j;
	sc_int<26>ini;
	ini = 0;
	int count = 0;
	int same = 0;

	if (reset.read() == 0)
	{
		pmode_table_shift_done.write(0);
		shift_update_ichmode.write(0);
		update_table_new.write(0);
		pmode_end.write(0);
		ichmode_end.write(0);
		for (i = 0; i < ICH_SIZE; i++)
		{
			table[i] = 0; 	//initial
			valid[i] = 0;
		}
	}
	else
	{
		if (ICH_enable.read() == 0)   //update
		{
			if ((group_line_read_done.read() == 1) && (newupdate_start.read() == 1))
			{
				if (linenumber.read() != 0)
				{
					for (i = 0 + (group.read() % GROUP_PER_LINE); i < 7 + (group.read() % GROUP_PER_LINE); i++)
					{
						table[25 + i - group.read() % GROUP_PER_LINE] = prebuff[i];
						valid[25 + i - group.read() % GROUP_PER_LINE] = 1;
					}
					update_table_new.write(1);
					pmode_table_shift_done.write(0);
					shift_update_ichmode.write(0);
				}
				else
				{
					update_table_new.write(1);
					pmode_table_shift_done.write(0);
					shift_update_ichmode.write(0);
				}
				/*
				//log//
				cout << "Newgroup table shift result" << endl;
				for (i = 0; i < ENTRY; i++)
				cout << table[i] << ",";
				cout << endl;
				for (i = 0; i < ENTRY; i++)
				cout << valid[i] << ",";
				cout << endl;
				//
				*/
			}
			else
			{
				update_table_new.write(0);
				pmode_table_shift_done.write(0);
				shift_update_ichmode.write(0);
				pmode_end.write(0);
				ichmode_end.write(0);
			}
		}
		else			//ich enable
		{
			if ((pmode_line_recon_read_done.read() == 1) && (pmode_start.read() == 1))	// pmode
			{
				if (linenumber.read() % SLICE_HEIGHT == 0)
				{
					for (i = 31 - 3; i >= 0; i--)		//shift
					{
						table[i + 3] = table[i];
						valid[i + 3] = valid[i];
					}

					table[0] = recon_buff[0];
					table[1] = recon_buff[1];
					table[2] = recon_buff[2];
					valid[0] = 1;
					valid[1] = 1;
					valid[2] = 1;
				}
				else
				{
					for (i = 31 - 7 - 3; i >= 0; i--)	//shift
					{
						table[i + 3] = table[i];
						valid[i + 3] = valid[i];
					}

					table[0] = recon_buff[0];
					table[1] = recon_buff[1];
					table[2] = recon_buff[2];
					valid[0] = 1;
					valid[1] = 1;
					valid[2] = 1;
				}
				/*
				//log//
				cout << "Pmode table shift result" << endl;
				for (i = 0; i < ENTRY; i++)
				cout << table[i] << ",";
				cout << endl;
				for (i = 0; i < ENTRY; i++)
				cout << valid[i] << ",";
				cout << endl;
				//
				*/
				pmode_table_shift_done.write(1);
				shift_update_ichmode.write(0);
				update_table_new.write(0);

				pmode_end.write(1);
				ichmode_end.write(0);


			}
			//ichmode
			else if (((ichmode_start.read() == 1) && (ichmode_index_line_read_done.read() == 1)) && (ichmode_mask_reconbuff_update.read() == 1))
			{
				ycocg_o[0].write(recon_buff[0]);
				ycocg_o[1].write(recon_buff[1]);
				ycocg_o[2].write(recon_buff[2]);
				cout << index_buff[0] << index_buff[1] << index_buff[2];
				for (i = 0; i<3; i++)
				{
					for (j = i + 1; j<3; j++)
					{
						if (index_buff[i] == index_buff[j])
							same = same + 1;
					}
				}
				if (same == 3)
					same = 2;
				if (linenumber.read() % SLICE_HEIGHT == 0)                 // for first line
				{
					if (index_buff[1] == index_buff[0])
					{
						index_buff[1] = index_buff[2];
						recon_buff[1] = recon_buff[2];
					}
					for (i = ICH_SIZE - 1; i >= 0; i--)
					{
						if (!mask[i])
						{
							for (j = 0; j < 3 - same; j++)
							{
								if (i < index_buff[j])
									count = count + 1;
							}
						}
						if (i + count < 32)
						{
							table[i + count] = table[i];
							valid[i + count] = valid[i];
						}

						count = 0;
					}

					for (i = 0; i < 3 - same; i++)
					{
						table[i] = recon_buff[i];
						valid[i] = 1;
					}

					for (i = 0; i< ICH_SIZE; i++)
						mask[i] = 0;
					same = 0;
					//ich mode is finish
				}
				else                            // not a first line
				{
					if (index_buff[1] == index_buff[0])
					{
						index_buff[1] = index_buff[2];
						recon_buff[1] = recon_buff[2];
					}
					for (i = ICH_SIZE - 1 - 7; i >= 0; i--)
					{
						if (!mask[i])
						{
							for (j = 0; j< 3 - same; j++)
							{
								if (i < index_buff[j])
									count = count + 1;
							}
						}
						if (i + count < 32 - 7)
						{
							table[i + count] = table[i];
							valid[i + count] = valid[i];
						}

						count = 0;
					}
					for (i = 0; i< 32; i++)
						mask[i] = 0;

					if (index_buff[1] == index_buff[0])
						recon_buff[1] = recon_buff[2];
					for (i = 0; i < 3 - same; i++)
					{
						table[i] = recon_buff[i];
					}
					//ich mode is finish
					same = 0;
				}
				/*
				//log
				cout << "ICHmode table shift result" << endl;
				for (i = 0; i < ENTRY; i++)
				cout << table[i]<<",";
				cout << endl;
				for (i = 0; i < ENTRY; i++)
				cout << valid[i] << ",";
				cout << endl;
				//
				*/
				shift_update_ichmode.write(1);
				pmode_table_shift_done.write(0);
				update_table_new.write(0);
				pmode_end.write(0);
				ichmode_end.write(1);

			}
			else
			{
				pmode_table_shift_done.write(0);
				shift_update_ichmode.write(0);
				update_table_new.write(0);
				pmode_end.write(0);
				ichmode_end.write(0);
			}
		}
	}
}