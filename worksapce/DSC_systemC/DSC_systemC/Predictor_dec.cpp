#include "Predictor_dec.h"
#include "string.h"

//state define
#define WAIT 0
#define CALCUL 1

void Predictor_dec::statemachine()
{
	if (reset.read() == 0)
	{
		init();
		state = WAIT;
	}
	else
	{
		state = next_state;
	}
}
void Predictor_dec::behavstate()
{
	if (!reset.read())
	{
		init();
		next_state = WAIT;
	}
	else
	{
		if (state == WAIT)
		{
			if (rc_update.read() == 1)
			{
				next_state = CALCUL;
			}
			else
			{
				next_state = state;
			}
		}
		else if (state == CALCUL)
		{
			if (CALCUL_done.read() == 1)
			{
				next_state = WAIT;
			}
			else
			{
				next_state = state;
			}
		}
	}
}
void Predictor_dec::CALCUL_state()
{
	if (!reset.read())
	{
		CALCUL_done.write(0);
	}
	else
	{
		if (take.read() == 1)
		{
			DSC_Algorithm();
			CALCUL_done.write(1);
		}
		else
		{
			CALCUL_done.write(0);
		}
	}
}
void Predictor_dec::CATCH()
{
	if (rc_update.read() == 1)
	{
		take.write(1);
	}
	else
	{
		take.write(0);
	}
}

void Predictor_dec::getInput()
{
	for (int i = 0; i < 3; i++)
	{
		Y = residue[i].read().range(25, 18);
		Co = residue[i].read().range(17, 9);
		Cg = residue[i].read().range(8, 0);

		quantizedResidual[0][i] = Y;
		quantizedResidual[1][i] = Co;
		quantizedResidual[2][i] = Cg;
	}
}
int Predictor_dec::MapQpToQlevel(int qp, int cpnt)
{
	int qlevel;

	if (cpnt == 0)
		qlevel = quantTableLuma[qp];
	else
		qlevel = quantTableChroma[qp];

	return (qlevel);
}
int Predictor_dec::QuantizeResidual(int e, int qlevel)
{
	int eq;

	if (e>0)
		eq = (e + QuantOffset[qlevel]) >> qlevel;
	else
		eq = -((QuantOffset[qlevel] - e) >> qlevel);

	return eq;
}
int Predictor_dec::ceil_log2(int val)
{
	int ret = 0, x;
	x = val;
	while (x) { ret++; x >>= 1; }
	return(ret);
}
int Predictor_dec::FindMidpoint(int cpnt, int qlevel)
{
	int range;
	int leftRecon[3];

	if (cpnt == 0)
		range = 1 << 8;
	else
		range = 1 << 8;

	leftRecon[cpnt] = currLine[cpnt][MIN(SLICE_WIDTH - 1, hPos) + PADDING_LEFT];
	return (range / 2 + (leftRecon[cpnt] % (1 << qlevel)));
}
int Predictor_dec::SampToLineBuf(int x, int cpnt)
{
	int shift_amount, round, storedSample;
	int linebuf_depth = 8;

	// *MODEL NOTE* MN_LINE_STORAGE
	shift_amount = MAX(cpntBitDepth[cpnt] - linebuf_depth, 0);
	if (shift_amount > 0)
		round = 1 << (shift_amount - 1);
	else
		round = 0;

	storedSample = MIN(((x + round) >> shift_amount), (1 << linebuf_depth) - 1);
	return (storedSample << shift_amount);
}
int Predictor_dec::FindResidualSize(int eq)
{
	int size_e;

	// Find the size in bits of e
	if (eq == 0) size_e = 0;
	else if (eq >= -1 && eq <= 0) size_e = 1;
	else if (eq >= -2 && eq <= 1) size_e = 2;
	else if (eq >= -4 && eq <= 3) size_e = 3;
	else if (eq >= -8 && eq <= 7) size_e = 4;
	else if (eq >= -16 && eq <= 15) size_e = 5;
	else if (eq >= -32 && eq <= 31) size_e = 6;
	else if (eq >= -64 && eq <= 63) size_e = 7;
	else if (eq >= -128 && eq <= 127) size_e = 8;
	else if (eq >= -256 && eq <= 255) size_e = 9;
	else if (eq >= -512 && eq <= 511) size_e = 10;
	else if (eq >= -1024 && eq <= 1023) size_e = 11;
	else if (eq >= -2048 && eq <= 2047) size_e = 12;
	else if (eq >= -4096 && eq <= 4095) size_e = 13;
	else size_e = 14;

	return size_e;
}

int Predictor_dec::SamplePredict(int prevLine[], int currLine[], int hPos, int predType, int qLevel, int cpnt)
{
	int a, b, c, d, e;
	int filt_b, filt_c, filt_d, filt_e;
	int blend_b, blend_c, blend_d, blend_e;
	int p;
	int bp_offset;
	int diff = 0;
	int h_offset_array_idx;

	h_offset_array_idx = (hPos / 3) * 3 + PADDING_LEFT;

	// organize samples into variable array defined in dsc spec
	c = prevLine[h_offset_array_idx - 1];
	b = prevLine[h_offset_array_idx];
	d = prevLine[h_offset_array_idx + 1];
	e = prevLine[h_offset_array_idx + 2];
	a = currLine[h_offset_array_idx - 1];

#define FILT3(a,b,c) (((a)+2*(b)+(c)+2)>>2)
	filt_c = FILT3(prevLine[h_offset_array_idx - 2], prevLine[h_offset_array_idx - 1], prevLine[h_offset_array_idx]);
	filt_b = FILT3(prevLine[h_offset_array_idx - 1], prevLine[h_offset_array_idx], prevLine[h_offset_array_idx + 1]);
	filt_d = FILT3(prevLine[h_offset_array_idx], prevLine[h_offset_array_idx + 1], prevLine[h_offset_array_idx + 2]);
	filt_e = FILT3(prevLine[h_offset_array_idx + 1], prevLine[h_offset_array_idx + 2], prevLine[h_offset_array_idx + 3]);

	switch (predType)
	{
	case PT_MAP:	// MAP prediction
					// *MODEL NOTE* MN_MMAP
		diff = CLAMP(filt_c - c, -(QuantDivisor[qLevel] / 2), QuantDivisor[qLevel] / 2);
		blend_c = c + diff;
		diff = CLAMP(filt_b - b, -(QuantDivisor[qLevel] / 2), QuantDivisor[qLevel] / 2);
		blend_b = b + diff;
		diff = CLAMP(filt_d - d, -(QuantDivisor[qLevel] / 2), QuantDivisor[qLevel] / 2);
		blend_d = d + diff;
		diff = CLAMP(filt_e - e, -(QuantDivisor[qLevel] / 2), QuantDivisor[qLevel] / 2);
		blend_e = e + diff;

		// Pixel on line above off the raster to the left gets same value as pixel below (ie., midpoint)
		if (hPos / SAMPLES_PER_UNIT == 0)
			blend_c = a;

		if ((hPos % SAMPLES_PER_UNIT) == 0)  // First pixel of group
			p = CLAMP(a + blend_b - blend_c, MIN(a, blend_b), MAX(a, blend_b));
		else if ((hPos % SAMPLES_PER_UNIT) == 1)   // Second pixel of group
			p = CLAMP(a + blend_d - blend_c + (quantizedResidual[cpnt][0] * QuantDivisor[qLevel]),
				MIN(MIN(a, blend_b), blend_d), MAX(MAX(a, blend_b), blend_d));
		else    // Third pixel of group
			p = CLAMP(a + blend_e - blend_c + (quantizedResidual[cpnt][0] + quantizedResidual[cpnt][1])*QuantDivisor[qLevel],
				MIN(MIN(a, blend_b), MIN(blend_d, blend_e)), MAX(MAX(a, blend_b), MAX(blend_d, blend_e)));
		break;
	case PT_LEFT:
		p = a;    // First pixel of group
		if ((hPos % SAMPLES_PER_UNIT) == 1)   // Second pixel of group
			p = CLAMP(a + (quantizedResidual[cpnt][0] * QuantDivisor[qLevel]), 0, (1 << cpntBitDepth[cpnt]) - 1);
		else if ((hPos % SAMPLES_PER_UNIT) == 2)  // Third pixel of group
			p = CLAMP(a + (quantizedResidual[cpnt][0] + quantizedResidual[cpnt][1])*QuantDivisor[qLevel],
				0, (1 << cpntBitDepth[cpnt]) - 1);
		break;
	default:  // PT_BLOCK+ofs = BLOCK predictor, starts at -1
			  // *MODEL NOTE* MN_BLOCK_PRED
		bp_offset = (int)predType - (int)PT_BLOCK;
		p = currLine[MAX(hPos + PADDING_LEFT - 1 - bp_offset, 0)];
		break;
	}

	return p;
}
void Predictor_dec::BlockPredSearch(int cpnt, int currLine[3][SLICE_WIDTH + PADDING_LEFT + PADDING_RIGHT], int hPos, int recon_x)
{
	int i, j;
	int candidate_vector;  // a value of 0 maps to -1, 2 maps to -3, etc.
	int pred_x;
	int pixel_mod_cnt;
	int min_err;
	PRED_TYPE min_pred;
	int cursamp;
	int pixdiff;
	int bp_sads[BP_RANGE];
	int modified_abs_diff;

	// This function runs right after a reconstructed value is determined and computes the best predictor to use for the NEXT line.
	// An implementation could run the block prediction search at any time after that up until the point at which the selection is needed.
	// *MODEL NOTE* MN_BP_SEARCH

	if (hPos == 0)
	{
		// Reset prediction accumulators every line
		bpCount = 0;
		lastEdgeCount = 10;  // Arbitrary large value as initial condition
		for (i = 0; i<NUM_COMPONENTS; ++i)
		{
			for (j = 0; j<BP_SIZE; ++j)
			{
				for (candidate_vector = 0; candidate_vector<BP_RANGE; ++candidate_vector)
					lastErr[i][j][candidate_vector] = 0;
			}
		}
	}

	// Last edge count check - looks at absolute differences between adjacent pixels
	//   - Don't use block prediction if the content is basically flat
	pixdiff = SampToLineBuf(recon_x, cpnt) - SampToLineBuf(currLine[cpnt][hPos + PADDING_LEFT - 1], cpnt);
	pixdiff = ABS(pixdiff);
	if (cpnt == 0)
		edgeDetected = 0;
	if (pixdiff > (BP_EDGE_STRENGTH << (bits_per_component - 8)))
		edgeDetected = 1;
	if (cpnt == NUM_COMPONENTS - 1)
	{
		if (edgeDetected)
			lastEdgeCount = 0;
		else
			lastEdgeCount++;
	}

	// The BP
	cursamp = (hPos / PRED_BLK_SIZE) % BP_SIZE;
	pixel_mod_cnt = hPos % PRED_BLK_SIZE;
	for (candidate_vector = 0; candidate_vector<BP_RANGE; candidate_vector++)
	{
		if (pixel_mod_cnt == 0)
		{			// predErr is summed over PRED_BLK_SIZE pixels
			predErr[cpnt][candidate_vector] = 0;
		}

		pred_x = SamplePredict(currLine[cpnt], currLine[cpnt], hPos, (PRED_TYPE)(candidate_vector + PT_BLOCK), 0, cpnt);

		// HW uses previous line's reconstructed samples, which may be bit-reduced
		pred_x = SampToLineBuf(pred_x, cpnt);
		recon_x = SampToLineBuf(recon_x, cpnt);

		pixdiff = recon_x - pred_x;
		pixdiff = ABS(pixdiff);
		modified_abs_diff = MIN(pixdiff >> (cpntBitDepth[cpnt] - 7), 0x3f);
		// ABS differences are clamped to 6 bits each, predErr for 3 pixels is 8 bits
		predErr[cpnt][candidate_vector] += modified_abs_diff;
	}

	if (pixel_mod_cnt == PRED_BLK_SIZE - 1)
	{
		// Track last 3 3-pixel SADs for each component (each is 7 bit)
		for (candidate_vector = 0; candidate_vector<BP_RANGE; ++candidate_vector)
			lastErr[cpnt][cursamp][candidate_vector] = predErr[cpnt][candidate_vector];

		if (cpnt<NUM_COMPONENTS - 1)
			return;   // SAD is across all 3 components -- wait until we've processed all 3

		for (candidate_vector = 0; candidate_vector<BP_RANGE; ++candidate_vector)
		{
			bp_sads[candidate_vector] = 0;

			for (i = 0; i<BP_SIZE; ++i)
			{
				int sad3x1 = 0;

				// Use all 3 components
				for (j = 0; j<NUM_COMPONENTS; ++j)
					sad3x1 += lastErr[j][i][candidate_vector];
				// sad3x1 is 9 bits
				sad3x1 = MIN(511, sad3x1);

				bp_sads[candidate_vector] += sad3x1;  // 11-bit SAD
			}
			// Each bp_sad can have a max value of 63*9 pixels * 3 components = 1701 or 11 bits
			bp_sads[candidate_vector] >>= 3;  // SAD is truncated to 8-bit for comparison
		}

		min_err = 1000000;
		min_pred = PT_MAP;
		for (candidate_vector = 0; candidate_vector<BP_RANGE; ++candidate_vector)
		{
			if (candidate_vector == 1)
				continue;												// Can't use -2 vector
																		// Ties favor smallest vector
			if (min_err > bp_sads[candidate_vector])
			{
				min_err = bp_sads[candidate_vector];
				min_pred = (PRED_TYPE)(candidate_vector + PT_BLOCK);
			}
		}

		if (hPos >= 9)  // Don't start algorithm until 10th pixel
		{
			if (min_pred > PT_BLOCK)
				bpCount++;
			else
				bpCount = 0;
		}
		if ((bpCount >= 3) && (lastEdgeCount < 3))
			prevLinePred[hPos / PRED_BLK_SIZE] = (PRED_TYPE)min_pred;
		else
			prevLinePred[hPos / PRED_BLK_SIZE] = (PRED_TYPE)PT_MAP;
	}
}
void Predictor_dec::UpdateMidpoint(int currLine[3][SLICE_WIDTH + PADDING_LEFT + PADDING_RIGHT])
{
	int i;
	int UhPos;
	int Ucpnt;

	UhPos = hPos - PIXELS_PER_GROUP + 1;

	// Apply ICH decision to reconstructed line & update ICH values.  Let's do this in raster order.
	for (i = 0; i<PIXELS_PER_GROUP; ++i)
	{
		for (Ucpnt = 0; Ucpnt<NUM_COMPONENTS; ++Ucpnt)  // Loop over how much component data for this pixel
		{
			if (midpointSelected[Ucpnt])
				currLine[Ucpnt][UhPos + PADDING_LEFT] = midpointRecon[Ucpnt][i];
		}
		UhPos++;
		if (UhPos >= SLICE_WIDTH)
			return;
	}
}
int Predictor_dec::UsingMidpoint(int cpnt)
{
	int qlevel, max_size, req_size[SAMPLES_PER_UNIT];
	int i;

	// Determine required size for unit
	qlevel = MapQpToQlevel(masterQp, cpnt);
	max_size = 0;
	for (i = 0; i<SAMPLES_PER_UNIT; i++)
	{
		req_size[i] = FindResidualSize(quantizedResidual[cpnt][i]);
		max_size = MAX(req_size[i], max_size);
	}

	// Check if required size is bpc-qlevel ...
	if (max_size >= cpntBitDepth[cpnt] - qlevel)  // ...if so, just use midpoint predictor
		return (1);
	else
		return (0);
}
void Predictor_dec::VLDUnit(int cpnt)
{
	int size;
	int qlevel;
	int midpoint_selected;

	qlevel = MapQpToQlevel(qp.read(), cpnt);
	// *MODEL NOTE* MN_DEC_MPP_SELECT
	size = cpntBitDepth[cpnt];
	midpoint_selected = (size == cpntBitDepth[cpnt] - qlevel);
	useMidpoint[cpnt] = midpoint_selected;
}

void Predictor_dec::init()
{
	range[0] = 1 << 8;
	range[1] = 1 << 9;
	range[2] = 1 << 9;

	vPos = 0;
	hPos = 0;
}

void Predictor_dec::DSC_Algorithm()
{
	// line buffers have padding to left and right
	int lbufWidth = SLICE_WIDTH + PADDING_LEFT + PADDING_RIGHT; // pixels to left and right
	int i;
	int pred2use;
	int qlevel;
	int pred_x;
	int actual_x;
	int err_raw;
	int err_q;
	int maxval;
	int recon_x;
	int absErr;

	int midpoint_pred, midpoint_recon;
	int origWithinQerr[3];
	int leftRecon[3];
	sc_int <26> w_recon_x;

	getInput();
	if (vPos == 0 && hPos == 0)
	{
		// init curr,prev Line
		for (cpnt = 0; cpnt<NUM_COMPONENTS; cpnt++)
		{
			int initValue;

			initValue = 1 << (8 - 1);
			if (cpnt != 0)
				initValue *= 2;

			for (i = 0; i<lbufWidth; i++)
			{
				currLine[cpnt][i] = initValue;
				prevLine[cpnt][i] = initValue;
			}
		}
	}

	for (i = 0; i<NUM_COMPONENTS; ++i)
		VLDUnit(i);

	prevMasterQp = masterQp;

	done = 0;
	sampModCnt = 0;

	while (!done)
	{
		for (cpnt = 0; cpnt<NUM_COMPONENTS; cpnt++)
		{
			qlevel = MapQpToQlevel(qp.read(), cpnt);

			if (vPos == 0)
			{
				// Use left predictor.  Modified MAP doesn't make sense since there is no previous line.
				pred2use = PT_LEFT;
			}
			else
			{
				pred2use = prevLinePred[hPos / PRED_BLK_SIZE];
			}
			pred_x = SamplePredict(prevLine[cpnt], currLine[cpnt], hPos, pred2use, qlevel, cpnt);
			// Decoder takes error from bitstream
			err_q = quantizedResidual[cpnt][sampModCnt];

			qlevel = MapQpToQlevel(qp.read(), cpnt);

			// Use midpoint prediction if selected
			if (useMidpoint[cpnt])
				pred_x = FindMidpoint(cpnt, qlevel);

			// reconstruct
			// *MODEL NOTE* MN_IQ_RECON
			maxval = range[cpnt] - 1;
			recon_x = CLAMP(pred_x + (err_q << qlevel), 0, maxval);


			currLine[cpnt][hPos + PADDING_LEFT] = recon_x;
			if (cpnt == 0)
			{
				Y = recon_x;
			}
			else if (cpnt == 1)
			{
				Co = recon_x;
			}
			else
			{
				Cg = recon_x;
			}
		}
		if (hPos % 3 == 0)
		{
			w_recon_x = (Y, Co, Cg);
			recon_x_out[0].write(w_recon_x);
		}
		else if (hPos % 3 == 1)
		{
			w_recon_x = (Y, Co, Cg);
			recon_x_out[1].write(w_recon_x);
		}
		else if (hPos % 3 == 2)
		{
			w_recon_x = (Y, Co, Cg);
			recon_x_out[2].write(w_recon_x);
		}
		masterQp = qp.read();
		sampModCnt++;

		if ((sampModCnt >= PIXELS_PER_GROUP) || (hPos + 1 == SLICE_WIDTH))
		{
			// Pad partial group at the end of the line
			if (sampModCnt < PIXELS_PER_GROUP)
			{
				for (i = sampModCnt; i<PIXELS_PER_GROUP; ++i)
				{
					for (cpnt = 0; cpnt<NUM_COMPONENTS; cpnt++)
					{
						quantizedResidual[cpnt][i] = 0;
						quantizedResidualMid[cpnt][i] = 0;
					}
					hPos++;
				}
			}

			if ((hPos < SLICE_WIDTH - 1) || (vPos < SLICE_WIDTH - 1))
			{  // Don't decode if we're done
				for (i = 0; i<NUM_COMPONENTS; ++i)
					VLDUnit(i);

				prevMasterQp = masterQp;
			}
			sampModCnt = 0;
		}
		hPos++;

		if (hPos % 3 == 0)
		{
			int mod_hPos;
			// end of line
			// Update block prediction based on real reconstructed values
			for (mod_hPos = 0; mod_hPos<SLICE_WIDTH; ++mod_hPos)
			{
				for (cpnt = 0; cpnt < NUM_COMPONENTS; ++cpnt)
				{
					BlockPredSearch(cpnt, currLine, mod_hPos, currLine[cpnt][mod_hPos + PADDING_LEFT]);
				}
			}

			// reduce number of bits per sample in line buffer (replicate pixels in left/right padding)
			for (i = 0; i<lbufWidth; i++)
				for (cpnt = 0; cpnt<NUM_COMPONENTS; cpnt++)
					prevLine[cpnt][i] = SampToLineBuf(currLine[cpnt][CLAMP(i, PADDING_LEFT, PADDING_LEFT + SLICE_WIDTH - 1)], cpnt);

			for (i = 0; i < SLICE_WIDTH; i++)
			{
				Y = prevLine[0][PADDING_LEFT + i];
				Co = prevLine[1][PADDING_LEFT + i];
				Cg = prevLine[2][PADDING_LEFT + i];

				prevLine_out[i].write((Y, Co, Cg));
			}

			if (hPos >= SLICE_WIDTH)
			{
				vPos++;
				hPos = 0;
			}
			if (vPos >= SLICE_HEIGHT)
			{
				vPos = 0;
			}
			done = 1;
		}
	}
}