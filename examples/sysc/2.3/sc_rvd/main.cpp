// Simulation Tutorial
// 1-bit Adder

// This is to include the SystemC constructs and objects.
#include "systemc.h"
// We declare the 1-bit ADDER as a "SC_MODULE" with the input and
// output ports declared immediately after the module declarations
// This will add two bits together(a,b), with a carry in(cin) and
// output the sum(sum) and a carry out(cout).
SC_MODULE(BIT_ADDER)
{
	sc_in<sc_logic> a, b, cin;
	sc_out<sc_logic> sum, cout;	//In the constructor block of the module, declare a method called "process"
								//with a sensitivity list that includes a, b, & cin.
	SC_CTOR(BIT_ADDER)
	{
		SC_METHOD(process);
		sensitive << a << b << cin;
	}

	//Define the functionality for the "process" method in the 1-BIT adder
	void process()
	{
		//Declare variables of type "logic" to be used in calculations
		sc_logic aANDb, aXORb, cinANDaXORb;

		//Perform intermediate calculations for the 1-BIT adder
		//Note to read input from a port, one must use the format: "port_name.read()"
		aANDb = a.read() & b.read();
		aXORb = a.read() ^ b.read();
		cinANDaXORb = cin.read() & aXORb;

		//Calculate sum and carry out of the 1-BIT adder
		sum = aXORb ^ cin.read();
		cout = aANDb | cinANDaXORb;
	}

};   //Don't forget the semicolon!

