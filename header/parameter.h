//#define bits_per_pixel 80
#define NUM_BUF_RANGES 15
#define OVERFLOW_AVOID_THRESHOLD -172
#define MAX(X, Y) ((X)>(Y) ? (X) : (Y))
#define MIN(X, Y) ((X)<(Y) ? (X) : (Y))

#define RC_SCALE_BINARY_POINT 3
#define OFFSET_FRACTIONAL_BITS 11
#define PIXELS_PER_GROUP 3
#define MAX_UNITS_PER_GROUP 3
#define NUM_COMPONENTS 3