#include <systemc.h>
#include "../header/RC_functions.h"
#include "../header/RC_fsm.h"

SC_MODULE (RC_top){

	sc_in <bool> CLK;
	sc_in <bool> RST_N;
	sc_in <bool> start_en;
	sc_in <bool> completeVlc; // 3

	sc_in <int> numBits;
	sc_in <int> sampModCnt;
	sc_in <int> rc_model_size;
	sc_in <int> initial_offset; //7

	sc_in <int> group_count;
	sc_in <int> vPos;
	sc_in <int> scale_increment_interval; 
	sc_in <int> scale_decrement_interval;

	sc_in <int> rcSizeUnit[MAX_UNITS_PER_GROUP];
	sc_in <int> initial_scale_value;
	sc_in <int> first_line_bpg_ofs;
	sc_in <int> nfl_bpg_offset;

	sc_in <int> initial_xmit_delay;
	sc_in <int> slice_width;
	sc_in <int> slice_height;
	sc_in <int> pixelCount;

	sc_in <int> bits_per_pixel;
	sc_in <int> slice_bpg_offset;
	sc_in <int> rc_quant_incr_limit0;
	sc_in <int> rc_quant_incr_limit1;

	sc_out <bool> qp_ready;
	sc_out <int> new_quant;
	sc_out <int> bufferFullness;        
	sc_out <int> qLevel;
	sc_out <bool> forceMpp;				// to VLC unit

	sc_in <int> rc_edge_factor;
	sc_in <int> rc_tgt_offset_lo;
	sc_in <int> rc_tgt_offset_hi;
	sc_signal<unsigned> state_out;
	sc_signal<bool> buffer_complete;
	sc_signal <bool> ready;
	sc_signal <bool> range_ready;

	RC_fsm *rc_fsm;
	RC_functions *rc_func;
	void initial();
	SC_CTOR(RC_top){
		SC_THREAD(initial);
		rc_fsm = new RC_fsm("RC_FSM");
		rc_fsm->clk(CLK);
		rc_fsm->rst_n(RST_N);
		rc_fsm->start_en(start_en);
		rc_fsm->state_out(state_out);
		rc_fsm->buffer_complete(ready);
		rc_fsm->completeVlc(completeVlc);
		rc_fsm->range_ready(range_ready);

		rc_func = new RC_functions("RC_FUNC");
		rc_func->clk(CLK);
		rc_func->rst_n(RST_N);
		rc_func->state(state_out);
		rc_func->qLevel(qLevel);

		rc_func->output_ready(ready);
		rc_func->forceMpp(forceMpp);
		rc_func->completeVlc(completeVlc);
		rc_func->numBits(numBits);

		rc_func->bufferFullness(bufferFullness);
		rc_func->range_ready(range_ready);
		rc_func->sampModCnt(sampModCnt);
		rc_func->group_count(group_count);

		rc_func->vPos(vPos);
		rc_func->rc_model_size(rc_model_size);
		rc_func->initial_scale_value(initial_scale_value);
		rc_func->initial_offset(initial_offset);

		rc_func->scale_decrement_interval(scale_decrement_interval);
		rc_func->scale_increment_interval(scale_increment_interval);
		rc_func->first_line_bpg_ofs(first_line_bpg_ofs);
		rc_func->slice_width(slice_width);

		rc_func->slice_height(slice_height);
		rc_func->nfl_bpg_offset(nfl_bpg_offset);
		rc_func->initial_xmit_delay(initial_xmit_delay);
		rc_func->pixelCount(pixelCount);

		rc_func->bits_per_pixel(bits_per_pixel);
		rc_func->slice_bpg_offset(slice_bpg_offset);
		rc_func->rc_tgt_offset_lo(rc_tgt_offset_lo);
		rc_func->rc_tgt_offset_hi(rc_tgt_offset_hi);

		rc_func->rc_edge_factor(rc_edge_factor);
		rc_func->rc_quant_incr_limit0(rc_quant_incr_limit0);
		rc_func->rc_quant_incr_limit1(rc_quant_incr_limit1);
		rc_func->new_quant(new_quant);
		rc_func->qp_ready(qp_ready);

		for (int i = 0; i<MAX_UNITS_PER_GROUP; i++) {
			rc_func->rcSizeUnit[i](rcSizeUnit[i]);
		}
	}
};
