#include <systemc.h>

SC_MODULE(RC_fsm){

	sc_in <bool> clk;
	sc_in <bool> rst_n;
	sc_in <bool> start_en;
	sc_in <bool> range_ready;

	sc_out <unsigned> state_out;
	sc_in <bool> buffer_complete;
	sc_in <bool> completeVlc;

	void state_machine();
	enum {
		initial,
		BufferTracker,
		CompleteVlc,
		LinearTrans,
		LongTerm,
		ShortTerm
	} curr_state;

	SC_CTOR(RC_fsm){
		SC_METHOD(state_machine);
			sensitive << clk.pos();
			sensitive << rst_n.neg();
	}

};
