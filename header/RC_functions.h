#include <systemc.h>
#include "parameter.h"
SC_MODULE(RC_functions)
{
	sc_in <bool> clk;
	sc_in <bool> rst_n;
	sc_in <unsigned> state;
	sc_in <int> numBits; 				// from VLC unit( when VLC is complete)

	sc_in <bool> completeVlc;
	sc_in <int> sampModCnt;
	//Need to initialize
	sc_in <int> rc_model_size;
	sc_in <int> initial_scale_value;

	sc_in <int> initial_offset;
	sc_in <int> group_count;
	sc_in <int> vPos;
	sc_in <int> scale_decrement_interval;

	sc_in <int> scale_increment_interval;
	sc_in <int> slice_width;
	sc_in <int> slice_height;
	sc_in <int> first_line_bpg_ofs;

	sc_in <int> nfl_bpg_offset;
	sc_in <int> pixelCount;
	sc_in <int> initial_xmit_delay;
	sc_in <int> slice_bpg_offset;

	sc_in <int> rc_tgt_offset_lo;
	sc_in <int> rc_tgt_offset_hi;
	sc_in <int> rc_edge_factor;

	sc_out <int> qLevel;
	sc_out <bool> output_ready;
	sc_out <int> bufferFullness;
	sc_out <bool> forceMpp;				// to VLC unit
	
	sc_in <int> rc_quant_incr_limit0;
	sc_in <int> rc_quant_incr_limit1;
	sc_out <bool> range_ready;

	sc_signal <int> overflowAvoid;
	sc_signal <int> curr_numBits;
	sc_signal <int> prev_numBits;
	sc_signal <int> codedGroupSize;
	sc_signal <int> maxBitsPerGroup;

	sc_in <int> rcSizeUnit[MAX_UNITS_PER_GROUP];
	int rcSizeGroup;
	sc_signal <int> bugFixCondition;
	sc_signal <int> adjFullness;
	sc_signal <int> rcModelBufferFullness;
	sc_signal <int> scale;
	sc_signal <int> test;
	int throttle_offset;
	sc_signal <int> qLevel_tmp;
	sc_signal <int> i;
	sc_signal <int> prevRage;
	sc_signal <int> compare_idx_max;
	sc_signal <int> compare_idx_min;
	sc_signal <int> selected_range;
	sc_signal <int> prev2Qp;
	sc_signal <int> rcSizeGroupPrev;
	//Initial condition
	sc_signal <int> rc_buf_thresh[NUM_BUF_RANGES - 1];
	sc_signal <int> rc_range_parameters[NUM_BUF_RANGES];
	sc_signal <int> range_bpg_offset[NUM_BUF_RANGES];
	sc_signal <int> range_min_qp[NUM_BUF_RANGES];
	sc_signal <int> range_max_qp[NUM_BUF_RANGES];

	int rcTgtBitsGroup;
	int tgtMinusOffset;
	int tgtPlusOffset;
	int incr_amount;

	int stQp;
	sc_signal <int> curQp;
	sc_signal <int> prevQp;
	sc_out <int> new_quant;
	sc_out <bool> qp_ready;

	sc_in <int> bits_per_pixel;
	sc_signal<int> min_QP;
	sc_signal<int> max_QP;
	sc_signal<int> prevPixelCount;
	int rcXformOffset;
	sc_signal<int> bpg_offset;
	int throttleFrac;
	int currentScale;
	bool scaleIncrementStart;
	int scaleAdjustCounter;
	int current_bpg_target;
	int increment;
	int unity_scale;
	int num_pixels;

	/*sc_in <int> numBitsChunk;			//
	sc_in <int> chunk_size;				//
	sc_in <int> bufferFullness;			// when this value can get initial state.
	sc_in <bool> vbr_enable;			// initial condition
	sc_in <int> pixelCount;				//
	*/

	void output();
	void BufferTracker();
	void Initial();
	void LinearTrans_Initial();
	void LinearTrans();
	void RefreshBufferFullness();
	void LongTerm();
	void calculate();
	void ShortTerm_store();
	void ShortTerm_initial();
	void ShortTerm();
	SC_CTOR (RC_functions)
	{
		SC_THREAD(Initial);
		SC_METHOD(RefreshBufferFullness);
			sensitive << state;
		SC_METHOD(BufferTracker);
			sensitive << completeVlc << numBits<<state;
		SC_METHOD (calculate);
			sensitive << clk.pos();
			sensitive_neg(rst_n);
		SC_METHOD(LinearTrans_Initial);
			sensitive << state;
		SC_METHOD(LinearTrans);
			sensitive_neg(rst_n);
			sensitive << clk.pos();
		SC_METHOD(LongTerm);
			sensitive << clk.pos();
		SC_METHOD(ShortTerm_initial);
			sensitive << state;
		SC_METHOD(ShortTerm_store);
			sensitive << clk.pos();
		SC_METHOD(ShortTerm);
			sensitive << clk.pos();
			
	}
};
