#include <systemc.h>
#include "../header/RC_top.h"
SC_MODULE(codec_main) {
	sc_in <bool> CLK;
	sc_in <bool> RST_N;
	sc_in <bool> start_en;
	sc_in <bool> completeVlc;

	sc_in <int> numBits;
	sc_in <int> sampModCnt;
	sc_in <int> rcModelSize;
	sc_in <int> initial_offset;

	sc_in <int> groupsPerLine;
	sc_in <int> vPos;
	sc_in <int> scale_increment_interval;
	sc_in <int> firstLineBpgOfs;


	sc_signal <int> rcSizeUnit[MAX_UNITS_PER_GROUP];

	sc_in <int> slice_width;
	sc_in <int> slice_height;
	sc_in <int> initial_xmit_delay;
	sc_in <int> pixelCount;
	
	sc_in <int> bitsPerPixel;
	sc_in <int> num_extra_mux_bits;

	sc_in <int> rc_edge_factor;
	sc_in <int> rc_tgt_offset_lo;
	sc_in <int> rc_tgt_offset_hi;
	sc_out <int> qLevel;

	sc_out <bool> ForceMpp;				// to VLC unit
	sc_out <int> bufferFullness;
	sc_in <int> rc_quant_incr_limit0;
	sc_in <int> rc_quant_incr_limit1;
	sc_out <int> new_quant;
	sc_out <bool> qp_ready;
	sc_signal <int> groups_total;
	sc_signal <int> slice_bpg_offset;
	sc_signal <int> bits_per_pixel;
	sc_signal <int> first_line_bpg_ofs;
	sc_signal <int> nfl_bpg_offset;
	sc_signal <int> initial_scale_value;
	sc_signal <int> scale_decrement_interval;
	sc_signal <int> groups_per_line;
	sc_signal <int> group_count;
	int target_bpp_x16;

	void initial();
	// Module instantiation
	RC_top *rc_top;
	SC_CTOR(codec_main) {
		SC_CTHREAD(initial,CLK.pos());
		rc_top = new RC_top("RC_TOP");

		rc_top->forceMpp(ForceMpp);
		rc_top->CLK(CLK);
		rc_top->RST_N(RST_N);
		rc_top->start_en(start_en);
		rc_top->completeVlc(completeVlc);

		rc_top->numBits(numBits);
		rc_top->initial_offset(initial_offset);
		rc_top->sampModCnt(sampModCnt);
		rc_top->group_count(group_count);
		
		rc_top->rc_model_size(rcModelSize);
		rc_top->vPos(vPos);
		rc_top->scale_increment_interval(scale_increment_interval);
		rc_top->first_line_bpg_ofs(first_line_bpg_ofs);

		rc_top->nfl_bpg_offset(nfl_bpg_offset);
		rc_top->slice_height(slice_height);
		rc_top->slice_width(slice_width);
		rc_top->initial_xmit_delay(initial_xmit_delay);

		rc_top->pixelCount(pixelCount);
		rc_top->qLevel(qLevel);
		rc_top->bufferFullness(bufferFullness);
		rc_top->initial_scale_value(initial_scale_value);

		rc_top->bits_per_pixel(bits_per_pixel);
		rc_top->scale_decrement_interval(scale_decrement_interval);
		rc_top->slice_bpg_offset(slice_bpg_offset);
		rc_top->rc_tgt_offset_lo(rc_tgt_offset_lo);

		rc_top->rc_edge_factor(rc_edge_factor);
		rc_top->rc_tgt_offset_hi(rc_tgt_offset_hi);
		rc_top->rc_quant_incr_limit0(rc_quant_incr_limit0);
		rc_top->rc_quant_incr_limit1(rc_quant_incr_limit1);
		rc_top->new_quant(new_quant);
		rc_top->qp_ready(qp_ready);

		for (int i = 0; i<MAX_UNITS_PER_GROUP; i++) {
			rc_top->rcSizeUnit[i](rcSizeUnit[i]);
		}
	}	
};
