#include "VLC.h"
#include "PrefixParameter.h"

void VLC::StateMachine()
{
	//LOG(cl++ << "\t@VLC ");
	if (!reset)
	{
		//LOG("reset signal - next: INIT" << endl);
		state = INIT;
	}
	else
	{
		switch (state)
		{
		case VLC::IDLE:
			//LOG(": IDLE");
			if (enable_in.read() == true)// endable signal
			{
				//LOG(" - next: RUN");
				state = RUN;
			}
			break;
		case VLC::INIT:
			//LOG(": INIT - next: IDLE");
			Init();
			state = IDLE;
			break;
		case VLC::RUN:
			//LOG(": RUN");
			if (calculate_done == true)
			{
				//LOG(" - next: TRANSMIT");
				state = TRANSMIT;
				calculate_done = false;
				mode_select_out.write(~ich_selected);
				ich_enable_out.write(true);
			}
			else
			{
				Run();
			}
			break;
		case VLC::TRANSMIT:
			//LOG(": TRANSMIT");
			if (transmit_done == true)
			{	
				//LOG(" - next: WAIT");
				state = WAIT;
				transmit_done = false;
				ich_enable_out.write(false);
				MUX_enable_out.write(true);
			}
			else
			{
				Transmit();
			}
			break;
		case VLC::WAIT:
			//LOG(": WAIT");
			MUX_enable_out.write(false);
			if (VLC_ready_in.read() == true)
			{
				//LOG(" - next: IDLE");
				state = IDLE;
			}
			break;
		default:
			break;
		}
		//LOG(endl);
	}
}

void VLC::Init()
{
	if (state == INIT)
	{

		calculate_done = false;
		transmit_done = false;


		for (int i = 0; i < PIXELS_PER_GROUP; i++)
		{
			for (int j = 0; j < NUM_COMPONENTS; j++)
			{
				residue[i][j] = 0;
			}
			calculate_stage[i] = 0;
			qlevel[i] = 0;
			qlevel_old[i] = 0;
			previous_num_bits[i] = 0;
			required_size[i] = 0;
			predicted_size[i] = 0;
			ich[i] = 0;
			out_amount[i] = -1;
			transmit_iterator = 0;
		}

		prev_ich_selected = false;
		ich_selected = false;
		max_size = 0;
		force_mpp = 0;
		LOG("init done" << endl);
	}
}

void VLC::Run()
{
	if (state == RUN)
	{

			//set residue in array for calculate easier
			residue[0][0] = P_mod_in[0].read().range(25, 18).to_int();
			residue[0][1] = P_mod_in[0].read().range(17, 9).to_int();
			residue[0][2] = P_mod_in[0].read().range(8, 0).to_int();
			residue[1][0] = P_mod_in[1].read().range(25, 18).to_int();
			residue[1][1] = P_mod_in[1].read().range(17, 9).to_int();
			residue[1][2] = P_mod_in[1].read().range(8, 0).to_int();
			residue[2][0] = P_mod_in[2].read().range(25, 18).to_int();
			residue[2][1] = P_mod_in[2].read().range(17, 9).to_int();
			residue[2][2] = P_mod_in[2].read().range(8, 0).to_int();
			ich[0] = ICH_mod_in[0].read().to_int();
			ich[1] = ICH_mod_in[1].read().to_int();
			ich[2] = ICH_mod_in[2].read().to_int();
			qp_ = qp_in.read().to_int();
			ich_selected = 0;
			//LOG("mode: "<<ich_selected<<endl);


		for (int i = 0; i < NUM_COMPONENTS; i++)
		{
			//qlevel update

				//LOG("component: "<<cl << endl);
				qlevel[i] = MapQpToQlevel(qp_, i);
				//LOG("qlevel: "<<qlevel[cl] << endl);
				adj_predicted_size[i] = GetQpAdjPredSize(i);
				//LOG("predicted size: "<<adj_predicted_size[cl] << endl);
				max_size = 0;
				prefix_value = 0;
				size = 0;
				calculate_stage[i] = 2;

				if ((i == 0) && (ich_use_in.read() == true) && !force_mpp)
				{
					if (prev_ich_selected)
					{
						bits_ich_mode = 1;
					}
					else
					{
						bits_ich_mode = (BIT_DEPTH + 1 - qlevel[0]) - adj_predicted_size[i];
					}
					bits_ich_mode += ICH_BITS * PIXELS_PER_GROUP;

					bits_p_mode = EstimateBitsForGroup();

					if ((ich_err_in.read() <= p_err_in.read() &&
						(bits_ich_mode + 4 * ich_err_in.read() < bits_p_mode + 4 * p_err_in.read())))
						ich_selected = true;
				}



			//todo: flatness control

			// Nothing to do for other components once ICH selected
			if ((i > 0) && (ich_selected == true))
			{
				break;
			}

			// figure out required size for unit
			for (int j = 0; j<SAMPLES_PER_UNIT; j++)
			{
				required_size[j] = FindResidualSize(residue[j][i]);
				max_size = MAX(required_size[j], max_size);
			}
			// MPP
			if (force_mpp || (max_size >= BIT_DEPTH_F(i) - qlevel[i]))  // ...if so, just use midpoint predictor
			{
				max_size = BIT_DEPTH_F(i) - qlevel[i];
				for (int j = 0; j<SAMPLES_PER_UNIT; ++j)
					required_size[j] = max_size;
			}

			if (adj_predicted_size[i] < max_size)
			{
				prefix_value = max_size - adj_predicted_size[i];
				size = max_size;
			}
			else
			{
				prefix_value = 0; // size did not grow
				size = adj_predicted_size[i];
			}

			// Escape code becomes size=0 if previous group was ICH mode, all other values get boosted by 1
			if (i == 0)
				prefix_value += prev_ich_selected;

			int max_pfx_size = BIT_DEPTH_F(i) - qlevel[i] + (i == 0) - adj_predicted_size[i];
			//LOG("adj_predicted_size: " << adj_predicted_size[i] << endl);
			//ICH-mode
			if ((ich_selected == true) && (i == 0))
			{
				//LOG("ICH-mode selected  " << "prev:" << prev_ich_selected << endl);
				int alt_pfx = (BIT_DEPTH + 1 - qlevel[i]) - adj_predicted_size[i];
				if (prev_ich_selected)
				{
					AddBits(i, 1,/*alt_pfx + */1);
					//LOG(i << " [" << (/*alt_pfx + */1) << "]"<<endl);
				}
				else
				{
					AddBits(i, 0, alt_pfx);
					//LOG(i << " [" << (alt_pfx) << "]" << endl);
				}
				for (int j = 0; j < PIXELS_PER_GROUP; ++j)
				{
					AddBits(j, ich[j], ICH_BITS);
					//LOG(j << " [" << ICH_BITS << "]" << endl);
				}
			}
			//P-mode
			else if (ich_selected == false)
			{
				//LOG("P-mode selected" << endl);
				if (prefix_value == max_pfx_size)  // Trailing "1" can be omitted
				{
					AddBits(i, 0, max_pfx_size);
					//LOG(i << " [" << max_pfx_size << "]" << endl);
				}
				else
				{
					AddBits(i, 1, prefix_value + 1);
					//LOG(i << " [" << prefix_value << "]" << endl);
				}
				for (int j = 0; j < SAMPLES_PER_UNIT; ++j)
				{
					AddBits(i, residue[j][i], size);
					//LOG(i << " [" << size << "]" << endl);
				}

				//prev qlevel save
				qlevel_old[i] = qlevel[i];
				predicted_size[i] = PredictSize(required_size);;
			}
			if (i == 0)
				prev_ich_selected = ich_selected;
		}
		calculate_done = true;
	}
}

void VLC::Transmit()
{
	if (state == TRANSMIT)
	{
		//LOG("interation: " << transmit_iterator << endl);
		for (int i = 0; i < UNITS_PER_GROUP; i++)
		{
			if (transmit_iterator <= (out_amount[i]+1))
			{
				if (out[i][transmit_iterator] == 1)
					unit_out[i] = "1";
				else if (out[i][transmit_iterator] == 0)
					unit_out[i] = "0";
				else
					unit_out[i] = "x";
			}
			else
				unit_out[i] = "x";
		}

		//LOG("INTER VALUE:\tY: " << out[0][transmit_iterator] << "\tCo: " << out[1][transmit_iterator] << "\tCg: " << out[2][transmit_iterator] << endl);
		for (int i = 0; i < UNITS_PER_GROUP; i++)
			out[i][transmit_iterator] = -10;
		transmit_iterator++;
		//LOG("OUTPUT:\t\tY: " << unit_out[0].read() << "\tCo: " << unit_out[1].read() << "\tCg: " << unit_out[2].read() << endl);

		if ((transmit_iterator > (out_amount[0]+1)) && (transmit_iterator > (out_amount[1]+1)) && (transmit_iterator > (out_amount[2]+1)))
		{
			transmit_done = true;
			transmit_iterator = 0;
			out_amount[0] = -1;
			out_amount[1] = -1;
			out_amount[2] = -1;
		}
	}
}

int VLC::MapQpToQlevel(int qp, int cpnt)
{
	int qlevel_luma_8bpc[] = { 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 7 };
	int qlevel_chroma_8bpc[] = { 0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 8, 8 };
	if (cpnt == 0)
		return qlevel_luma_8bpc[qp];
	else
	{
		return qlevel_chroma_8bpc[qp];

	}
}

int VLC::GetQpAdjPredSize(int cpnt)
{
	int pred_size, max_size;

	// *MODEL NOTE* MN_DSU_SIZE_PREDICTION
	pred_size = predicted_size[cpnt];

	pred_size += qlevel_old[cpnt] - qlevel[cpnt];
	max_size = BIT_DEPTH_F(cpnt) - qlevel[cpnt];
	pred_size = CLAMP(pred_size, 0, max_size - 1);

	return (pred_size);
}

int VLC::FindResidualSize(int eq)
{
	int size_e;

	// Find the size in bits of e
	if (eq == 0) size_e = 0;
	else if (eq >= -1 && eq <= 0) size_e = 1;
	else if (eq >= -2 && eq <= 1) size_e = 2;
	else if (eq >= -4 && eq <= 3) size_e = 3;
	else if (eq >= -8 && eq <= 7) size_e = 4;
	else if (eq >= -16 && eq <= 15) size_e = 5;
	else if (eq >= -32 && eq <= 31) size_e = 6;
	else if (eq >= -64 && eq <= 63) size_e = 7;
	else if (eq >= -128 && eq <= 127) size_e = 8;
	else if (eq >= -256 && eq <= 255) size_e = 9;
	else if (eq >= -512 && eq <= 511) size_e = 10;
	else if (eq >= -1024 && eq <= 1023) size_e = 11;
	else if (eq >= -2048 && eq <= 2047) size_e = 12;
	else if (eq >= -4096 && eq <= 4095) size_e = 13;
	else size_e = 14;

	return size_e;
}

void VLC::AddBits(int cpnt, int value, int bit_num)
{
	//LOG("COMPONET: " << cpnt << "  DATA : " << value << '\t' << "BITS: ["<<bit_num<<"]\t");
	int value_ = value;
	for (int i = 0; i < bit_num; i++)
	{
		++out_amount[cpnt];
		//LOG("(" << out_amount[cpnt]<<")");
		out[cpnt][out_amount[cpnt]] = ((value_ >> (bit_num - (i + 1))) && 1);
		value_ -= (out[cpnt][out_amount[cpnt]] << (bit_num - (i + 1)));
		//LOG(out[cpnt][out_amount[cpnt]] << "\t");
	}
	//LOG(endl);
	//add to output array
}

int VLC::PredictSize(int *req_size)
{
	int pred_size;

	pred_size = 2;  // +0.5 for rounding
	pred_size += req_size[0] + req_size[1];
	pred_size += 2 * req_size[2];
	pred_size >>= 2;

	return (pred_size);
}

int VLC::EstimateBitsForGroup()
{
	int i, cpnt;
	int max_size[NUM_COMPONENTS];
	int total_size;
	int qlevel[NUM_COMPONENTS], size, qerr;
	int max_residual_size;
	int pred_size;

	max_size[0] = max_size[1] = max_size[2] = 0;

	qlevel[0] = MapQpToQlevel(qp_, 0);
	qlevel[1] = qlevel[2] = MapQpToQlevel(qp_, 1);

	for (cpnt = 0; cpnt < NUM_COMPONENTS; ++cpnt)
	{
		for (i = 0; i<PIXELS_PER_GROUP; ++i)
		{
			qerr = residue[i][cpnt];		// use real P-mode residual sizes
			size = FindResidualSize(qerr);
			if (size > max_size[cpnt])
				max_size[cpnt] = size;
		}
		max_residual_size = BIT_DEPTH_F(cpnt) - qlevel[cpnt];
		if (max_size[cpnt] > max_residual_size)
			max_size[cpnt] = max_residual_size;
	}

	total_size = 0;
	for (cpnt = 0; cpnt < NUM_COMPONENTS; ++cpnt)
	{
		pred_size = GetQpAdjPredSize(cpnt);
		if (max_size[cpnt] < pred_size)
			total_size += 1 + SAMPLES_PER_UNIT * pred_size;
		else if ((max_size[cpnt] == BIT_DEPTH_F(cpnt) - qlevel[cpnt]) && (cpnt != 0)) //Only do for Chroma
			total_size += (max_size[cpnt] - pred_size) + SAMPLES_PER_UNIT * max_size[cpnt];
		else
			total_size += 1 + (max_size[cpnt] - pred_size) + SAMPLES_PER_UNIT * max_size[cpnt];

	}
	//If previous group was ICH and luma is not maxsize, add another bit
	if ((max_size[0] < BIT_DEPTH_F(0) - qlevel[0]) && prev_ich_selected)
		total_size += 1;

	return (total_size);
}