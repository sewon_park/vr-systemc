#include "PrefixParameter.h"
#include "MUX.h"

void MUX::StateMachine()
{
	//LOG(cl++ << "\t@MUX ");
	if (reset.read() == false)
	{
		//LOG("reset signal - next: INIT" << endl);
		state = INIT;
	}
	else
	{
		switch (state)
		{
		case MUX::IDLE:
			//LOG(": IDLE");
			VLC_ready_out.write(false);
			if (enable.read() == true)
			{
				state = RUN;
			}
			else
			{
				Idle();
			}
			break;
		case MUX::INIT:
			//LOG(": INIT");
			Initialize();
			state = IDLE;
			break;
		case MUX::RUN:
			//LOG(": RUN");
			if (calculate_done == true)
			{
				calculate_done = false;
				state = TRANSMIT;
			}
			else
			{
				Run();
			}
			break;
		case MUX::TRANSMIT:
			//LOG(": TRANSMIT");
			if (transmit_done == true)
			{
				//LOG(" - next: IDLE");
				state = IDLE;
				transmit_done = false;
				VLC_ready_out.write(true);
			}
			else
			{
				Transmit();
			}
			break;
		default:
			break;
		}
	}
	//LOG(endl);
}

void MUX::Idle()
{
	for (int i = 0; i < UNITS_PER_GROUP; i++)
	{
		if (fifo_in[i].read() == "1")
		{
			FifoPutBits(&(enc_balance_fifo[i]), 1, 1);
			//LOG("fifo[" << i << "]= " << fifo_in[i].read() << " [" << enc_balance_fifo[i].fullness << "]\t");
		}
		else if (fifo_in[i].read() == "0")
		{
			FifoPutBits(&(enc_balance_fifo[i]), 0, 1);
			//LOG("fifo[" << i << "]= " << fifo_in[i].read() << " [" << enc_balance_fifo[i].fullness << "]\t");
		}
		else
		{
			if ((backup_fullness[i] != enc_balance_fifo[i].fullness))
			{
				if((backup_fullness[i] < enc_balance_fifo[i].fullness))
				FifoPutBits(&(se_size_fifo[i]), (enc_balance_fifo[i].fullness - backup_fullness[i]), 6);
				//LOG("{update : " << i << " size: " << (enc_balance_fifo[i].fullness - backup_fullness[i]) << "}  ");
				backup_fullness[i] = enc_balance_fifo[i].fullness;
			}
			//LOG("fifo[" << i << "]= X" << " [" << enc_balance_fifo[i].fullness << "]\t");
		}
		//LOG("[[" << se_size_fifo[i].fullness << "]]");
		fifo_size_out[i] = enc_balance_fifo[i].fullness;
	}
	//LOG(endl);
}

void MUX::Run()
{
	if (muxing_mode.read() == true)
	{
		//if (group_count.read() > MUX_WORD_SIZE + MAX_SE_SIZE - 3)
		if (group_count_++ > MUX_WORD_SIZE + MAX_SE_SIZE - 3)
		{
			ProcessGroupEnc(buf_out);
			calculate_done = true;
		}
		else
		{
			calculate_done = true;
		}
	}
	else
	{
		WriteEntryToBitstream(buf_out);
		calculate_done = true;
	}
}

void MUX::Initialize()
{
	if (state == INIT)
	{
		for (int i = 0; i < UNITS_PER_GROUP; i++)
		{
			FifoInit(&(se_size_fifo[i]),'s',(int)(6*(MUX_WORD_SIZE+MAX_SE_SIZE-1)+7)/8);
			FifoInit(&(enc_balance_fifo[i]),'e', (int)(((MUX_WORD_SIZE+MAX_SE_SIZE-1)*MAX_SE_SIZE)+7)/8);
			FifoInit(&(dec_model_fifo[i]),'d', (int)(MUX_WORD_SIZE + MAX_SE_SIZE + 7) / 8);
			backup_fullness[i] = 0;
		}
		transmit_done = false;
		bit_counter = 0;
		bit_counter_ = 0;
		group_count_ = 0;
		if (rate_buffer == NULL)
		{
			rate_buffer = fopen("rate_buffer.txt", "w");
		}
	}
}

void MUX::Transmit()
{
	if (state == TRANSMIT)
	{
		bitstream = buf_out;

		int bitcntmod8;
		int bufidx;
		int bit;

		for (bit_counter_; bit_counter_ < bit_counter; bit_counter_++)
		{
			//if ((bit_counter_ % 8) == 0)
				//LOG(endl);
			bitcntmod8 = (bit_counter_) % 8;
			bufidx = (bit_counter_) >> 3;
			bit = buf_out[bufidx] >> (7 - bitcntmod8);
			bit &= 1;
			if (bit == 1)
			{
				fprintf(rate_buffer, "1");
				//LOG("1");
			}
			else if (bit == 0)
			{
				fprintf(rate_buffer, "0");
				//LOG("0");
			}
			else
			{
				fprintf(rate_buffer, "x");
				//LOG("0");
			}
		}
		//LOG(endl);
		transmit_done = true;
	}
}

void MUX::ProcessGroupEnc(unsigned char *buf)
{
	int i, j;
	unsigned char d;
	int max_se_size[NUM_COMPONENTS];
	int sz;

	max_se_size[0] = BIT_DEPTH_F(0) * 4 + 4;  // prefix + code + up to 3 bits for flatness
	max_se_size[1] = BIT_DEPTH_F(1) * 4;  // prefix omitted
	max_se_size[2] = BIT_DEPTH_F(2) * 4;

	// Check to see if mux words are needed and feed the FIFO's
	for (i = 0; i<NUM_COMPONENTS; ++i)
	{

		if (dec_model_fifo[i].fullness < max_se_size[i])
		{
			for (j = 0; j<(MUX_WORD_SIZE / 8); ++j)
			{
				sz = enc_balance_fifo[i].fullness;
				if (sz >= 8)
					d = FifoGetBits(&(enc_balance_fifo[i]), 8, 0);
				else if (sz > 0)
					d = FifoGetBits(&(enc_balance_fifo[i]), sz, 0) << (8 - sz);
				else
					d = 0;
				FifoPutBits(&(dec_model_fifo[i]), d, 8);
				//("[" << dec_model_fifo[i].fullness << "] {"<<enc_balance_fifo[i].fullness <<"}" );
				PutBits(d, 8, buf);
			}
		}
	}

	// Virtual decoder
	for (i = 0; i<NUM_COMPONENTS; ++i)
	{
		sz = FifoGetBits(&(se_size_fifo[i]), 6, 0);
		//LOG(i << " " << sz << " " << dec_model_fifo[i].fullness <<endl);
		FifoGetBits(&(dec_model_fifo[i]), sz, 0);        // Remove one SE
	}
}

/*! \param dsc_cfg   DSC configuration structure
\param dsc_state Current DSC state
\param buf       Pointer to output buffer */
void MUX::WriteEntryToBitstream(unsigned char *buf)
{
	int i;
	int sz;
	unsigned int d;

	for (i = 0; i<UNITS_PER_GROUP; ++i)
	{
		sz = FifoGetBits(&(se_size_fifo[i]), 6, 0);
		if (sz>32)
		{
			d = FifoGetBits(&(enc_balance_fifo[i]), 32, 0);
			PutBits(d, 32, buf);
			sz -= 32;
		}
		d = FifoGetBits(&(enc_balance_fifo[i]), sz, 0);
		if (enc_balance_fifo[i].fullness != 0)
			LOG(enc_balance_fifo[i].name<<" Error: shifter underflow\n");
		PutBits(d, sz, buf);
	}
}

/*! \param fifo		 Pointer to FIFO data structure
\param size		 Specifies FIFO size in bytes */
void MUX::FifoInit(fifo_t *fifo, char name, int size)
{
	fifo->name = name;
	fifo->data = (unsigned char *)malloc(sizeof(unsigned char) * size);
	fifo->size = size * 8;
	fifo->fullness = 0;
	fifo->read_ptr = fifo->write_ptr = 0;
	fifo->max_fullness = 0;
	fifo->bits_added = 0;
}

/*! \param fifo		Pointer to FIFO data structure */
void MUX::FifoFree(fifo_t *fifo)
{
	free(fifo->data);
}

/*! \param fifo		Pointer to FIFO data structure
\param n		Number of bits to retrieve
\param sign_extend Flag indicating to extend sign bit for return value
\return			Value from FIFO */
int MUX::FifoGetBits(fifo_t *fifo, int n, int sign_extend)
{
	unsigned int d = 0;
	int i, pad = 0;
	unsigned char b;
	int sign = 0;

	if (fifo->fullness < n)
	{
		LOG(fifo->name << " FIFO underflow!\n");
		exit(1);
	}
	if (fifo->fullness < n)
	{
		pad = n - fifo->fullness;
		n = fifo->fullness;
	}

	for (i = 0; i<n; ++i)
	{
		b = (fifo->data[fifo->read_ptr / 8] >> (7 - (fifo->read_ptr % 8))) & 1;
		if (i == 0) sign = b;
		d = (d << 1) + b;
		fifo->fullness--;
		fifo->read_ptr++;
		if (fifo->read_ptr >= fifo->size)
			fifo->read_ptr = 0;
	}

	if (sign_extend && sign)
	{
		int mask;
		mask = (1 << n) - 1;
		d |= (~mask);
	}
	return (d << pad);
}

/*! \param fifo		Pointer to FIFO data structure
\param d		Value to add to FIFO
\param n		Number of bits to add to FIFO */
void MUX::FifoPutBits(fifo_t *fifo, unsigned int d, int nbits)
{
	int i;
	unsigned char b;

	if (fifo->fullness + nbits > fifo->size)
	{
		LOG(fifo->name <<" FIFO overflow!\n");
		exit(1);
	}

	fifo->bits_added += nbits;
	fifo->fullness += nbits;
	for (i = 0; i<nbits; ++i)
	{
		b = (d >> (nbits - i - 1)) & 1;
		if (!b)
			fifo->data[fifo->write_ptr / 8] &= ~(1 << (7 - (fifo->write_ptr % 8)));
		else
			fifo->data[fifo->write_ptr / 8] |= (1 << (7 - (fifo->write_ptr % 8)));
		fifo->write_ptr++;
		if (fifo->write_ptr >= fifo->size)
			fifo->write_ptr = 0;
	}
	if (fifo->fullness > fifo->max_fullness)
		fifo->max_fullness = fifo->fullness;
}

/*! \param val		 Value to write
\param size		 Number of bits to write
\param buf       Pointer to buffer location
\param bit_count Bit index into buffer (modified) */
void MUX::PutBits(int val, int size, unsigned char *buf)
{
	int bit_count = bit_counter;
	int i;
	int curbit;
	int bitcntmod8;
	int bufidx;

	if (size > 32)
		LOG("error: putbits supports max of 32 bits\n");
	//LOG("Bitstream[" << size << "]:\t");
	for (i = size - 1; i >= 0; --i)
	{
		bitcntmod8 = (bit_count) % 8;
		bufidx = (bit_count) >> 3;
		curbit = (val >> i) & 1;
		if (bitcntmod8 == 0)
			buf[bufidx] = 0;		// Zero current byte
		if (curbit)
		{
			buf[bufidx] |= (1 << (7 - bitcntmod8));
			//LOG("1");
		}
		else
		{
			//LOG("0");
		}
		bit_count++;
		bit_counter++;
	}
	//LOG(endl);
	//LOG(bit_counter << endl);
}