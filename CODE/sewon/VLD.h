#pragma once
#include "PrefixParameter.h"
#include "systemc.h"

SC_MODULE(VLD)
{
	//input
	sc_in_clk clk;//clock
	sc_in<bool> reset;//reset signal
	sc_in<bool> enable;//enable signal - from DEMUX
	sc_in<sc_uint<4>> qp_in;//quantization parameter - from RC
	sc_in<sc_lv<1>> unit_in[UNITS_PER_GROUP];// bit stream data - from DEMUX
	sc_in<bool> ich_done_in;//ICH is ready to next value

	//output
	sc_out<bool> mode_select_out;// 1:P-mode, 0:ICH_mode - to ICH module
	sc_out<sc_uint<26>> P_mod_out[PIXELS_PER_GROUP];// residue - to Predictor module
	sc_out<sc_uint<5>> ICH_mod_out[PIXELS_PER_GROUP];// index - to ICH module
	sc_out<int> fullness_out[NUM_COMPONENTS];//fullness information - to RC
	sc_out<bool> DEMUX_ready_out;//ready signal - to DEMUX

	//local variable
		//state
	enum State
	{
		IDLE,
		INIT,
		RUN,
		TRANSMIT //make buf file
	};
	State state = IDLE;
	bool calculate_done;
	bool transmit_done;
		//fifo
	typedef struct fifo_s
	{
		char name;
		unsigned char *data;
		int size;
		int fullness;
		int read_ptr;
		int write_ptr;
		int max_fullness;
		int bits_added;
	} fifo_t;
	fifo_t dec_balance_fifo[UNITS_PER_GROUP];
		//output
	int residue[PIXELS_PER_GROUP][NUM_COMPONENTS];
	int ich[PIXELS_PER_GROUP];
		//internal calculate
	int previous_num_bits[PIXELS_PER_GROUP];
	int force_mpp = 0;
	int max_bits_per_group = ((3 * BIT_DEPTH + 15) / 16);
	int qp_;
	int qlevel[PIXELS_PER_GROUP], qlevel_old[PIXELS_PER_GROUP];
	bool prev_ich_selected, ich_selected;
	int adj_predicted_size[UNITS_PER_GROUP];
	int required_size[UNITS_PER_GROUP];
	int predicted_size[UNITS_PER_GROUP];
	int tmp_residue[PIXELS_PER_GROUP];
	int rc_size_unit[UNITS_PER_GROUP];

	//functions
		//state
	void StateMachine();
	void Idle();
	void Init();
	void Run();
	void Transmit();
		//internal calculate
	int MapQpToQlevel(int qp, int cpnt);
	int GetQpAdjPredSize(int cpnt);
	int FindResidualSize(int eq);
	int PredictSize(int *req_size);
		//fifo
	void FifoInit(fifo_t *fifo, char name, int size);
	void FifoFree(fifo_t *fifo);
	int FifoGetBits(fifo_t *fifo, int n, int sign_extend);
	void FifoPutBits(fifo_t *fifo, unsigned int d, int nbits);

	SC_CTOR(VLD)
	{
		SC_METHOD(StateMachine);
			dont_initialize();
			sensitive << clk.pos();
			sensitive_neg << reset;

	}

	//for clock counting
	int cl = 0;
};