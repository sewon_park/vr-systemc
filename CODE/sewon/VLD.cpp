#include "VLD.h"

void VLD::StateMachine()
{
	//LOG(cl++ << "\t@VLD ");
	if (!reset)
	{
		//LOG("reset signal - next: INIT" << endl);
		state = INIT;
	}
	else
	{
		switch (state)
		{
		case IDLE:
			DEMUX_ready_out.write(false);
			//LOG(": IDLE");
			if (enable.read() == true)// endable signal
			{
				//LOG(" - next: RUN");
				state = RUN;
			}
			else
			{
				Idle();
			}
			break;
		case INIT:
			//LOG(": INIT - next: IDLE");
			Init();
			state = IDLE;
			break;
		case RUN:
			//LOG(": RUN");
			if (calculate_done == true)
			{
				//LOG(" - next: TRANSMIT");
				state = TRANSMIT;
				calculate_done = false;
				for (int i = 0; i < NUM_COMPONENTS; i++)
				{
					P_mod_out[i].write(tmp_residue[i]);
				}
				mode_select_out.write(ich_selected);
				//for (int j = 0; j < NUM_COMPONENTS; j++)
				//{
				//	for (int i = 0; i < NUM_COMPONENTS; i++)
				//	{
				//		LOG(residue[j][i] <<" ");
				//	}
				//	//LOG(endl);
				//}
			}
			else
			{
				Run();
			}
			break;
		case TRANSMIT:
			//LOG(": TRANSMIT");
			if (transmit_done == true)
			{
				if (ich_done_in.read() == true)
				{
					//LOG(" - next: IDLE");
					state = IDLE;
					transmit_done = false;
					DEMUX_ready_out.write(true);
				}
			}
			else
			{
				Transmit();
			}
			break;
		default:
			break;
		}
		//LOG(endl);
	}
}

void VLD::Idle()
{
	if (state == IDLE)
	{
		for (int i = 0; i < UNITS_PER_GROUP; i++)
		{
			if (unit_in[i].read() == "1")
			{
				FifoPutBits(&(dec_balance_fifo[i]), 1, 1);
				//LOG("fifo[" << i << "]= " << unit_in[i].read() << " [" << dec_balance_fifo[i].fullness << "]\t");
			}
			else if (unit_in[i].read() == "0")
			{
				FifoPutBits(&(dec_balance_fifo[i]), 0, 1);
				//LOG("fifo[" << i << "]= " << unit_in[i].read() << " [" << dec_balance_fifo[i].fullness << "]\t");
			}
			else
			{
				//LOG("fifo[" << i << "]= X" << " [" << dec_balance_fifo[i].fullness << "]\t");
			}
			fullness_out[i].write(dec_balance_fifo[i].fullness);
		}
		//LOG(endl);
	}
}

void VLD::Init()
{
	if (state == INIT)
	{
		calculate_done = false;
		transmit_done = false;


		for (int i = 0; i < UNITS_PER_GROUP; i++)
		{
			for (int j = 0; j < SAMPLES_PER_UNIT; j++)
			{
				residue[i][j] = 0;
			}
			FifoInit(&(dec_balance_fifo[i]), 'd', (int)(((MUX_WORD_SIZE + MAX_SE_SIZE - 1)*MAX_SE_SIZE) + 7) / 8);
			qlevel[i] = 0;
			qlevel_old[i] = 0;
			previous_num_bits[i] = 0;
			required_size[i] = 0;
			predicted_size[i] = 0;
			adj_predicted_size[i] = 0;
			ich[i] = 0;
			tmp_residue[i] = 0;
		}

		prev_ich_selected = false;
		ich_selected = false;
		force_mpp = 0;
		//LOG("init done" << endl);
	}
}

void VLD::Run()
{
	if (state == RUN)
	{
		qp_ = qp_in.read().to_int();

		for (int i = 0; i < NUM_COMPONENTS; i++)
		{
			//LOG("component: "<<cl << endl);
			qlevel[i] = MapQpToQlevel(qp_, i);
			//LOG("qlevel: "<<qlevel[i] << endl);
			adj_predicted_size[i] = GetQpAdjPredSize(i);
			//LOG("predicted size: "<<adj_predicted_size[i] << endl);

			if (i == 0)
			{
				prev_ich_selected = ich_selected;
				ich_selected = 0;
			}

			//todo: flatness control

			if (ich_selected)
				break;							// Don't read other 2 components if we're in ICH mode

			int max_prefix = BIT_DEPTH_F(i) - qlevel[i] + (i == 0) - adj_predicted_size[i];
			int prefix_value = 0;
			while ((prefix_value < max_prefix) && !(FifoGetBits(&(dec_balance_fifo[i]),1,0)))
				prefix_value++;
			int size;

			if ((i == 0) && prev_ich_selected)
				size = adj_predicted_size[i] + prefix_value - 1;
			else
				size = adj_predicted_size[i] + prefix_value;

			int use_ich;
			if (prev_ich_selected)
				use_ich = (prefix_value == 0);
			else
				use_ich = (size >= (BIT_DEPTH + 1 - qlevel[0]));

			if ((i == 0) && use_ich)  // ICH hit
			{
				ich_selected = 1;

				for (int j = 0; j<PIXELS_PER_GROUP; ++j)
				{
					ICH_mod_out[j].write(FifoGetBits(&dec_balance_fifo[j], ICH_BITS, 0));
				}
				mode_select_out.write(ich_selected);
				
				rc_size_unit[0] = ICH_BITS + 1;
				rc_size_unit[1] = rc_size_unit[2] = ICH_BITS;

				break;
			}

			int max_size = 0;
			for (int j = 0; j<SAMPLES_PER_UNIT; j++)
			{
				residue[j][i] = FifoGetBits(&dec_balance_fifo[i], size, 0);
				required_size[j] = FindResidualSize(residue[j][i]);
				max_size = MAX(required_size[j], max_size);
				switch (i)
				{
				case 0:
					tmp_residue[j] = (residue[j][i] << ((BIT_DEPTH + 1) * 2));
					break;
				case 1:
					tmp_residue[j] += (residue[j][i] << (BIT_DEPTH + 1));
					break;
				case 2:
					tmp_residue[j] += (residue[j][i]);
					break;
				default:
					break;
				}
			}
		/*	if (midpoint_selected)
			{
				max_size = size;
				for (int j = 0; j<SAMPLES_PER_UNIT; ++j)
					required_size[j] = size;
			}
*/

			//prev qlevel save
			qlevel_old[i] = qlevel[i];
			rc_size_unit[i] = max_size*SAMPLES_PER_UNIT + 1;
			predicted_size[i] = PredictSize(required_size);;

		}
		calculate_done = true;
	}
}

void VLD::Transmit()
{
	for (int i = 0; i < PIXELS_PER_GROUP; i++ )
	{
		P_mod_out[i].write(tmp_residue[i]);
	}
	transmit_done = true;
}


int VLD::MapQpToQlevel(int qp, int cpnt)
{
	int qlevel_luma_8bpc[] = { 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 7 };
	int qlevel_chroma_8bpc[] = { 0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 8, 8 };
	if (cpnt == 0)
		return qlevel_luma_8bpc[qp];
	else
	{
		return qlevel_chroma_8bpc[qp];

	}
}

int VLD::GetQpAdjPredSize(int cpnt)
{
	int pred_size, max_size;

	// *MODEL NOTE* MN_DSU_SIZE_PREDICTION
	pred_size = predicted_size[cpnt];

	pred_size += qlevel_old[cpnt] - qlevel[cpnt];
	max_size = BIT_DEPTH_F(cpnt) - qlevel[cpnt];
	pred_size = CLAMP(pred_size, 0, max_size - 1);

	return (pred_size);
}

int VLD::FindResidualSize(int eq)
{
	int size_e;

	// Find the size in bits of e
	if (eq == 0) size_e = 0;
	else if (eq >= -1 && eq <= 0) size_e = 1;
	else if (eq >= -2 && eq <= 1) size_e = 2;
	else if (eq >= -4 && eq <= 3) size_e = 3;
	else if (eq >= -8 && eq <= 7) size_e = 4;
	else if (eq >= -16 && eq <= 15) size_e = 5;
	else if (eq >= -32 && eq <= 31) size_e = 6;
	else if (eq >= -64 && eq <= 63) size_e = 7;
	else if (eq >= -128 && eq <= 127) size_e = 8;
	else if (eq >= -256 && eq <= 255) size_e = 9;
	else if (eq >= -512 && eq <= 511) size_e = 10;
	else if (eq >= -1024 && eq <= 1023) size_e = 11;
	else if (eq >= -2048 && eq <= 2047) size_e = 12;
	else if (eq >= -4096 && eq <= 4095) size_e = 13;
	else size_e = 14;

	return size_e;
}

int VLD::PredictSize(int *req_size)
{
	int pred_size;

	pred_size = 2;  // +0.5 for rounding
	pred_size += req_size[0] + req_size[1];
	pred_size += 2 * req_size[2];
	pred_size >>= 2;

	return (pred_size);
}

/*! \param fifo		Pointer to FIFO data structure
\param n		Number of bits to retrieve
\param sign_extend Flag indicating to extend sign bit for return value
\return			Value from FIFO */
int VLD::FifoGetBits(fifo_t *fifo, int n, int sign_extend)
{
	unsigned int d = 0;
	int i, pad = 0;
	unsigned char b;
	int sign = 0;

	if (fifo->fullness < n)
	{
		LOG(fifo->name << " FIFO underflow!\n");
		exit(1);
	}
	if (fifo->fullness < n)
	{
		pad = n - fifo->fullness;
		n = fifo->fullness;
	}

	for (i = 0; i<n; ++i)
	{
		b = (fifo->data[fifo->read_ptr / 8] >> (7 - (fifo->read_ptr % 8))) & 1;
		if (i == 0) sign = b;
		d = (d << 1) + b;
		fifo->fullness--;
		fifo->read_ptr++;
		if (fifo->read_ptr >= fifo->size)
			fifo->read_ptr = 0;
	}

	if (sign_extend && sign)
	{
		int mask;
		mask = (1 << n) - 1;
		d |= (~mask);
	}
	return (d << pad);
}

/*! \param fifo		Pointer to FIFO data structure
\param d		Value to add to FIFO
\param n		Number of bits to add to FIFO */
void VLD::FifoPutBits(fifo_t *fifo, unsigned int d, int nbits)
{
	int i;
	unsigned char b;

	if (fifo->fullness + nbits > fifo->size)
	{
		LOG(fifo->name << " FIFO overflow!\n");
		exit(1);
	}

	fifo->bits_added += nbits;
	fifo->fullness += nbits;
	for (i = 0; i<nbits; ++i)
	{
		b = (d >> (nbits - i - 1)) & 1;
		if (!b)
			fifo->data[fifo->write_ptr / 8] &= ~(1 << (7 - (fifo->write_ptr % 8)));
		else
			fifo->data[fifo->write_ptr / 8] |= (1 << (7 - (fifo->write_ptr % 8)));
		fifo->write_ptr++;
		if (fifo->write_ptr >= fifo->size)
			fifo->write_ptr = 0;
	}
	if (fifo->fullness > fifo->max_fullness)
		fifo->max_fullness = fifo->fullness;
}

/*! \param fifo		 Pointer to FIFO data structure
\param size		 Specifies FIFO size in bytes */
void VLD::FifoInit(fifo_t *fifo, char name, int size)
{
	fifo->name = name;
	fifo->data = (unsigned char *)malloc(sizeof(unsigned char) * size);
	fifo->size = size * 8;
	fifo->fullness = 0;
	fifo->read_ptr = fifo->write_ptr = 0;
	fifo->max_fullness = 0;
	fifo->bits_added = 0;
}

/*! \param fifo		Pointer to FIFO data structure */
void VLD::FifoFree(fifo_t *fifo)
{
	free(fifo->data);
}