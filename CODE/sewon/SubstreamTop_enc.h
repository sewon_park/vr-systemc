#pragma once
#include "VLC.h"
#include "MUX.h"
#include "systemc.h"

SC_MODULE(SUBSTREAM_TOP_enc)
{
	//in global
	sc_in_clk clk;			//clock
	sc_in<bool> reset;		//reset signal



	VLC *vlc;
	//in (maybe from ich)
	sc_in<bool> enable_vlc;			//enable signal - from (maybe ich)
	//in P_MODE
	sc_in<sc_uint<26>> residual[3];	//residue - from Predictor module
	sc_in<int> p_err;				//p-err value - from Predictor module
	//in ICH
	sc_in<sc_uint<5>> ICH_index[3];	//index - from ICH module
	sc_in<bool> ich_select_bit;		// 0:dont use ICH , 1:use ICH - from ICH module
	sc_in<int> ich_err;				//ich_err value - from ICH module
	//in RC							
	sc_in<sc_uint<4>> qp;			// quantization parameter - from Rate Controller
	//out ICH
	sc_out<bool> ich_en;			// ICH update signal - to ICH module
	sc_out<bool> mode_sel;			// 1:P-mode, 0:ICH_mode - to ICH module

	MUX *mux;
	sc_in<int> group_counter;		//group count from out side
	sc_in<bool> muxing_mode;		// 1 for use muxing
	//out RC
	sc_out<int> Y_fifio_size;		//fifo_size - to Rate Controller
	sc_out<int> Co_fifo_size;		//fifo_size - to Rate Controller
	sc_out<int> Cg_fifo_size;		//fifo_size - to Rate Controller
	//output no meaning
	sc_out<unsigned char*> bitstream; //output; use FILE for test


	//signal MUX - VLC
	sc_signal<bool> VLC_ready;

	//signal VLC - MUX
	sc_signal<bool> enable_mux;
	sc_signal<sc_lv<1>> Y_out;
	sc_signal<sc_lv<1>> Co_out;
	sc_signal<sc_lv<1>> Cg_out;

	SC_CTOR(SUBSTREAM_TOP_enc)
	{
		vlc = new VLC("VLC");

		vlc->clk(clk);
		vlc->reset(reset);
		vlc->enable_in(enable_vlc);
		vlc->VLC_ready_in(VLC_ready);
		vlc->P_mod_in[0](residual[0]);
		vlc->P_mod_in[1](residual[1]);
		vlc->P_mod_in[2](residual[2]);
		vlc->p_err_in(p_err);
		vlc->ICH_mod_in[0](ICH_index[0]);
		vlc->ICH_mod_in[1](ICH_index[1]);
		vlc->ICH_mod_in[2](ICH_index[2]);
		vlc->ich_use_in(ich_select_bit);
		vlc->ich_err_in(ich_err);
		vlc->qp_in(qp);
		vlc->unit_out[0](Y_out);
		vlc->unit_out[1](Co_out);
		vlc->unit_out[2](Cg_out);
		vlc->ich_enable_out(ich_en);
		vlc->mode_select_out(mode_sel);
		vlc->MUX_enable_out(enable_mux);


		mux = new MUX("MUX");
		mux->clk(clk);
		mux->reset(reset);
		mux->enable(enable_mux);
		mux->muxing_mode(muxing_mode);
		mux->group_count(group_counter);
		mux->fifo_in[0](Y_out);
		mux->fifo_in[1](Co_out);
		mux->fifo_in[2](Cg_out);
		mux->fifo_size_out[0](Y_fifio_size);
		mux->fifo_size_out[1](Co_fifo_size);
		mux->fifo_size_out[2](Cg_fifo_size);
		mux->bitstream(bitstream);
		mux->VLC_ready_out(VLC_ready);
	}
};