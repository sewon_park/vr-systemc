#include "DEMUX.h"

void DEMUX::StateMachine()
{
	//LOG(cl++ << "\t@DMUX ");
	if (!reset)
	{
		//LOG("reset signal - next: INIT" << endl);
		state = INIT;
	}
	else
	{
		switch (state)
		{
		case IDLE:
			//LOG(": IDLE");
			if (enable_in.read() == true)// endable signal
			{
				//LOG(" - next: RUN");
				state = RUN;
			}
			break;
		case INIT:
			//LOG(": INIT - next: IDLE");
			Initialize();
			state = IDLE;
			break;
		case RUN:
			//LOG(": RUN");
			if (calculate_done == true)
			{
				//LOG(" - next: TRANSMIT");
				state = TRANSMIT;
				calculate_done = false;
			}
			else
			{
				Run();
			}
			break;
		case TRANSMIT:
			//LOG(": TRANSMIT");
			if (transmit_done == true)
			{
				//LOG(" - next: WAIT");
				state = WAIT;
				transmit_done = false;
				VLD_enable_out.write(true);
			}
			break;
		case WAIT:
			VLD_enable_out.write(false);
			if (ready_in.read() == true)
			{
				state = IDLE;
			}
			break;
		default:
			break;
		}
		//LOG(endl);
	}
}

void DEMUX::Initialize()
{
	if (state == INIT)
	{
		bool transmit_done;
		//bitstream
		*buf_in = NULL;
		mux_bit_counter = 0;
		transmit_iterator = 0;
		calculate_done = false;
		for (int i = 0; i < NUM_COMPONENTS; i++)
		{
			out_amount[i] = -1;
			for (int j = 0; j < 48; j++)
				out[i][j] = 0;

		}
	}
}

void DEMUX::Run()
{
	if (state == RUN)
	{
		if (enable_in.read() == true)
		{
			//buf_in = buf_input;
			if (muxing_mode.read() == true)
			{
				ProcessGroupDec(buf_input);
				calculate_done = true;

			}
			else
			{
				WriteEntryFromBitstream(buf_input);
				calculate_done = true;
			}
		}
	}
}


void DEMUX::Transmit()
{
	if (state == TRANSMIT)
	{
		//LOG("interation: " << transmit_iterator << endl);
		for (int i = 0; i < UNITS_PER_GROUP; i++)
		{
			if (transmit_iterator <= (out_amount[i] + 1))
			{
				if (out[i][transmit_iterator] == 1)
					unit_out[i] = "1";
				else if (out[i][transmit_iterator] == 0)
					unit_out[i] = "0";
				else
					unit_out[i] = "x";
			}
			else
				unit_out[i] = "x";
		}

		//LOG("INTER VALUE:\tY: " << out[0][transmit_iterator] << "\tCo: " << out[1][transmit_iterator] << "\tCg: " << out[2][transmit_iterator] << endl);
		for (int i = 0; i < UNITS_PER_GROUP; i++)
			out[i][transmit_iterator] = -10;
		transmit_iterator++;
		//LOG("OUTPUT:\t\tY: " << unit_out[0].read() << "\tCo: " << unit_out[1].read() << "\tCg: " << unit_out[2].read() << endl);

		if ((transmit_iterator >(out_amount[0] + 1)) && (transmit_iterator > (out_amount[1] + 1)) && (transmit_iterator > (out_amount[2] + 1)))
		{
			transmit_done = true;
			transmit_iterator = 0;
			out_amount[0] = -1;
			out_amount[1] = -1;
			out_amount[2] = -1;
		}
	}
}

//! Process a single group through the FIFO's, and insert 0 - 3 mux words in shifters
/*! \param dsc_cfg   DSC configuration structure
\param dsc_state Current DSC state
\param buf       Pointer to input buffer */
void DEMUX::ProcessGroupDec(unsigned char *buf)
{
	int i, j;
	unsigned char d;
	int max_se_size[NUM_COMPONENTS];

	max_se_size[0] = BIT_DEPTH_F(0) * 4 + 4;
	max_se_size[1] = BIT_DEPTH_F(1) * 4;  // prefix omitted
	max_se_size[2] = BIT_DEPTH_F(2) * 4;

	// Check to see if mux words are needed and feed the FIFO's
	for (i = 0; i<NUM_COMPONENTS; ++i)
	{
		if (fullness_in[i].read() < max_se_size[i])
		{
			for (j = 0; j<(MUX_WORD_SIZE / 8); ++j)
			{
				d = getbits(8, buf, &mux_bit_counter, 0);
				AddBits(i, d, 8);
			}
		}
	}
}

/*! \param dsc_cfg   DSC configuration structure
\param dsc_state Current DSC state
\param buf       Pointer to output buffer */
void DEMUX::WriteEntryFromBitstream(unsigned char *buf)
{
	/*
	int i;
	int sz;
	unsigned int d;

	for (i = 0; i<UNITS_PER_GROUP; ++i)
	{
		sz = FifoGetBits(&(se_size_fifo[cl]), 6, 0);
		if (sz>32)
		{
			d = FifoGetBits(&(enc_balance_fifo[i]), 32, 0);
			PutBits(d, 32, buf, &bit_counter);
			sz -= 32;
		}
		d = FifoGetBits(&(enc_balance_fifo[ci), sz, 0);
		if (enc_balance_fifo[i].fullness != 0)
			LOG(enc_balance_fifo[i].name << " Error: shifter underflow\n");
		PutBits(d, sz, buf, &bit_counter);
	}
	*/
}

void DEMUX::AddBits(int cpnt, int value, int bit_num)
{
	//LOG("COMPONET: " << cpnt << '\t' << "BITS: [" << bit_num << "]\t");
	int value_ = value;
	for (int i = 0; i < bit_num; i++)
	{
		++out_amount[cpnt];
		out[cpnt][out_amount[cpnt]] = ((value_ >> (bit_num - (i + 1))) && 1);
		value_ -= (out[cpnt][out_amount[cpnt]] << (bit_num - (i + 1)));
		//LOG(out[cpnt][out_amount[cpnt]]);
	}
	//LOG(endl);
	//add to output array
}
/*
int DEMUX::GetBits(int unit, int size, int sign_extend, unsigned char *buf)
{
	mux_bit_counter += size;

	if (muxing_mode.read() == false)
		return (getbits(size, buf, &mux_bit_counter, sign_extend));

	return (FifoGetBits(&(shifter[unit]), size, sign_extend));
}
*/
int DEMUX::getbits(int size, unsigned char *buf, int *bit_count, int sign_extend)
{
	int i;
	int outval = 0;
	int bufidx;
	int bit;
	int sign = 0;
	//LOG("GET BITS:");
	if (size == 0)
		return(0);
	for (i = 0; i<size; ++i)
	{
		bufidx = (*bit_count)++;
		bit = buf[bufidx];
		bit &= 1;
		//LOG(bit);
		if (i == 0)
			sign = bit;
		outval = (outval << 1) | bit;
	}
	if (sign_extend && sign)
	{
		int mask;
		mask = (1 << size) - 1;
		outval |= (~mask);
	}
	//LOG(endl);
	return(outval);
}