#pragma once
#include "PrefixParameter.h"
#include "systemc.h"
#include <stdio.h>

SC_MODULE(MUX)
{
	//input
	sc_in_clk clk;//clock
	sc_in<bool> reset;//reset signal
	sc_in<bool> enable;//enable MUX - form VLC
	sc_in<bool> muxing_mode;// 1 for use muxing
	sc_in<int> group_count;//group count from out side
	sc_in<sc_lv<1>> fifo_in[UNITS_PER_GROUP];//data - from VLC

	//output
	sc_out<int> fifo_size_out[UNITS_PER_GROUP];//fifo_size - to Rate Controller
	sc_out<bool> VLC_ready_out;//ready VLC - to VLC
	sc_out<unsigned char *> bitstream;//output; use FILE for test

	//local variables
		//state
	enum State
	{
		IDLE,//get input;
		INIT, 
		RUN,
		TRANSMIT
	};
	State state;
	int group_count_;//temp value
	bool calculate_done;
	bool transmit_done;
		//fifo
	typedef struct fifo_s
	{
		char name;
		unsigned char *data;
		int size;
		int fullness;
		int read_ptr;
		int write_ptr;
		int max_fullness;
		int bits_added;
	} fifo_t;
	fifo_t se_size_fifo[UNITS_PER_GROUP];
	fifo_t enc_balance_fifo[UNITS_PER_GROUP];
	fifo_t dec_model_fifo[UNITS_PER_GROUP];
	int backup_fullness[UNITS_PER_GROUP];
		//bitstream
//	unsigned char buf_out[CHUCK_SIZE*SLICE_HEIGHT];
	unsigned char buf_out[10000];//for test
	int bit_counter;
	int bit_counter_;

	//functions
	void StateMachine();
	void Idle();
	void Run();
	void Initialize();
	void Transmit();
		//fifo
	void FifoInit(fifo_t *fifo, char name, int size);
	void FifoFree(fifo_t *fifo);
	int FifoGetBits(fifo_t *fifo, int n, int sign_extend);
	void FifoPutBits(fifo_t *fifo, unsigned int d, int nbits);
		//bitstream
	void PutBits(int val, int size, unsigned char *buf);
	void WriteEntryToBitstream(unsigned char *buf);
	void ProcessGroupEnc(unsigned char *buf);

	SC_CTOR(MUX)
	{
		SC_METHOD(StateMachine);
			dont_initialize();
			sensitive << clk.pos();

	}


	//for clock counting
	int cl = 0;

	FILE * rate_buffer = NULL;
};