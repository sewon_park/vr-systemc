//function define
#define CLAMP(X, MIN, MAX) ((X)>(MAX) ? (MAX) : ((X)<(MIN) ? (MIN) : (X)))
#define MAX(X, Y) ((X)>(Y) ? (X) : (Y))
#define MIN(X, Y) ((X)<(Y) ? (X) : (Y))
#define ABS(X) ((X)<0 ? (-1*(X)) : (X))
//parameter define
#define NUM_COMPONENTS 3
#define PADDING_LEFT 5
#define PADDING_RIGHT 5
#define PIXELS_PER_GROUP 3
#define SAMPLES_PER_UNIT 3
#define PRED_BLK_SIZE 3
//changable parameter
#define SLICE_WIDTH 9
#define SLICE_HEIGHT 9
#define BP_RANGE 10
#define BP_SIZE 3
#define BP_EDGE_STRENGTH 32