#include "ICH_decode.h"
#include "PrefixParameter.h"
#include "systemc.h"


SC_MODULE(ICH_decoder_top)
{

    sc_in<bool>	clock;			//for sync
	sc_in<bool> reset;			//
	sc_in<bool> ICH_enable;		//from VLC entropy decoder //1 ICH use, 0 no ICH   		cant find
	sc_in<bool> mode;			//from VLC entropy decoder //1 P-mode, ICH-mode    		connect to mode_select_out in VLD.h
	sc_in<bool> newgroup;		//from controller //1 new group 0 no			
	sc_in<int> group;			//from controller //number of group
	sc_in<int> linenumber;		//from controller //line number
	
	sc_in<sc_int<26>> recon[PIXELS_PER_GROUP];		//from predictor 			        connect to recon_x_out in predict_decode.h

	sc_in<sc_uint<5>>index[PIXELS_PER_GROUP];		//from VLC entropy decoder module   connect to ICH_mod_out in VLD.h
	sc_in<sc_int<26>>preline[SLICE_WIDTH];			//from predictor					connect to prevline_out in predict_decode.h
	sc_out<sc_int<26>>ycocg_o[PIXELS_PER_GROUP];	//to buffer
	
	//sc_out<int>ichmode_end;			//to predictor
	//sc_out<int>pmode_end;			//to predictor
	//add port
	sc_out<int>update_end;			//to predictor 
	int i;        	
	ICH_decode *decoder;
   

	SC_CTOR(ICH_decoder_top) 
    {	
		decoder = new ICH_decode("DECODER");

		decoder->clock(clock);
		decoder->reset(reset);
		decoder->ICH_enable(ICH_enable);

		decoder->update_end(update_end);
		decoder->mode(mode);
		decoder->newgroup(newgroup);
		decoder->group(group);
		decoder->linenumber(linenumber);
		for (i = 0; i < PIXELS_PER_GROUP; i++)
		{
			decoder->recon[i](recon[i]);
			decoder->index[i](index[i]);
		}
		for (i = 0; i < SLICE_WIDTH; i++)
			decoder->preline[i](preline[i]);

		for (i = 0; i < PIXELS_PER_GROUP; i++)
			decoder->ycocg_o[i](ycocg_o[i]);

		//decoder->ichmode_end(ichmode_end);
		//decoder->pmode_end(pmode_end);
     	
		
        //Declare DECODER IOport
    }
};