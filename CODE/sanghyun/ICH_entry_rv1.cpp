
#include "systemc.h"
//#include "parameter.h"
#include "ICH_entry.h"
//#include "fstream"
//#include "iostream"
#include "math.h"
#include "PrefixParameter.h"
#define Log2(x) log(x)/log(2)

// use err[0],[1],[2] => cal icherrlog2ceil


void ICH_entry:: idle()
{

	int i;
	ICH_enable_r = ICH_enable.read();
	mode_r = mode.read();
	newgroup_r = newgroup.read();
	//pmode_end.write(0);
	//ichmode_end.write(0);
	update_end.write(0);
	errflag.write(0);
	
	 for(i = 0; i<ICH_SIZE; i++)
	 	mask[i] = 0;
}
void ICH_entry:: statemachine()
{	
	int i;
	sc_int<26> ini;
	ini = 0;

	if(!reset.read())
	{		
		for(i = 0;i < ICH_SIZE; i++)
		{
			valid[i] = 0;
			table[i] = ini ; 	//initial setting
		}
		state = IDLE;

	}
	else
	{
		state = behavstate();		
	}
}

int ICH_entry::behavstate()
{
	int next_state;
	
	if(!reset.read())
	{
		next_state = IDLE;
		return next_state;
	}
	else
	{
		if (state == IDLE)
		{
			idle();
			if (!ICH_enable_r)	//ICH_enable = 0, newgroup = 1
			{
				if (newgroup_r)
				{
					next_state = NEWGROUP;
					return next_state;
				}
				else
				{
					next_state = state;
					return next_state;
				}
			}
			else 				//ICH_enable = 1
			{
				if (mode_r)		//Pmode
				{
					next_state = PMODE;
					return next_state;
				}
				else 			//ICH_mode
				{						
					next_state = ICHMODE;
					return next_state;
				}
			}
			return next_state = state;
		} 
		else if (state == PMODE)
		{
			if (pmode())
			{
				next_state = IDLE;
				return next_state;
			}
			else
			{
				next_state = state;
				return next_state;
			}
		}
		else if (state == ICHMODE)
		{	
			
			if (ichmode() == 1)
			{					
				next_state = IDLE;
				return next_state;
			}
			else
			{
				next_state = state;
				return next_state;
			}
		}	
		else	//NEWGROUP
		{
			group_r = group.read();
			
			
			if (linenumber.read() == 0) // first line
			{	
				if (newgroup_errsearch())
				{
					next_state = IDLE;
					return next_state;
				}
				else
				{
					next_state = state;
					return next_state;
				}
			}
			else			   //no first line
			{
				if (newgroup_update())
				{
					if (newgroup_errsearch())
					{
						next_state = IDLE;
						return next_state;
					}
					else
					{
						next_state = state;
						return next_state;
					}
				}
				else
				{
					next_state = state;
					return next_state;
				}
			}
		}
	}

}

int ICH_entry:: pmode()
{	
	int line = 0;
	int i;

	line = linenumber.read();
		//recon val to buff
		for(i = 0; i < PIXELS_PER_GROUP;i++)
		recon_buff[i] = recon[i].read();		
		//shift		
		if(linenumber.read()%LINEPIX == 0)	//first line 	index 0~31
		{
			for(i = ICH_SIZE - 1 - PIXELS_PER_GROUP; i >=0;i--)
			{	
				valid[i+PIXELS_PER_GROUP] = valid[i];
				table[i+PIXELS_PER_GROUP] = table[i];
			}
		}
		else			//no first line index 0~24
		{
			for(i = ICH_SIZE - 1 - 7 - PIXELS_PER_GROUP; i >= 0;i--)
			{
				table[i+PIXELS_PER_GROUP] = table[i];
				valid[i+PIXELS_PER_GROUP] = valid[i];
			}

		}
		//update
		for(i = 0;i < PIXELS_PER_GROUP;i++)
		{
			table[i] = recon_buff[i];
			valid[i] = 1;
		}
			/*
			//log
			cout << " Encoder Pmode table shift result" << endl;
			for (i = 0; i < ENTRY; i++)
				cout << table[i]<<",";
			cout << endl;
			for (i = 0; i < ENTRY; i++)
				cout << valid[i] << ",";
			cout << endl;
			//
			*/
		update_end.write(1);
		// pmode_end.write(1);
		//ichmode_end.write(0);
	return 1;
}

int ICH_entry:: ichmode()
{	
		int i,j;
		int count = 0;
		int same = 0;		
		int SIZE;

		for( i = 0; i < PIXELS_PER_GROUP; i++)
			index_buff[i] = index[i].read().to_int();
			/*
			index_buff[0] = index[0].read().to_int();
			index_buff[1] = index[1].read().to_int();
			index_buff[2] = index[2].read().to_int();
			*/
		for(i = 0; i<PIXELS_PER_GROUP; i++)
			mask[index_buff[i]] = 1;
		//check same
		for(i = 0; i< PIXELS_PER_GROUP; i++)
		{
			for(j = i+1;j< PIXELS_PER_GROUP;j++)
			{
				if(index_buff[i] == index_buff[j])
					same = same +1;
			}
		}
		if(same == 3)
			same = 2;		
		//table => buff
		for(i = 0; i< PIXELS_PER_GROUP; i++)
		{	
			if(valid[index_buff[i]] == 1)
				recon_buff[i] = table[index_buff[i]];
			//else	
		}
		//shift		
		//1st line : no 1st line
		SIZE = (linenumber.read()%SLICE_WIDTH == 0) ?  ICH_SIZE : ICH_SIZE-7;	//1st line : no 1st line		
		if (index_buff[1] == index_buff[0])
		{	
			index_buff[1] = index_buff[2];
			recon_buff[1] = recon_buff[2];
		}	
		for(i = ICH_SIZE -1; i >= 0; i--)
		{		
			if(!mask[i])//if (is valid)      valid[index] = 1  : not excute
			{	
				for(j = 0;j < PIXELS_PER_GROUP- same; j++)
				{
				if(i < index_buff[j])				
					count = count +1;										
				}	
			}			
			//shift
			if(i+count < ICH_SIZE)
			{
			table[i+count] = table[i];
			valid[i+count ] = valid[i];
			}			
			count = 0;
		}
		for (i = 0; i <  PIXELS_PER_GROUP - same; i++)
		{
			table[i] = recon_buff[i];
			valid[i] = 1;			
		}	
		same = 0;	
		/*
		//log
			cout << " Encoder ICHmode table shift result" << endl;
			for (i = 0; i < ENTRY; i++)
				cout << table[i]<<",";
			cout << endl;
			for (i = 0; i < ENTRY; i++)
				cout << valid[i] << ",";
			cout << endl;
		//
		*/
		update_end.write(1);
		//pmode_end.write(0);
		//ichmode_end.write(1);
		
	return 1;
}
/////////////////////
//Need optimization//
/////////////////////
int ICH_entry:: newgroup_errsearch()
{			
	//new optimize code
	sc_int<8>y[PIXELS_PER_GROUP];
	sc_int<9>co[PIXELS_PER_GROUP],cg[PIXELS_PER_GROUP];
	//diff[# of pixel in group][entry][# of component]
	int i,j,k;
	int modified_qp;
	int qp_read;
	int hit = 1;
	int lowest[PIXELS_PER_GROUP] = {99999,99999,99999};   //store lowest weight sum
	int best[PIXELS_PER_GROUP] = {99,99,99};              //store index
	int weightsad[PIXELS_PER_GROUP] = {0,0,0};            //store weight sum
	int diff[PIXELS_PER_GROUP][ICH_SIZE][3];
	int max[3] = {0,0,0};                           //store max err value
	int maxerr[PIXELS_PER_GROUP];         //store maxQperr
	//calcuate qp
	qp_read = qp.read();
	modified_qp = MIN(15,qp_read+2);
	maxerr[0] = QuantDivisor[qlevel_luma_8bpc[modified_qp]]/2;
	maxerr[1] = maxerr[2] = QuantDivisor[qlevel_chroma_8bpc[modified_qp]]/2;
	//input new group value
		for(i = 0; i< PIXELS_PER_GROUP; i++)
		{
			y[i] = ycocg_in[i].read().range(25,18);
			co[i] = ycocg_in[i].read().range(17,9);
			cg[i] = ycocg_in[i].read().range(8,0);
		}
	//cal err and store in array
		for(j = 0; j < PIXELS_PER_GROUP; j++)     //parallel?
		{
			for(i = 0; i< ICH_SIZE; i++)
			{
				if(valid[i] == 1)
				{
					diff[j][i][0] = (int)table[i].range(25,18) - (int)y[j];		//pixel j y val
					diff[j][i][1] = (int)table[i].range(17,9) - (int)co[j];		//pixel j co val
					diff[j][i][2] = (int)table[i].range(8,0) - (int)cg[j];		//pixel j cg val
				}
			}
		}
	//decision and index_o cal    
		for(j = 0; j < PIXELS_PER_GROUP; j++)     //parallel?
			{
				for(i = 0; i< ICH_SIZE; i++)
				{
					if(valid[i] == 1)
					{   
						//decise use or not
						if((diff[j][i][0] < maxerr[0]) && (diff[j][i][1] < maxerr[1]) && (diff[j][i][2] < maxerr[2]))
							hit =1;
						else
							hit = 0;
						//cal weight 
						weightsad[j] = 2*abs(diff[j][i][0]) + abs(diff[j][i][1]) + abs(diff[j][i][2]);
						if(lowest[j] > weightsad[j])
						{
							lowest[j] = weightsad[j];
							best[j] = i;
						}

					}
				}
			}
		ICH_decision.write(hit);
	//find max y co cg err 
		for(j = 0; j < 3; j++)              //for each component
		{
			for(i = 0;i < ICH_SIZE; i++)       // for all entry
			{
				for( k = 0; k < PIXELS_PER_GROUP; k++)
				{
					//max[# of component]
					if( max[j] < diff[k][i][j])
						max[j] = diff[k][i][j];
				}
			}
		}
	//
		//logerrichmode.write(2 * ceil(Log2(max[0])) + ceil(Log2(max[1])) + ceil(Log2(max[2])));
		logerrichmode.write(2 * ceil_log2(max[0]) + ceil_log2(max[1]) + ceil_log2(max[2]));
		errflag.write(1);
	//	

	//write value
		for(i = 0; i < PIXELS_PER_GROUP; i++)
		{
			index_o[i].write(best[i]);
			//err[i].write(max[i]);
		}				
	return 1;
}

int ICH_entry:: newgroup_update()
{		
	sc_int<26>prebuff[SLICE_WIDTH];

	int groupnum = 0;
	int i;
		groupnum = group.read();
	for(i = 0; i< SLICE_WIDTH; i++)
		prebuff[i] = preline[i].read();
	
	for(i = 0 + (groupnum%GROUPINLINE); i< 7+(groupnum%GROUPINLINE);i++)
	{
		table[25+i- (groupnum % GROUPINLINE)] = prebuff[i];	
		valid[25+i-groupnum%GROUPINLINE] = 1;		
	}
	return 1;
}
int ceil_log2(int val)
{
	int ret = 0, x;
	x = val;
	while (x) { ret++; x >>= 1; }
	return(ret);
}
/*
int sc_main(int argc, char* argv[])
{	
		///IO define
		//sc_signal<bool> clock;
		sc_int<26>ini;
		ini = 0;
		sc_clock clock("clock",1);
		sc_signal<bool> reset;
		sc_signal<bool> mode;
		sc_signal<bool> ICH_enable;
		sc_signal<bool> newgroup;

		sc_signal<int> group;
		sc_signal<int> linenumber;
		
		sc_signal<sc_int<26>>ycocg_in[3];
		sc_signal<sc_int<26>>recon[3];
		sc_signal<sc_uint<5>>index[3];
		sc_signal<sc_int<26>>preline[9];
		//sc_signal<int>state
		sc_signal<int>err[3];
		sc_signal<int>index_o[3];
		//sc_signal<int>state_internal;

		sc_signal<int> qp;
		sc_signal<int> ICH_decision;

		sc_signal<int>ichmode_end;
		sc_signal<int>pmode_end;


		
		////module instiate
		int i;
		ICH_entry ICH("ICH");
		ICH.clock(clock);
		ICH.reset(reset);
		ICH.ICH_enable(ICH_enable);
		ICH.mode(mode);
		ICH.newgroup(newgroup);
		ICH.group(group);
		ICH.linenumber(linenumber);
		ICH.qp(qp);
		ICH.ICH_decision(ICH_decision);

		ICH.ichmode_end(pmode_end);
		ICH.pmode_end(pmode_end);
		//ICH.state_o(state_o);

	
		for(i = 0; i< 3; i++)
		{
			ICH.ycocg_in[i](ycocg_in[i]);
			ICH.recon[i](recon[i]);
			
			ICH.index_o[i](index_o[i]);
			ICH.err[i](err[i]);
		}
		for(i = 0; i<9;i++)
			ICH.preline[i](preline[i]);

		ICH.index[0](index[0]);
		ICH.index[1](index[1]);
		ICH.index[2](index[2]);
		////
		//sc_start();
		cout<<"Encoder-process"<<endl;

		sc_trace_file *wf = sc_create_vcd_trace_file("ICH_wave");
		sc_trace(wf,clock,"CLK");
		sc_trace(wf,reset,"reset");
		sc_trace(wf,mode,"mode");
		sc_trace(wf,ICH_enable,"ICH_enable"); 
		sc_trace(wf,newgroup,"NEW");
		//sc_trace(wf,state_o,"STATE");
		sc_trace(wf,linenumber,"LINE");
		sc_trace(wf,index[0],"INDEX0");
		sc_trace(wf, index[1], "INDEX1");
		sc_trace(wf, index[2], "INDEX2");
		sc_trace(wf,ICH.state,"state");
		
		sc_trace(wf,ycocg_in[0],"ycocg_in0");
		sc_trace(wf,ycocg_in[1],"ycocg_in1");
		sc_trace(wf,ycocg_in[2],"ycocg_in2");

		sc_trace(wf,index_o[0],"index_o0");
		sc_trace(wf,index_o[1],"index_o1");
		sc_trace(wf,index_o[2],"index_o2");

		sc_trace(wf,err[0],"err0");
		sc_trace(wf,err[1],"err1");
		sc_trace(wf,err[2],"err2");

		cout<<"Debug"<<endl;
		reset = 0;
		mode = 0;
		ICH_enable = 0;
		linenumber = 1;
		newgroup = 0;
		for(i = 0; i< 9;i++)
			preline[i] = ini +i;
		cout<<"initial setting"<<endl;
		sc_start(6,SC_NS);
		reset = 1;
		mode = 0;
		ICH_enable = 1;
		//newgroup = 1;
		linenumber = 0;
		//group = 1;
		index[0] = 1;
		index[1] = 2;
		index[2] = 4;
		ycocg_in[0] = 11;
		ycocg_in[1] = 1100;
		ycocg_in[2] = 31;
		sc_start(2,SC_NS);
		index[0] = 1;
		index[1] = 7;
		index[2] = 4;
		sc_start(2,SC_NS);
		
		//art(2,SC_NS);
		//ICHMODE test
		
		reset = 1;
		mode = 0;
		newgroup = 1;

		ICH_enable = 1;
		index[0] = 1;
		index[1] = 1;
		index[2] = 1;
		sc_start(2,SC_NS);
		index[0] = 1;
		index[1] = 2;
		index[2] = 3;
		sc_start(2,SC_NS);
		index[0] = 28;
		index[1] = 29;
		index[2] = 30;
		
		//pmode test
		
		reset = 1;
		linenumber = 0;
		ICH_enable = 1;
		mode = 1;
		recon[0] = 11111;
		recon[1] = 1111;
		recon[2] = 111;
		sc_start(2,SC_NS);
		linenumber = 1;
		sc_start(2,SC_NS);
		


		


		//cout<<"test1"<<endl;


		sc_stop();



		return 0;
}*/