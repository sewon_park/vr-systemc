#include "ICH_entry.h"
#include "systemc.h"
#include "Prefixparameter.h"
SC_MODULE(ICH_encoder_top)
{
    //IO port
	int i;
    //ENCODER
	//erase err, ichmode end, pmode end

    sc_in<bool>	clock;			//for sync
	sc_in<bool> reset;			//
	sc_in<bool> ICH_enable;		//from decision for group module //1 ICH use, 0 no ICH,  			   connect to ich_enable_out in VLC.h
	sc_in<bool> mode;			//from decision for group module //1 P-mode, 0 ICH-mode, 			   connect to mode_select_out in VLC.h
	sc_in<bool> newgroup;		//from controller //1 new group 0 no
	sc_in<int> group;			//from controller //number of group vpos
	sc_in<int> linenumber;		//from controller //line number     hpos
	sc_in<sc_int<26>> ycocg_in[PIXELS_PER_GROUP];	//from buffer
	sc_in<sc_int<26>> recon[PIXELS_PER_GROUP];		//from predictor //from decision for group module, cant find
	sc_in<sc_uint<5>> index[PIXELS_PER_GROUP];		//from decision for group module // 			   can't find
	sc_in<sc_int<26>>preline[SLICE_WIDTH];			//from predictor, 								   connect to prevLine_out in predict_encoder.h
	sc_in<int> qp;					//from rate controller
	//
	//sc_out<int> err[3];				//to decision for group module, max over pixels in group,for abs form 0 = y, 1 = co, 2 = cg
	sc_out<int> index_o[PIXELS_PER_GROUP];			//to decision for group module,  				   connect to ICH_mod_in in VLC.h
	sc_out<int> ICH_decision;		//to decision for group module, 					 			   connect to ich_use_in in VLC.h

	//sc_out<int>ichmode_end;		//to predictor
	//sc_out<int>pmode_end;			//to predictor
	sc_out<int>update_end;			//tp predictor, 												   connect to end_update in predict_encoder.h
	sc_out<int>logerrichmode;		//to decision for group module,									   connect to ich_err_in in VLC.h
	sc_out<int>errflag;				//to decision for group module, 								   can't find	
	ICH_entry *encoder;	
	

    SC_CTOR(ICH_encoder_top)
    {
        //Declare ENCODER IOport
		encoder = new ICH_entry("ENCODER");
     	encoder->clock(clock);
		encoder->reset(reset);
		encoder->ICH_enable(ICH_enable);
		encoder->mode(mode);
		encoder->newgroup(newgroup);
		encoder->group(group);
		encoder->linenumber(linenumber);
		encoder->logerrichmode(logerrichmode);
		encoder->update_end(update_end);
		encoder->qp(qp);
		encoder->errflag(errflag);
		encoder->ICH_decision(ICH_decision);

		for(i = 0;i < PIXELS_PER_GROUP; i++)
		{
			encoder->recon[i](recon[i]);
			encoder->index[i](index[i]);
			//encoder->err[i](err[i]);
			encoder->index_o[i](index_o[i]);
			encoder->ycocg_in[i](ycocg_in[i]);
		}
		for(i = 0;i < SLICE_WIDTH;i++)		
		encoder->preline[i](preline[i]);


		//encoder->ichmode_end(ichmode_end);
		//encoder->pmode_end(pmode_end);
        
    }



};