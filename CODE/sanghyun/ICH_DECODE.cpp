#include "systemc.h"
#include "ICH_decode.h"
#include "parameter.h"
#include "string.h"
#include "PrefixParameter.h"
//ichmode pmode end siganl merge to updateen signal
void ICH_decode:: idle()
{
	int i;
	reset_r = reset.read();
	ICH_enable_r = ICH_enable.read();
	mode_r = mode.read();
	newgroup_r = newgroup.read();
	 for(i = 0; i<ICH_SIZE; i++)
	 	mask[i] = 0;
	
}
void ICH_decode:: statemachine()
{
	if(reset.read()==0)
	{
		//cout<<"test"<<endl;
		state = IDLE;
	}
	else
	{
		state = next_state_d;		
	}
}

void ICH_decode::behavstate()
{
	if (!reset.read())
	{
		next_state_d = IDLE;
	}
	else
	{

		if (state == IDLE)
		{
                idle();
				
				pmode_start.write(0);			
				ichmode_start.write(0);
				newupdate_start.write(0);	
                if (!ICH_enable.read())	//ICH_enable = 0, newgroup = 1
                {
                    if (newgroup.read())
                    {
                        next_state_d = NEWGROUP;
						 pmode_start.write(0);
						 ichmode_start.write(0);
						 newupdate_start.write(1);                        
                    }
                    else
                    {
                        next_state_d = state;
                        
                    }
                }
                else 				//ICH_enable = 1
                { 
                    if (mode.read())		//Pmode
                    {
                        next_state_d = PMODE;
						 pmode_start.write(1);
						 ichmode_start.write(0);
						 newupdate_start.write(0);                        
                    }
                    else 			//ICH_mode
                    {                        
                        next_state_d = ICHMODE;						
						 pmode_start.write(0);
						 ichmode_start.write(1);
						 newupdate_start.write(0);                        
                    }
                }                
		}
		else if (state == PMODE)
		{						
			if (pmode_table_shift_done.read() == 1 )
			{
				next_state_d = IDLE;
				 pmode_start.write(0);				
			}
			else
			{
				next_state_d = state;				
			}
		}
		else if (state == ICHMODE)
		{						
			if ((ichmode_mask_reconbuff_update.read() == 1)&&(ichmode_index_line_read_done.read() == 1)&&(shift_update_ichmode.read() == 1))
			{													
				next_state_d = IDLE;
				ichmode_start.write(0);								
			}
			else
			{
				next_state_d = state;				
			}
		}

		else if(state == NEWGROUP)	//NEWGROUP
		{			
				if (update_table_new.read() == 1)
				{
						next_state_d = IDLE;
						 newupdate_start.write(0);						 		
				}
				else
				{		
					next_state_d = state;		
				}			
		}
	}
}

void ICH_decode:: line_recon_read_pmode()
{   
	int i;
	if(!reset.read())
	{
		pmode_line_recon_read_done.write(0);
	}
	else
	{
		if(pmode_start.read()==1)
		{			
			line = linenumber.read();
			for(i = 0; i< PIXELS_PER_GROUP; i++)		
				recon_buff[i] = recon[i].read();

			pmode_line_recon_read_done.write(1);
		
		}
		else
		{
			pmode_line_recon_read_done.write(0);
		}
	}
}

void ICH_decode:: index_line_read()
{	
	int i;
	if(!reset.read())
	{
		ichmode_index_line_read_done.write(0);
	}
	else
	{
		if(ichmode_start.read()==1)
		{		
			for(i = 0; i<PIXELS_PER_GROUP; i++)
			index_buff[i] = index[i].read().to_int();
		
			line = linenumber.read();
			ichmode_index_line_read_done.write(1);		
		}
		else
		{
			for(i = 0; i<PIXELS_PER_GROUP; i++)
			index_buff[i] = 0;
			ichmode_index_line_read_done.write(0);
		}
	}
}

void ICH_decode:: mask_reconbuff_update()
{
	int i;
     if(!reset.read())
	 {
		 ichmode_mask_reconbuff_update.write(0);
	 }
	 else
	 {
		if ((ichmode_start.read()==1)&&(ichmode_index_line_read_done.read() == 1))
		{
			for(i = 0; i< PIXELS_PER_GROUP; i++)
			mask[index_buff[i]] = 1;
			for(i = 0;i <PIXELS_PER_GROUP; i++)
			recon_buff[i] = table[index_buff[i]];

			ichmode_mask_reconbuff_update.write(1);
		}
		else
		{	
			for(i = 0; i< ICH_SIZE; i++)
			mask[i] = 0;
			ichmode_mask_reconbuff_update.write(0);
		}
	 }
}

void ICH_decode:: group_line_read()
{
    int i;
	if(!reset.read())
	{
		group_line_read_done.write(0);
	}
	else
	{
		if(newupdate_start.read() == 1)
		{
			groupnum = group.read();
			line = linenumber.read();

			for(i = 0; i< SLICE_WIDTH; i++)
				prebuff[i] = preline[i].read();    //prebuff must be global variable

			group_line_read_done.write(1);			
		}
		else
		{
			group_line_read_done.write(0);
		}

	}
}

void ICH_decode::shift_table()
{
	int i;
	int j;
	sc_int<26>ini;
	ini = 0;
	int count = 0;
	int same = 0;
	
	if (reset.read() == 0)
	{	
		pmode_table_shift_done.write(0);
		shift_update_ichmode.write(0);
		update_table_new.write(0);
		//pmode_end.write(0);
		//ichmode_end.write(0);
		update_end.write(0);
		for (i = 0; i < ICH_SIZE; i++)
		{
			table[i] = 0; 	//initial
			valid[i] = 0;
		}		
	}
	else
	{
		if (ICH_enable.read() == 0)   //update
		{	
			if((group_line_read_done.read() == 1) && (newupdate_start.read()== 1))
			{	
				if(linenumber.read() != 0)
				{						
					for (i = 0 + (group.read() % GROUPINLINE); i < 7 + (group.read() % GROUPINLINE); i++)
					{
						table[25 + i - group.read() % GROUPINLINE] = prebuff[i];
						valid[25 + i - group.read() % GROUPINLINE] = 1;
					}
					update_table_new.write(1);
					pmode_table_shift_done.write(0);
					shift_update_ichmode.write(0);
				}
				else
				{
					update_table_new.write(1);
					pmode_table_shift_done.write(0);
					shift_update_ichmode.write(0);
				}
				/*
				//log//
				cout << "Newgroup table shift result" << endl;
				for (i = 0; i < ENTRY; i++)
					cout << table[i] << ",";
				cout << endl;
				for (i = 0; i < ENTRY; i++)
					cout << valid[i] << ",";
				cout << endl;
				//
				*/
			}
			else
			{
				update_table_new.write(0);
				pmode_table_shift_done.write(0);
				shift_update_ichmode.write(0);
			//	pmode_end.write(0);
			//	ichmode_end.write(0);
				update_end.write(0);
			}
		}
		else			//ich enable
		{
			if ((pmode_line_recon_read_done.read() == 1)&&(pmode_start.read() == 1))	// pmode
			{					
				if (linenumber.read() % SLICE_WIDTH == 0)
				{
					for (i = ICH_SIZE - 1 - PIXELS_PER_GROUP; i >= 0; i--)		//shift
					{
						table[i + PIXELS_PER_GROUP] = table[i];
						valid[i + PIXELS_PER_GROUP] = valid[i];
					}
					
					for(i = 0; i < PIXELS_PER_GROUP; i++)
					{
						table[i] = recon_buff[i];
						valid[i] = 1;
					}
					/*
					table[0] = recon_buff[0];
					table[1] = recon_buff[1];
					table[2] = recon_buff[2];
					valid[0] = 1;
					valid[1] = 1;
					valid[2] = 1;	
					*/				
				}
				else
				{
					for (i = ICH_SIZE - 1 - PIXELS_PER_GROUP- 7; i >= 0; i--)	//shift
					{
						table[i + PIXELS_PER_GROUP] = table[i];
						valid[i + PIXELS_PER_GROUP] = valid[i];
					}
					for(i = 0; i < PIXELS_PER_GROUP; i++)
					{
						table[i] = recon_buff[i];
						valid[i] = 1;
					}
					/*
					table[0] = recon_buff[0];
					table[1] = recon_buff[1];
					table[2] = recon_buff[2];
					valid[0] = 1;
					valid[1] = 1;
					valid[2] = 1;	
					*/
				}
				/*	
				//log//
				cout << "Pmode table shift result" << endl;
				for (i = 0; i < ENTRY; i++)
					cout << table[i] << ",";
				cout << endl;
				for (i = 0; i < ENTRY; i++)
					cout << valid[i] << ",";
				cout << endl;
				//
				*/
				pmode_table_shift_done.write(1);
				shift_update_ichmode.write(0);
				update_table_new.write(0);

				//pmode_end.write(1);
				//ichmode_end.write(0);
				update_end.write(1);


			}
			//ichmode
			else if(((ichmode_start.read()==1)&&(ichmode_index_line_read_done.read() == 1))&&(ichmode_mask_reconbuff_update.read() == 1))
			{	
				for(i = 0; i < PIXELS_PER_GROUP; i++)
					ycocg_o[i].write(recon_buff[i]);
				/*
				ycocg_o[0].write(recon_buff[0]);
				ycocg_o[1].write(recon_buff[1]);
				ycocg_o[2].write(recon_buff[2]);
				cout <<index_buff[0]<<index_buff[1]<<index_buff[2];
				*/
				for(i = 0;i<PIXELS_PER_GROUP;i++)
				{
					for(j = i+1; j<PIXELS_PER_GROUP; j++)
					{
						if(index_buff[i] == index_buff[j])
						same = same +1 ;
					}
				}
					if(same == 3)
					same = 2;

				if (linenumber.read() % 9 == 0)                 // for first line
				{	
					if (index_buff[1] == index_buff[0])
					{
						index_buff[1] = index_buff[2];
						recon_buff[1] = recon_buff[2];
					}	
					for (i = ICH_SIZE - 1; i >= 0; i--)
					{
						if (!mask[i])
						{
							for(j = 0; j < PIXELS_PER_GROUP-same; j++)								
							{
								if(i < index_buff[j])
								count = count + 1;
							}														
						}
						if (i + count < ICH_SIZE)
						{
							table[i + count] = table[i];
							valid[i + count] = valid[i];
						}

						count = 0;
					}

					for (i = 0; i < PIXELS_PER_GROUP - same; i++)
						{
							table[i] = recon_buff[i];
							valid[i] = 1;
						}
					
					for (i = 0; i< ICH_SIZE; i++)
						mask[i] = 0;
					same = 0;	
			      //ich mode is finish
				}
				else                            // not a first line
				{
					if (index_buff[1] == index_buff[0])
					{
						index_buff[1] = index_buff[2];
						recon_buff[1] = recon_buff[2];
					}
					for (i = ICH_SIZE-1-7; i >= 0; i--)
					{
						if (!mask[i])
						{
							for(j = 0; j< PIXELS_PER_GROUP-same; j++)
							{
								if (i < index_buff[j])
									count = count + 1;
							}
						}
						if (i + count < ICH_SIZE -7)
						{
							table[i + count] = table[i];
							valid[i + count] = valid[i];
						}

						count = 0;
					}
					for (i = 0; i< ICH_SIZE; i++)
						mask[i] = 0;

					if (index_buff[1] == index_buff[0])
					recon_buff[1] = recon_buff[2];
					for (i = 0; i < PIXELS_PER_GROUP - same; i++)
					{
						table[i] = recon_buff[i];
					}
			      //ich mode is finish
					same = 0;
				}
				/*
				//log
				cout << "ICHmode table shift result" << endl;
				for (i = 0; i < ENTRY; i++)
					cout << table[i]<<",";
				cout << endl;
				for (i = 0; i < ENTRY; i++)
					cout << valid[i] << ",";
				cout << endl;
				//
				*/
				shift_update_ichmode.write(1);
				pmode_table_shift_done.write(0);
				update_table_new.write(0);
				//pmode_end.write(0);
				//ichmode_end.write(1);
				update_end.write(1);

			}
			else
			{
				pmode_table_shift_done.write(0);
				shift_update_ichmode.write(0);
				update_table_new.write(0);
				//pmode_end.write(0);
				//ichmode_end.write(0);
				update_end.write(0);
			}
		}
	}


}





/*
int sc_main(int argc, char* argv[])
{
		sc_int<26> ini;
		ini = 0;
		///IO define
		//sc_signal<bool> clock;
        sc_clock clock("clock",1);
		//sc_clock clk("clock", 1, SC_NS, 1);
		sc_signal<bool> reset;
		sc_signal<bool> mode;
		sc_signal<bool> ICH_enable;
		sc_signal<bool> newgroup;
        //sc_signal<sc_int<26>>recon;
		sc_signal<int> group;
		sc_signal<int> linenumber;
		sc_signal<sc_int<26>>recon[3];
		//sc_signal<sc_int<26>>recon;
		//sc_signal<sc_int<26>>ycocg_in;
		sc_signal<sc_int<26>>ycocg_o[3];
		sc_signal<int>index[3];
		sc_signal<sc_int<26>>preline[9];
		//sc_signal<sc_int<2>>state_o;

		//sc_signal<int>err[3];
		//sc_signal<int>index_o[3];
		////module instiate
		int i;
		ICH_decode ICH("ICH");
        
		ICH.clock(clock);
		ICH.reset(reset);
		ICH.ICH_enable(ICH_enable);
		ICH.mode(mode);
		ICH.newgroup(newgroup);
		ICH.group(group);
		ICH.linenumber(linenumber);
		//ICH.state_o(state_o);
		//ICH.recon(recon);

			//ICH.ycocg_in[i](ycocg_in[i]);
			
			//ICH.index_o[i](index_o[i]);
			//ICH.err[i](err[i]);sc_clock CLOCK("clock", 20);.
		for(i = 0; i< 3; i++)
		{
			ICH.ycocg_o[i](ycocg_o[i]);
			ICH.index[i](index[i]);
			ICH.recon[i](recon[i]);
		}

		for(i = 0; i<9;i++)
			ICH.preline[i](preline[i]);
		////
		//sc_start();
		sc_trace_file *wf = sc_create_vcd_trace_file("ICH_renew");
		sc_trace(wf,clock,"CLK");
		sc_trace(wf,reset,"reset");
		sc_trace(wf,mode,"mode");
		sc_trace(wf,ICH_enable,"ICH_enable");
		sc_trace(wf,newgroup,"NEW");
		//sc_trace(wf,state_o,"STATE");
		sc_trace(wf,linenumber,"LINE");

		//sc_trace(wf,index,"INDEX");
		sc_trace(wf,ycocg_o[0],"YCOCG_O0");
		sc_trace(wf,ycocg_o[1],"YCOCG_O1");
		sc_trace(wf,ycocg_o[2],"YCOCG_O2");

		sc_trace(wf,recon[0],"RECON0");
		sc_trace(wf,recon[1],"RECON1");
		sc_trace(wf,recon[2],"RECON2");
		
		sc_trace(wf,ICH.ichmode_start,"ichstart");

		sc_trace(wf,ICH.pmode_start,"pmodestart");
		sc_trace(wf, ICH.pmode_line_recon_read_done, "pmodelinerecondone");
		//sc_trace(wf, ICH.pmode_table_shift_done, "pmode table shift");
		sc_trace(wf, ICH.pmode_table_shift_done, "pmodeend");

		sc_trace(wf,ICH.state,"state");
		sc_trace(wf,ICH.shift_update_ichmode,"ichend");
		sc_trace(wf,ICH.ichmode_index_line_read_done,"ichindexread");
		sc_trace(wf,ICH.ichmode_mask_reconbuff_update,"ichmask_reconbuff");

		sc_trace(wf, ICH.newupdate_start, "groupstart");
		//sc_trace(wf,ICH.group_line_read_done, "group line read done");
		sc_trace(wf, ICH.update_table_new, "update");
		sc_trace(wf,index[0],"INDEX0");
		sc_trace(wf,index[1],"INDEX1");
		sc_trace(wf,index[2],"INDEX2");
        //cf array sc_signal
        //int z;


      //cout<<"@"<<sc_time_stamp()<<endl;
		sc_trace(wf,ICH.table,"table");
        cout<<"OO"<<endl;
		reset = 0;
		mode = 0;
		newgroup = 0;
		ICH_enable = 0;
		linenumber = 0;
		for(i = 0; i < 9; i++)
		preline[i] = ini+i;


		cout<<"Decode-Prosess"<<endl;
		cout<<"ICH-MODE"<<endl;
		sc_start(6,SC_NS); 
		
		reset = 1;
		ICH_enable = 0;
		newgroup = 1;
		group = 0;
		linenumber = 1;
		sc_start(8, SC_NS);
		sc_stop();

		
		reset = 1;
		mode = 1;
		ICH_enable = 1;
		linenumber = 1;
		recon = 11;
		sc_start(10,SC_NS);
		
		
		mode = 0;
		reset = 1;
		ICH_enable = 1;			//ich mode need index linenumber
		linenumber = 1;
		index[0] = 26;
		index[1] = 27;
		index[2] = 30;
		//index[1] = 1;
		//index[2] = 5;
		//mode = 0;
		sc_start(5,SC_NS);
		
		mode = 1;
		reset = 1;
		ICH_enable = 1;
		linenumber = 1;
		recon[0] = 111;
		recon[1] = 112;
		recon[2] = 113;
		sc_start(3,SC_NS);
		mode = 0;
		index[0] = 0;
		index[1] = 1;
		index[2] = 2;
		sc_start(5,SC_NS);
		ICH_enable = 0;
		newgroup = 1;
		linenumber = 1;
		group = 0;
		sc_start(5,SC_NS);
		mode = 0;
		ICH_enable = 1;
		index[0] = 25;
		index[1] = 26;
		index[2] = 27;
		sc_start(5,SC_NS);
		


		
		sc_start(5,SC_NS);
		index = 0;
		sc_start(5,SC_NS);
		recon = 1111;
		mode = 1;
		sc_start(6,SC_NS);
		mode = 0;
		index = 0;
		sc_start(6,SC_NS);
		//index = 1;
		//sc_start(6, SC_NS);
		cout<<"ICH-mode-end"<<endl;
		
		
		cout<<"#1"<<endl;
		sc_start(6,SC_NS);
		cout<<"#2"<<endl;
		ICH_enable = 1;			//ich mode need index linenumber
		linenumber = 1;
		//index[0] = 1;
		//index[1] = 2;
		//index[2] = 5;
		newgroup = 0;
		//mode = 0;
		cout<<"test"<<endl;

		sc_start(6,SC_NS);
		newgroup = 1;
		ICH_enable = 0;
		sc_start(6,SC_NS);
		
        sc_stop();
		
		newgroup = 0;
		for(i = 0;i<3;i++)
		{
			clock = 1;
			reset = 1;
			sc_start(1,SC_NS);
			clock = 0;
			reset = 1;
			sc_start(1,SC_NS);
		}
		//newgroup = 0;
		ICH_enable = 1;
		mode = 1;
		for(i = 0;i<3;i++)
		{
			clock = 1;
			reset = 1;
			sc_start(1,SC_NS);
			clock = 0;
			reset = 1;
			sc_start(1,SC_NS);
		}
		ICH_enable = 1;
		mode = 0;
		for(i = 0;i<3;i++)
		{
			clock = 1;
			reset = 1;
			sc_start(1,SC_NS);
			clock = 0;
			reset = 1;
			sc_start(1,SC_NS);
		}

		for(i = 0;i<3;i++)
		{
			clock = 1;
			reset = 1;
			sc_start(1,SC_NS);
			clock = 0;
			reset = 1;
			sc_start(1,SC_NS);
		}
		ICH_enable = 1;
		mode = 1;

		for(i = 0;i<3;i++)
		{
			clock = 1;
			reset = 1;
			sc_start(1,SC_NS);
			clock = 0;
			reset = 1;
			sc_start(1,SC_NS);
		}
		ICH_enable = 0;
		newgroup = 1;
		for(i = 0;i<3;i++)
		{
			clock = 1;
			reset = 1;
			sc_start(1,SC_NS);
			clock = 0;
			reset = 1;
			sc_start(1,SC_NS);
		}
		
		cout<<"test-end"<<endl;




		
		return 0;
}*/