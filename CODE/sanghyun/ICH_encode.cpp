#include "systemc.h"
#include "ICH_encoder.h"
#define IDLE 0
#define PMODE 1
#define ICHMODE 2
#define NEWGROUP 3
#define MIN(X,Y) ((X) < (Y) ? (X) : (Y)) 
void ICH_entry:: idle()
{
	int i;
	reset_r = reset.read();
	ICH_enable_r = ICH_enable.read();
	mode_r = mode.read();
	newgroup_r = newgroup.read();
	 for(i = 0; i<32; i++)
	 	mask[i] = 0;
	
}
void ICH_entry:: statemachine()
{

	if(reset.read()==0)
	{
		cout<<"test"<<endl;
		state = IDLE;

	}
	else
	{

		state = next_state_d;
		cout<<"decode state"<<state;
		if(state == IDLE)
			cout<<"IDLE"<<endl;
		else if(state == PMODE)
			cout<<"PMODE"<<endl;
		else if(state == ICHMODE)
			cout<<"ICHMODE"<<endl;
		else
			cout<<"NEWGROUP"<<endl;



	}
}

void ICH_entry::behavstate()
{
	if (!reset.read())
	{
		next_state_d = IDLE;
		cout<<"resert?"<<endl;
				
	}
	else
	{

		if (state == IDLE)
		{
                //idle();
                //int i;
                //ichmode_start.write(1);
				pmode_start.write(0);
				
				ichmode_start.write(0);
				newupdate_start.write(0);
				cout<<"no reset"<<endl;
                if (!ICH_enable.read())	//ICH_enable = 0, newgroup = 1
                {
                    if (newgroup.read())
                    {
                        next_state_d = NEWGROUP;
						 pmode_start.write(0);
						 ichmode_start.write(0);
						 newupdate_start.write(1);
                        //return next_state;
                    }
                    else
                    {
                        next_state_d = state;
                        //return next_state;
                    }
                }
                else 				//ICH_enable = 1
                { 
                    if (mode.read())		//Pmode
                    {
                        next_state_d = PMODE;
						 pmode_start.write(1);
						 ichmode_start.write(0);
						 newupdate_start.write(0);
                        //return next_state;
                    }
                    else 			//ICH_mode
                    {
                        cout<<"EXE1"<<endl;
                        next_state_d = ICHMODE;
						cout<<"next"<<next_state_d<<endl;
						 pmode_start.write(0);
						 ichmode_start.write(1);
						 newupdate_start.write(0);
                        //eturn next_state;
                    }
                }
                //return next_state = state;
		}
		else if (state == PMODE)
		{
			cout<<"PMM"<<endl;
			if (pmode_table_shift_done.read() == 1 )
			{
				next_state_d = IDLE;
				 pmode_start.write(0);

				//return next_state;
			}
			else
			{
				next_state_d = state;
				//return next_state;
			}
		}

		else if (state == ICHMODE)
		{

			if ((ichmode_mask_reconbuff_update.read() == 1)&&(ichmode_index_line_read_done.read() == 1)&&(shift_update_ichmode.read() == 1))
			{
				cout<<"EXE?"<<endl;
				
				cout<<"&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&"<<endl;
				
				next_state_d = IDLE;
				ichmode_start.write(0);
				cout<<"next"<<next_state_d;
				//return next_state;
			}
			else
			{
				next_state_d = state;
				//return next_state;
			}
		}

		else if(state == NEWGROUP)	//NEWGROUP
		{


			
				if ((minfindst4_done.read() == 1)&&(maxfindst4_done.read() == 1))                   //max fine end && min find end
				{                                       
						next_state_d = IDLE;
						 newupdate_start.write(0);
                }
				else
				{
					next_state_d = state;
				}
			

		}
		

	}
}

void ICH_entry:: line_recon_read_pmode()
{   
	int i;
	if(!reset.read())
	{
		pmode_line_recon_read_done.write(0);
	}
	else
	{
    if(pmode_start.read()==1)
    {
        line = linenumber.read();
		for(i = 0; i< 3; i++)		
			recon_buff[i] = recon[i].read();

        pmode_line_recon_read_done.write(1);
		
    }
    else
    {
        pmode_line_recon_read_done.write(0);
    }
	}
}
void ICH_entry:: table_shift_pmode()
{
    //int i;
	//int i;
	if(!reset.read())
	{
		  //pmode_table_shift_done.write(0);
	}
    if((pmode_line_recon_read_done.read() == 1)&&(pmode_start.read() == 1))
    {	
		
    }
    else
    {
        //pmode_table_shift_done.write(0);
    }
}
void ICH_entry:: index_line_read()
{	
	int i;
	if(!reset.read())
	{
		ichmode_index_line_read_done.write(0);
	}
	else
	{
		if(ichmode_start.read()==1)
		{
		
			for(i = 0; i<3; i++)
			index_buff[i] = index[i].read().to_int();
			line = linenumber.read();

			ichmode_index_line_read_done.write(1);
			
		}
		else
		{
			for(i = 0; i<3; i++)
			index_buff[i] = 0;
		ichmode_index_line_read_done.write(0);
		}
	}
}

void ICH_entry:: mask_reconbuff_update()
{
	int i;
     if(!reset.read())
	 {
		 ichmode_mask_reconbuff_update.write(0);
	 }
	 else
	 {
		if ((ichmode_start.read()==1)&&(ichmode_index_line_read_done.read() == 1))
		{
			cout<<"mask_reconbuff"<<endl;
			for(i = 0; i< 3; i++)
			mask[index_buff[i]] = 1;
			for(i = 0;i <3; i++)
			recon_buff[i] = table[index_buff[i]];

			ichmode_mask_reconbuff_update.write(1);
		}
		else
		{	
			for(i = 0; i< 32; i++)
			mask[i] = 0;
			ichmode_mask_reconbuff_update.write(0);
		}
	 }
}

void ICH_entry:: shift_update_ichmode_f()
{

	if(!reset.read())
	{
		 
		
	}
	else
	{
		if(((ichmode_start.read()==1)&&(ichmode_index_line_read_done.read() == 1))&&(ichmode_mask_reconbuff_update.read() == 1)&&(shift_update_ichmode.read() == 0))
		{
			
		}
		else
		{
		}
	}
}
//////////////////////////////////////////////////////////for encoder////////////////////////////////
void ICH_entry:: group_line_read()
{
    int i;
    int qp_r;
    int groupnum;
    sc_int<8>y[3];
    sc_int<9>co[3];
    sc_int<9>cg[3];
	if(!reset.read())
	{
		group_line_read_done.write(0);
	}
	else
	{
		if(newupdate_start.read() == 1)
		{
			cout<<"group_line_read"<<endl;
			groupnum = group.read();
			line = linenumber.read();

            qp_r = qp.read();
            group_ff.write(groupnum);
            line_ff.write(line);
            qp_ff.write(qp_r);
            for(i =0; i < 3; i++)
            {
                y[i] = ycocg_in[i].read().range(25,18);
                co[i] = ycocg_in[i].read().range(17,9);
                cg[i] = ycocg_in[i].read().range(8,0);
            }
            for(i = 0; i < 3; i++)
            {
                y_ff[i].write(y[i]);
                co_ff[i].write(co[i]);
                cg_ff[i].write(cg[i]);
            }

			cout<<"DECODER LINE"<<endl;
		
			cout << groupnum <<":" <<line << endl;
			//cout << newupdate_start.read() << endl;
			for(i = 0; i< 9; i++)
				prebuff[i] = preline[i].read();    //prebuff must be global variable

			group_line_read_done.write(1);			
		}
		else
		{
			group_line_read_done.write(0);
		}
		cout<<"readdone:"<<group_line_read_done<<endl;
	}
}
void ICH_entry:: update_table_new_f()
{
   // int i;
	if(!reset.read())
	{
		//update_table_new.write(0);
		//update_table_new.write(0);
	}
	else
	{	
		cout << "DONE" << group_line_read_done.read() << endl;
		if((group_line_read_done.read() == 1) && (newupdate_start.read()== 1))
		{
			cout<<"update_table_new_f"<<endl;
			// call function
			cout<<"function call"<<endl;
			//shift_table(reset.read(), linenumber.read(), mode.read(), ICH_enable.read(), newgroup.read());
			//for(i = 0 + (groupnum%3); i < 7 + (groupnum%3); i++)
			//table[25+i-groupnum%3] = prebuff[i];
			//update_table_new.write(1);          //new update is finish
			//for(i = 0; i< 32; i++)
			//	cout<<"table["<<i<<"] : "<< table[i]<<endl;
		}
		else
		{
			//update_table_new.write(0);          //new update is not finish yet
		}
	}
}

void ICH_entry::shift_table()
{
	int i;
	int j;
	sc_int<26>ini;
	ini = 0;
	int count = 0;
	int same = 0;
	//cout<<"reser : "<<reset.read()<<"line : "<<linenumber.read()<<"mode : "<<mode.read()<<"ich-enable : "<<ICH_enable.read()<<"update : "<<newgroup.read()<<endl;
	if (reset.read() == 0)
	{	
		pmode_table_shift_done.write(0);
		shift_update_ichmode.write(0);
		update_table_new.write(0);
		pmode_end.write(0);
		ichmode_end.write(0);
		for (i = 0; i < 32; i++)
			table[i] =  i; 	//initial
		
	}
	else
	{
		if (ICH_enable.read() == 0)   //update
		{	
			if((group_line_read_done.read() == 1) && (newupdate_start.read()== 1))
			{	
				if(line_ff.read() != 0)
				{	
					cout<<"excute"<<endl;
					for (i = 0 + (group.read() % 3); i < 7 + (group.read() % 3); i++)
						table[25 + i - group.read() % 3] = prebuff[i];
					update_table_new.write(1);
					pmode_table_shift_done.write(0);
					shift_update_ichmode.write(0);
				}
				else
				{
					update_table_new.write(1);
					pmode_table_shift_done.write(0);
					shift_update_ichmode.write(0);
				}
			}
			else
			{
				update_table_new.write(0);
				pmode_table_shift_done.write(0);
				shift_update_ichmode.write(0);
				pmode_end.write(0);
				ichmode_end.write(0);
			}
		}
		else			//ich enable
		{
			if ((pmode_line_recon_read_done.read() == 1)&&(pmode_start.read() == 1))	// pmode
			{	
				cout << "work?" << endl;
				if (linenumber.read() % 9 == 0)
				{
					for (i = 31 - 3; i >= 0; i--)
						table[i + 3] = table[i];
					
					table[0] = recon_buff[0];
					table[1] = recon_buff[1];
					table[2] = recon_buff[2];

					//pmode_table_shift_done.write(1);
				}
				else
				{
					cout << "takeit" << endl;
					for (i = 31 - 7 - 3; i >= 0; i--)
						table[i + 3] = table[i];

					table[0] = recon_buff[0];
					table[1] = recon_buff[1];
					table[2] = recon_buff[2];

					//pmode_table_shift_done.write(1);
					cout << "shift done : " << pmode_table_shift_done.read() << endl;
				}
				pmode_table_shift_done.write(1);
				shift_update_ichmode.write(0);
				update_table_new.write(0);

				pmode_end.write(1);
				ichmode_end.write(0);


			}
			//else if(((ichmode_start.read()==1)&&(ichmode_index_line_read_done.read() == 1))&&(ichmode_mask_reconbuff_update.read() == 1)&&(shift_update_ichmode.read() == 0))		//ichmode
			else if(((ichmode_start.read()==1)&&(ichmode_index_line_read_done.read() == 1))&&(ichmode_mask_reconbuff_update.read() == 1))
			{	
				//ycocg_o[0].write(recon_buff[0]);
				//ycocg_o[1].write(recon_buff[1]);
				//ycocg_o[2].write(recon_buff[2]);
				for(i = 0;i<3;i++)
				{
					for(j = i+1; j<3; j++)
					{
						if(index_buff[i] == index_buff[j])
						same = same +1 ;
					}
				}
				
				if (linenumber.read() % 9 == 0)                 // for first line
				{	
					if (index_buff[1] == index_buff[0])
					{
						index_buff[1] = index_buff[2];
						recon_buff[1] = recon_buff[2];
					}	
					for (i = 32 - 1; i >= 0; i--)
					{
						if (!mask[i])
						{
							for(j = 0; j < 3-same; j++)								
							{
								if(i < index_buff[j])
								count = count + 1;
							}														
						}
						if (i + count < 32)
							table[i + count] = table[i];

						count = 0;
					}

					for (i = 0; i < 3 - same; i++)
						{
							table[i] = recon_buff[i];
						}
					
					for (i = 0; i< 32; i++)
						mask[i] = 0;

				
					for (i = 0; i < 3 - same; i++)
					{
						table[i] = recon_buff[i];
					}
					same = 0;	
					//shift_update_ichmode.write(1);      //ich mode is finish
				}
				else                            // not a first line
				{
					if (index_buff[1] == index_buff[0])
					{
						index_buff[1] = index_buff[2];
						recon_buff[1] = recon_buff[2];
					}
					cout << "NOT first" << endl;
					for (i = 32 - 1 - 7; i >= 0; i--)
					{
						if (!mask[i])
						{
							for(j = 0; j< 3-same; j++)
							{
								if (i < index_buff[j])
									count = count + 1;
							}
						}
						if (i + count < 32-7)
							table[i + count] = table[i];

						count = 0;
					}
					for (i = 0; i< 32; i++)
						mask[i] = 0;

					if (index_buff[1] == index_buff[0])
					recon_buff[1] = recon_buff[2];
					for (i = 0; i < 3 - same; i++)
					{
						table[i] = recon_buff[i];
					}
					//shift_update_ichmode.write(1);      //ich mode is finish
					same = 0;
				}
				shift_update_ichmode.write(1);
				pmode_table_shift_done.write(0);
				update_table_new.write(0);
				pmode_end.write(0);
				ichmode_end.write(1);

			}
			else
			{
				pmode_table_shift_done.write(0);
				shift_update_ichmode.write(0);
				update_table_new.write(0);
				pmode_end.write(0);
				ichmode_end.write(0);
			}
		}
	}
}
//shift update done 
void ICH_entry::errcal()                       //combinational
{   
    int i,j,k;
    sc_int<8>y[3];
    sc_int<9>co[3];
    sc_int<9>cg[3];
    if(!reset.read())
    {
        errcal_done.write(0);
    }
    else
    {
        if(update_table_new.read() == 1)
        {
            //errcal

            //read ycocg from ff
            for(i = 0; i < 3; i++)
            {
                y[i] = y_ff[i].read();
                co[i] = co_ff[i].read();
                cg[i] = cg_ff[i].read();
            }
            //diff[# of pix in group][# pix in entry][# of component]
            for(k = 0; k < 3;k++)             //number of pix in group
            {
                for(j = 0;j < 32; j++)      //number of pix in entry
                {
                    diff[k][j][0] = abs((int)y[i] - (int)table[i].range(25,18));
                    diff[k][j][1] = abs((int)co[i] - (int)table[i].range(17,9));
                    diff[k][j][2] = abs((int)cg[i] - (int)table[i].range(8,0));
                }
            }
           errcal_done.write(1);
        }
        else
        {
            errcal_done.write(0);
        }
    }
}
void ICH_entry::maxQperr()                     //combinational
{   
    int qp_int = 0;
	int modified_qp;

    if(!reset.read())
    {
        maxQperr_done.write(0);
    }
    else
    {   
        //calQp
        //maxerr
        if(group_line_read_done.read() == 1)
        {
            qp_int = qp_ff.read();    
            modified_qp = MIN(15, qp_int+2);

            maxerr[0] = QuantDivisor[qlevel_luma_8bpc[modified_qp]]/2;
            maxerr[1] = maxerr[2] = QuantDivisor[qlevel_chroma_8bpc[modified_qp]]/2;

            maxQperr_done.write(1);
        }
        else
        {
            maxQperr_done.write(0);
        }

    }    
}
void ICH_entry::store_err_to_ff()
{	
	int i, j, k;
    if(!reset.read())
    {
        store_err_to_ff_done.write(0);
    }
    else
    {
        if( (maxQperr_done.read() == 1) && (errcal_done.read() == 1) )
        {   
            //update diff & qperr
            for(k = 0; k < 3;k++) 
            {   
                maxerr_ff[k].write(maxerr[k]);

                for(j = 0;j < 32; j++) 
                {
                    for(i = 0; i < 3; i++)
                    {
                        diff_ff[k][j][i].write(diff[k][j][i]);
                    }
                }
            }
            //store_err_to_ff
            store_err_to_ff_done.write(1);
        }
        else
        {
            store_err_to_ff_done.write(0);
        }
    }
}
void ICH_entry::compare()          //determine ICH_use //combinational
{   
    int maxerr_int[3];
    int i,j,k;
    int diff_int[3][32][3];
    if(!reset.read())
    {
        compare_done.write(0);        
    }
    else
    {
        if(store_err_to_ff_done.read() == 1)
        {
            //compare  
            for(k = 0; k < 3;k++) 
            {   
                maxerr_int[k] = maxerr_ff[k].read();

                for(j = 0;j < 32; j++) 
                {
                    for(i = 0; i < 3; i++)
                    {
                        diff_int[k][j][i] = diff_ff[k][j][i].read();
                    }
                }
            }
            //
            for(i = 0; i< 3; i++)
            {
                for(j = 0; j<32; j++)
                {
                    if( (diff_int[k][i][0] < maxerr_int[0]) && (diff_int[k][i][1] < maxerr_int[1]) && (diff_int[k][i][2] < maxerr_int[2]))
                        hit = 1;
                    else
                        hit = 0;
                }
            }

            compare_done.write(1);
        }
        else
        {
            compare_done.write(0);
        }
    }
}
//suquential
void ICH_entry::minfindst0()
{   
    int i,j,k;
    int diff_buff_st0[3][32][3];
    int weight0,weight1;
    weight0 = 0;
    weight1 = 0;
    if(!reset.read())
    {
        minfindst0_done.write(0);
    }
    else
    {
        if(store_err_to_ff_done.read() == 1)
        {   
            for(i = 0; i<3;i++)
            {
                for(j = 0; j< 32; j++)
                {
                    for(k = 0; j< 3; k++)
                    {
                        diff_buff_st0[i][j][k] = diff_ff[i][j][k].read();
                    }
                }
            }

            for(i = 0; i < 3; i++)                  //for 3pixels in group
            {
                for(j = 0; j < 16; j++)
                {   
                    weight0 = 2*diff_buff_st0[i][j][0] + diff_buff_st0[i][j][1] + diff_buff_st0[i][j][2];
                    weight1 = 2*diff_buff_st0[i][31-j][0] + diff_buff_st0[i][31-j][1] + diff_buff_st0[i][31-j][2];
                    if(weight0 <= weight1)
                    {   
                        for(k = 0; k < 3;k++)
                        {
                            diff_buff_st0_min_ff[i][j][k].write(diff_buff_st0[i][j][k]);                            
                        }
                        index_buff_st0_ff[i][j].write(j);

                    }  
                    else
                    {
                        for(k = 0; k < 3;k++)
                        {
                            diff_buff_st0_min_ff[i][j][k].write(diff_buff_st0[i][31-j][k]);                            
                        }
                        index_buff_st0_ff[i][j].write(31-j);
                    }
                }
            }
            //find min 
            minfindst0_done.write(1);
        }
        else
        {   
            minfindst0_done.write(0);
        }
    }
}
void ICH_entry::minfindst1()
{
    int i,j,k;
    int diff_buff_st1[3][16][3];                //16 entry for st1
    int index_buff_st1[3][16];
    int weight0,weight1;
    weight0 = 0;
    weight1 = 0;

    if(!reset.read())
    {
        minfindst1_done.write(0);        
    }
    else
    {
        if(minfindst0_done.read() == 1)
        {
            for(i = 0; i<3;i++)
            {
                for(j = 0; j< 16; j++)
                {   
                    index_buff_st1[i][j] = index_buff_st0_ff[i][j].read();

                    for(k = 0; j< 3; k++)
                    {
                        diff_buff_st1[i][j][k] = diff_buff_st0_min_ff[i][j][k].read();
                    }
                }
            }

            for(i = 0; i < 3; i++)                  //for 3pixels in group
            {
                for(j = 0; j < 8; j++)                //st1 entry 16 half =8
                {   
                    weight0 = 2*diff_buff_st1[i][j][0] + diff_buff_st1[i][j][1] + diff_buff_st1[i][j][2];
                    weight1 = 2*diff_buff_st1[i][15-j][0] + diff_buff_st1[i][15-j][1] + diff_buff_st1[i][15-j][2];
                    if(weight0 <= weight1)
                    {   
                        for(k = 0; k < 3;k++)
                        {
                            diff_buff_st1_min_ff[i][j][k].write(diff_buff_st1[i][j][k]);
                        }
                        index_buff_st1_ff[i][j].write(index_buff_st1[i][j]);
                    }  
                    else
                    {
                        for(k = 0; k < 3;k++)
                        {
                            diff_buff_st1_min_ff[i][j][k].write(diff_buff_st1[i][15-j][k]);
                        }
                        index_buff_st1_ff[i][j].write(index_buff_st1[i][15-j]);
                    }
                }
            }
            //find min
            minfindst1_done.write(1);
        }
        else
        {
            minfindst1_done.write(0);
        }
    }
}
void ICH_entry::minfindst2()
{   
    int i,j,k;
    int diff_buff_st2[3][8][3];                //8 entry for st2
    int index_buff_st2[3][8];
    int weight0,weight1;
    weight0 = 0;
    weight1 = 0;

    if(!reset.read())
    {
        minfindst2_done.write(0);        
    }
    else
    {
        if(minfindst1_done.read() == 1)
        {   
            for(i = 0; i<3;i++)
            {
                for(j = 0; j< 8; j++)
                {   
                    index_buff_st2[i][j] = index_buff_st1_ff[i][j].read();

                    for(k = 0; j< 3; k++)
                    {
                        diff_buff_st2[i][j][k] = diff_buff_st1_min_ff[i][j][k].read();
                    }
                }
            }

            for(i = 0; i < 3; i++)                  //for 3pixels in group
            {
                for(j = 0; j < 4; j++)                //st1 entry 8 half =4
                {   
                    weight0 = 2*diff_buff_st2[i][j][0] + diff_buff_st2[i][j][1] + diff_buff_st2[i][j][2];
                    weight1 = 2*diff_buff_st2[i][7-j][0] + diff_buff_st2[i][7-j][1] + diff_buff_st2[i][7-j][2];
                    if(weight0 <= weight1)
                    {   
                        for(k = 0; k < 3;k++)
                        {
                            diff_buff_st2_min_ff[i][j][k].write(diff_buff_st2[i][j][k]);
                        }
                        index_buff_st2_ff[i][j].write(index_buff_st2[i][j]);
                    }  
                    else
                    {
                        for(k = 0; k < 3;k++)
                        {
                            diff_buff_st2_min_ff[i][j][k].write(diff_buff_st2[i][7-j][k]);
                        }
                        index_buff_st2_ff[i][j].write(index_buff_st2[i][7-j]);
                    }
                }
            }
            //find min
            minfindst2_done.write(1);
        }
        else
        {
            minfindst2_done.write(0);
        }
    }
}
void ICH_entry::minfindst3()
{   
    int i,j,k;
    int diff_buff_st3[3][4][3];                //4 entry for st3
    int index_buff_st3[3][4];
    int weight0,weight1;
    weight0 = 0;
    weight1 = 0;
    if(!reset.read())
    {
        minfindst3_done.write(0);        
    }
    else
    {
        if(minfindst2_done.read() == 1)
        {   
            for(i = 0; i<3;i++)
            {
                for(j = 0; j< 4; j++)
                {   
                    index_buff_st3[i][j] = index_buff_st2_ff[i][j].read();

                    for(k = 0; j< 3; k++)
                    {
                        diff_buff_st3[i][j][k] = diff_buff_st2_min_ff[i][j][k].read();
                    }
                }
            }

            for(i = 0; i < 3; i++)                  //for 3pixels in group
            {
                for(j = 0; j < 2; j++)                //st3 entry 4 half =2
                {   
                    weight0 = 2*diff_buff_st3[i][j][0] + diff_buff_st3[i][j][1] + diff_buff_st3[i][j][2];
                    weight1 = 2*diff_buff_st3[i][3-j][0] + diff_buff_st3[i][3-j][1] + diff_buff_st3[i][3-j][2];
                    if(weight0 <= weight1)
                    {   
                        for(k = 0; k < 3;k++)
                        {
                            diff_buff_st3_min_ff[i][j][k].write(diff_buff_st3[i][j][k]);
                        }
                        index_buff_st3_ff[i][j].write(index_buff_st3[i][j]);
                    }  
                    else
                    {
                        for(k = 0; k < 3;k++)
                        {
                            diff_buff_st3_min_ff[i][j][k].write(diff_buff_st3[i][3-j][k]);
                        }
                        index_buff_st3_ff[i][j].write(index_buff_st3[i][3-j]);
                    }
                }
            }
            //find min
            minfindst3_done.write(1);
        }
        else
        {
            minfindst3_done.write(0);
        }
    }
}
void ICH_entry::minfindst4()
{   
     int i,j,k;
    int diff_buff_st4[3][2][3];                //2 entry for st4
    int index_buff_st4[3][2];
    int weight0,weight1;
    weight0 = 0;
    weight1 = 0;
    if(!reset.read())
    {
        minfindst4_done.write(0);        
    }
    else
    {
        if(minfindst3_done.read() == 1)
        {   
            for(i = 0; i<3;i++)
            {
                for(j = 0; j< 2; j++)
                {   
                    index_buff_st4[i][j] = index_buff_st3_ff[i][j].read();

                    for(k = 0; j< 3; k++)
                    {
                        diff_buff_st4[i][j][k] = diff_buff_st3_min_ff[i][j][k].read();
                    }
                }
            }

            for(i = 0; i < 3; i++)                  //for 3pixels in group
            {
                for(j = 0; j < 1; j++)                //st3 entry 2 half =1
                {   
                    weight0 = 2*diff_buff_st4[i][j][0] + diff_buff_st4[i][j][1] + diff_buff_st4[i][j][2];
                    weight1 = 2*diff_buff_st4[i][1-j][0] + diff_buff_st4[i][1-j][1] + diff_buff_st4[i][1-j][2];
                    if(weight0 <= weight1)
                    {   
                        //for(k = 0; k < 3;k++)
                        //{
                        //    diff_buff_st3_min_ff[i][j][k].write(diff_buff_st3[i][j][k]);
                        //}
                        index_o[i].write(index_buff_st4[i][j]);
                    }  
                    else
                    {
                        //for(k = 0; k < 3;k++)
                        //{
                        //    diff_buff_st3_min_ff[i][j][k].write(diff_buff_st2[i][3-j][k]);
                        //}
                        index_o[i].write(index_buff_st4[i][1-j]);
                    }
                }
            }
            //find min
            minfindst4_done.write(1);
        }
        else
        {
            minfindst4_done.write(0);
        }
    }
}
void ICH_entry::maxfindst0()
{
    int i,j,k;
    int diff_buff_st0[3][32][3];            //for st 0 32 entry
    if(!reset.read())
    {
        maxfindst0_done.write(0);
    }
    else
    {
        if(store_err_to_ff_done.read() == 1)
        {   
            for(i = 0; i<3;i++)
            {
                for(j = 0; j< 32; j++)
                {
                    for(k = 0; j< 3; k++)
                    {
                        diff_buff_st0[i][j][k] = diff_ff[i][j][k].read();
                    }
                }
            }

            for(i = 0; i < 3; i++)
            {
                for(j = 0; j < 16; j++)
                {   
                    for(k = 0; k< 3; k++)
                    {
                    if(diff_buff_st0[i][j][k] > diff_buff_st0[i][31-j][k])
                        diff_buff_st0_ff[i][j][k].write(diff_buff_st0[i][j][k]); 
                    else
                        diff_buff_st0_ff[i][j][k].write(diff_buff_st0[i][31-j][k]);    
                    }
                }
            }
            //find val
            maxfindst0_done.write(1);
        }
        else
        {
            maxfindst0_done.write(0);

        }
    }
}
void ICH_entry::maxfindst1()
{   
    int i,j,k;
    int diff_buff_st1[3][16][3];            //for st 1 16 entry
    if(!reset.read())
    {
        maxfindst1_done.write(0);        
    }
    else
    {
        if(maxfindst0_done.read() == 1)
        {   
            for(i = 0; i<3;i++)
            {
                for(j = 0; j< 16; j++)
                {
                    for(k = 0; j< 3; k++)
                    {
                        diff_buff_st1[i][j][k] = diff_buff_st0_ff[i][j][k].read();
                    }
                }
            }

            for(i = 0; i < 3; i++)
            {
                for(j = 0; j < 8; j++)
                {   
                    for(k = 0; k< 3; k++)
                    {
                    if(diff_buff_st1[i][j][k] > diff_buff_st1[i][15-j][k])
                        diff_buff_st1_ff[i][j][k].write(diff_buff_st1[i][j][k]); 
                    else
                        diff_buff_st1_ff[i][j][k].write(diff_buff_st1[i][15-j][k]);    
                    }
                }
            }
            //find val
            maxfindst1_done.write(1);
        }
        else
        {
            maxfindst1_done.write(0);
        }
    }
}
void ICH_entry::maxfindst2()
{   
    int i,j,k;
    int diff_buff_st2[3][8][3];            //for st 2. 8 entry
    if(!reset.read())
    {
        maxfindst2_done.write(0);        
    }
    else
    {
        if(maxfindst1_done.read() == 1)
        {   
            for(i = 0; i<3;i++)
            {
                for(j = 0; j< 8; j++)
                {
                    for(k = 0; j< 3; k++)
                    {
                        diff_buff_st2[i][j][k] = diff_buff_st1_ff[i][j][k].read();
                    }
                }
            }

            for(i = 0; i < 3; i++)
            {
                for(j = 0; j < 4; j++)
                {   
                    for(k = 0; k< 3; k++)
                    {
                    if(diff_buff_st2[i][j][k] > diff_buff_st2[i][7-j][k])
                        diff_buff_st2_ff[i][j][k].write(diff_buff_st2[i][j][k]); 
                    else
                        diff_buff_st2_ff[i][j][k].write(diff_buff_st2[i][7-j][k]);    
                    }
                }
            }
            //find val
            maxfindst2_done.write(1);
        }
        else
        {
            maxfindst2_done.write(0);
        }
    }
}
void ICH_entry::maxfindst3()
{   
    int i,j,k;
    int diff_buff_st3[3][4][3];            //for st 3. 4 entry
    if(!reset.read())
    {
        maxfindst3_done.write(0);        
    }
    else
    {
        if(maxfindst2_done.read() == 1)
        {   
            for(i = 0; i<3;i++)
            {
                for(j = 0; j< 4; j++)
                {
                    for(k = 0; j< 3; k++)
                    {
                        diff_buff_st3[i][j][k] = diff_buff_st2_ff[i][j][k].read();
                    }
                }
            }

            for(i = 0; i < 3; i++)
            {
                for(j = 0; j < 2; j++)
                {   
                    for(k = 0; k< 3; k++)
                    {
                    if(diff_buff_st3[i][j][k] > diff_buff_st3[i][3-j][k])
                        diff_buff_st3_ff[i][j][k].write(diff_buff_st3[i][j][k]); 
                    else
                        diff_buff_st3_ff[i][j][k].write(diff_buff_st3[i][3-j][k]);    
                    }
                }
            }
            //find val
            maxfindst3_done.write(1);
        }
        else
        {
            maxfindst3_done.write(0);
        }
    }
}
void ICH_entry::maxfindst4()
{   
    int i,j,k;
    int diff_buff_st4[3][2][3];            //for st 4. 2 entry
	int best[3];
    if(!reset.read())
    {
        maxfindst4_done.write(0);        
    }
    else
    {
        if(maxfindst3_done.read() == 1)
        {
            for(i = 0; i<3;i++)
            {
                for(j = 0; j< 2; j++)
                {
                    for(k = 0; j< 3; k++)
                    {
                        diff_buff_st4[i][j][k] = diff_buff_st3_ff[i][j][k].read();
                    }
                }
            }

            for(i = 0; i < 3; i++)
            {
                for(j = 0; j < 1; j++)
                {   
                    for(k = 0; k< 3; k++)
                    {
                    if(diff_buff_st4[i][j][k] > diff_buff_st4[i][1-j][k])
                        best_buff[i][j][k] = diff_buff_st4[i][j][k];                                                
                    else
                        best_buff[i][j][k] = diff_buff_st4[i][1-j][k];                   
                    }
                }
            }
            for(i = 0; i< 3; i++)
            {
            if( (best_buff[0][0][i] >= best_buff[1][0][i]) && (best_buff[0][0][i] >= best_buff[2][0][i]) )
                best[i] = best_buff[0][0][i];
            else if( (best_buff[0][0][i] <= best_buff[1][0][i]) && (best_buff[1][0][i] >= best_buff[2][0][i]) )
                best[i] =  best_buff[1][0][i];
            else
                best[i] = best_buff[2][0][i];
            }
            //find val


            //hit index  update
            ICH_decision.write(hit);
            for(i =0; i< 3; i++)
                err[i].write(best[i]);

            maxfindst4_done.write(1);               //final value out
        }
        else
        {
            maxfindst4_done.write(0);
        }
    }
}
int sc_main(int argc, char* argv[])
{    
    sc_int<26> ini;
    ini =  0;
    int i;

    //IO declare
    sc_clock clock("clock",1);
    sc_in<bool> reset;
	sc_in<bool> ICH_enable;		//1 ICH use, 0 no ICH 
	sc_in<bool> mode;			//1 P-mode, 0 ICH-mode
	sc_in<bool> newgroup;		//1 new group 0 no
	sc_in<int> group;			//number of group vpos
	sc_in<int> linenumber;		//line number     hpo
	sc_in<sc_int<26>> ycocg_in[3];
	sc_in<sc_int<26>> recon[3];
	
	sc_in<sc_uint<5>> index[3];
	sc_in<sc_int<26>>preline[9];
	sc_in<int> qp;
	//
	sc_out<int> err[3];				//max over pixels in group,for abs form 0 = y, 1 = co, 2 = cg
	sc_out<int> index_o[3];
	sc_out<int> ICH_decision;

	sc_out<int>ichmode_end;
	sc_out<int>pmode_end;

    //module
    ICH_entry encoder("ICH");
    encoder.clock(clock);
    encoder.reset();
    encoder.mode();
    encoder.newgroup();
    encoder.group();
    encoder.linenumber();
    
    encoder.preline[]();
    encoder.qp();
    
  
    encoder.ICH_decision();
    encoder.ichmode_end();
    encoder.pmode_end();

    for(i = 0; i < 3; i++)
    {
        encoder.ycocg_in[i](ycocg_in[i]);
        encoder.recon[i](recon[i]);
        encoder.index[i](index[i]);
        encoder.err[i](err[i]);
        encoder.index_o[i](index_o[i]);
    }
    for(i = 0; i < 9; i++)
        encoder.preline[i](preline[i]);




    //trace

	return 0;
}