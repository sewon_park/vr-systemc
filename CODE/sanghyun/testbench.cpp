#include "systemc.h"
//#include "ICH_entry.h"
//#include "ICH_decode.h"
#include "ICH_DECODER_TOP.h"
#include "ICH_ENCODER_TOP.h"



int sc_main(int argc, char* argv[])
{           
    int i;
    sc_int<26> ini;
    ini = 0;
    //decoder module
	//ICH_decode DECODER("DECODER");
		ICH_decoder_top DECODER("DECODER");

    //Encoder module 
	//ICH_entry ENCODER("ENCODER");
		ICH_encoder_top ENCODER("ENCODER");



    //Decoder signal

    sc_clock clock("clock", 1);
    sc_signal<bool> reset;
    sc_signal<bool>mode;
    sc_signal<bool> ICH_enable;
    sc_signal<bool> newgroup;
    sc_signal<int> group;
    sc_signal<int> linenumber;

    sc_signal<sc_int<26>>recon[3];
    sc_signal<sc_int<26>>ycocg_o[3];
    //sc_signal<int>index[3];
	sc_signal<sc_uint<5>>index[3];
    sc_signal<sc_int<26>>preline[9];
    
    //Encoder signal
    //sc_clock clock("clock",1);
    //sc_signal<bool> reset;
    //sc_signal<bool> mode;
    //sc_signal<bool> ICH_enable;
    //sc_signal<bool> newgroup;

    //sc_signal<int> group;
    //sc_signal<int> linenumber;
    
    sc_signal<sc_int<26>>ycocg_in[3];
    //sc_signal<sc_int<26>>recon[3];
    //sc_signal<int>index[3];
    //sc_signal<sc_int<26>>preline[9];
    //sc_signal<int>state_o;
    sc_signal<int>err[3];
    sc_signal<int>index_o[3];
    //sc_signal<int>state_internal;

    sc_signal<int> qp;
    sc_signal<int> ICH_decision;

	sc_signal<int>ichmode_end_decoder;
	sc_signal<int>pmode_end_decoder;

	sc_signal<int>ichmode_end_encoder;
	sc_signal<int>pmode_end_encoder;

    //port

    //Encoder
        ENCODER.clock(clock);
		ENCODER.reset(reset);
		ENCODER.ICH_enable(ICH_enable);
		ENCODER.mode(mode);
		ENCODER.newgroup(newgroup);
		ENCODER.group(group);
		ENCODER.linenumber(linenumber);
		ENCODER.qp(qp);
		ENCODER.ICH_decision(ICH_decision);	
		ENCODER.ichmode_end(ichmode_end_encoder);
		ENCODER.pmode_end(pmode_end_encoder);
		//ICH.state_o(state_o);

	
		for(i = 0; i< 3; i++)
		{
			ENCODER.ycocg_in[i](ycocg_in[i]);
			ENCODER.recon[i](recon[i]);
			
			ENCODER.index_o[i](index_o[i]);
			ENCODER.err[i](err[i]);
		}
		for(i = 0; i<9;i++)
			ENCODER.preline[i](preline[i]);
		
		for(i = 0; i< 3; i++)
		ENCODER.index[i](index[i]);
		//ENCODER.index[0](index[0]);
		//ENCODER.index[1](index[1]);
		//ENCODER.index[2](index[2]);

    //decoder           
		DECODER.clock(clock);
		DECODER.reset(reset);
		DECODER.ICH_enable(ICH_enable);
		DECODER.mode(mode);
		DECODER.newgroup(newgroup);
		DECODER.group(group);
		DECODER.linenumber(linenumber);
		DECODER.pmode_end(pmode_end_decoder);
		DECODER.ichmode_end(ichmode_end_decoder);
		//ICH.state_o(state_o);
		//ICH.recon(recon);

			//ICH.ycocg_in[i](ycocg_in[i]);
			
			//ICH.index_o[i](index_o[i]);
			//ICH.err[i](err[i]);sc_clock CLOCK("clock", 20);.
		for(i = 0; i< 3; i++)
		{
			DECODER.ycocg_o[i](ycocg_o[i]);
			DECODER.index[i](index[i]);
			DECODER.recon[i](recon[i]);
		}

		for(i = 0; i<9;i++)
			DECODER.preline[i](preline[i]);

        sc_trace_file *wf = sc_create_vcd_trace_file("Combine_renow");

        //trace encoder
        sc_trace(wf,clock,"CLK");
		sc_trace(wf,reset,"reset");
		sc_trace(wf,mode,"mode");
		sc_trace(wf,ICH_enable,"ICH_enable"); 
		sc_trace(wf,newgroup,"NEW");		
		sc_trace(wf,linenumber,"LINE");
		sc_trace(wf,index[0],"INDEX0");
		sc_trace(wf,index[1], "INDEX1");
		sc_trace(wf,index[2], "INDEX2");
		sc_trace(wf,ENCODER.encoder->state,"state_encode");
		sc_trace(wf,ycocg_in[0],"ycocg_in0");
		sc_trace(wf,ycocg_in[1],"ycocg_in1");
		sc_trace(wf,ycocg_in[2],"ycocg_in2");
		sc_trace(wf,index_o[0],"index_o0");
		sc_trace(wf,index_o[1],"index_o1");
		sc_trace(wf,index_o[2],"index_o2");
        sc_trace(wf,qp,"QP");
        sc_trace(wf,ICH_decision,"DECISION");
		sc_trace(wf,err[0],"err0");
		sc_trace(wf,err[1],"err1");
		sc_trace(wf,err[2],"err2");

		sc_trace(wf,ichmode_end_encoder,"ichmode_end_encoder");
		sc_trace(wf,pmode_end_encoder,"pmode_end_encoder");
		//sc_trace(wf,DECODER.group_line_read_done,"group line read done");	//why err
        
        //trace decoder

        //sc_trace(wf,clock,"CLK");
		//sc_trace(wf,reset,"reset");
		//sc_trace(wf,mode,"mode");
		//sc_trace(wf,ICH_enable,"ICH_enable");
		//sc_trace(wf,newgroup,"NEW");	
		//sc_trace(wf,linenumber,"LINE");		
		sc_trace(wf,ycocg_o[0],"YCOCG_O0");
		sc_trace(wf,ycocg_o[1],"YCOCG_O1");
		sc_trace(wf,ycocg_o[2],"YCOCG_O2");
		sc_trace(wf,recon[0],"RECON0");
		sc_trace(wf,recon[1],"RECON1");
		sc_trace(wf,recon[2],"RECON2");		
		sc_trace(wf,DECODER.decoder->ichmode_start,"ichstart");
		sc_trace(wf,DECODER.decoder->pmode_start,"pmodestart");
		sc_trace(wf, DECODER.decoder->pmode_line_recon_read_done, "pmodelinerecondone");
		sc_trace(wf, DECODER.decoder->pmode_table_shift_done, "pmodeend");
		sc_trace(wf,DECODER.decoder->state,"state_decode");
		sc_trace(wf,DECODER.decoder->shift_update_ichmode,"ichend");
		sc_trace(wf,DECODER.decoder->ichmode_index_line_read_done,"ichindexread");
		sc_trace(wf,DECODER.decoder->ichmode_mask_reconbuff_update,"ichmask_reconbuff");
		sc_trace(wf, DECODER.decoder->newupdate_start, "groupstart");
		sc_trace(wf, DECODER.decoder->update_table_new, "update");
		
		sc_trace(wf, pmode_end_decoder, "pmodeupdate_end_decoder");
		sc_trace(wf, ichmode_end_decoder, "ichupdate_end_decoder");
		//sc_trace(wf,index[0],"INDEX0");
		//sc_trace(wf,index[1],"INDEX1");
		//sc_trace(wf,index[2],"INDEX2");
        cout<<"COMBINE"<<endl;
        reset = 0;
        mode = 0;
        newgroup = 0;
        ICH_enable = 0;
        linenumber = 0;
        for(i = 0; i < 9; i++)
        preline[i] = ini +i;
        sc_start(6,SC_NS); 
        //cout<<"ICH mode test"<<endl;
        reset = 1;
        //mode = 0;
        ICH_enable = 1;
		//newgroup = 1;
		group = 0;
		linenumber = 0;
		qp = 7;
        //index[0] = 1;
        //index[1] = 2;
        //index[2] = 3;
		ycocg_in[0] = 1;
		ycocg_in[1] = 2;
		ycocg_in[2] = 3;
		//sc_start(2, SC_NS);
		ICH_enable = 1;
		mode = 1;
		recon[0] = 1;
		recon[1] = 2;
		recon[2] = 3;
		sc_start(15 ,SC_NS);
		reset = 1;
		ICH_enable = 0;
		newgroup = 1;
		group=0;		
		linenumber = 1;
		sc_start(4, SC_NS);
		ICH_enable = 1;
		newgroup = 0;
		mode = 0;
		index[0] = 2;
		index[1] = 5;
		index[2] = 8;
		sc_start(6, SC_NS);
		
        //sc_start(20,SC_NS);
		
		sc_stop();


    


		return 1;

}