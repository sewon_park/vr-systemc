//#include "parameter.h"
#include "PrefixParameter.h"
SC_MODULE(ICH_entry)
{
	//In
	sc_in<bool>	clock;
	sc_in<bool> reset;
	sc_in<bool> ICH_enable;		//1 ICH use, 0 no ICH 
	sc_in<bool> mode;			//1 P-mode, 0 ICH-mode
	sc_in<bool> newgroup;		//1 new group 0 no
	sc_in<int> group;			//number of group vpos
	sc_in<int> linenumber;		//line number     hpos
	sc_in<sc_int<26>> ycocg_in[PIXELS_PER_GROUP];
	sc_in<sc_int<26>> recon[PIXELS_PER_GROUP];
	//sc_in<int> index[3];
	sc_in<sc_uint<5>> index[PIXELS_PER_GROUP];
	sc_in<sc_int<26>> preline[SLICE_WIDTH];
	sc_in<int> qp;
	//
	//sc_out<int> err[PIXINGROUP];				//max over pixels in group,for abs form 0 = y, 1 = co, 2 = cg
	sc_out<int> index_o[PIXELS_PER_GROUP];
	sc_out<int> ICH_decision;

	//sc_out<int>ichmode_end;
	//sc_out<int>pmode_end;
	//renew signal
	sc_out<int> update_end;
	sc_out<int>logerrichmode;
	sc_out<int>errflag;
	//sc_out<int>state_o;
	//local variable
	sc_signal<int> state;
	int index_buff[PIXELS_PER_GROUP];
	sc_int<26>recon_buff[PIXELS_PER_GROUP];
	sc_int<26>table[ICH_SIZE];					//include prefixparameter.h


	int reset_r;
	int ICH_enable_r;
	int mode_r,newgroup_r;
	// valid[32] => check index
	// 1 => ICHmode index , 0 else
	int mask[ICH_SIZE] = {0,0,0,0,0,0,0,0,
					 0,0,0,0,0,0,0,0,
					 0,0,0,0,0,0,0,0,
					 0,0,0,0,0,0,0,0};
	int valid[ICH_SIZE] = { 0,0,0,0,0,0,0,0,
					  0,0,0,0,0,0,0,0,
					  0,0,0,0,0,0,0,0,
					  0,0,0,0,0,0,0,0};
					  
	int group_r,linenumber_r;
	//
	const int QuantDivisor[13] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };
	//const int QuantOffset[] = {0, 0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047 };
	int qlevel_luma_8bpc[16] = {0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 7 };
	int qlevel_chroma_8bpc[16] = {0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 8, 8 };
	//int qlevel_luma_10bpc[] = {0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 7, 8, 9 };
	//int qlevel_chroma_10bpc[] = {0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 10, 10, 10 };
	//int qlevel_luma_12bpc[] = {0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 9, 10, 11 };
	//int qlevel_chroma_12bpc[] = {0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 12, 12, 12 }; 

	//function
	void statemachine();
	int behavstate();

	void idle();

	int pmode();
	
	int ichmode();


	//int update();			//don't need updatestate 

	int newgroup_errsearch();
	int newgroup_update();
	//

	SC_CTOR(ICH_entry)
		{
			SC_METHOD(statemachine);
			//dont_initialize();
			sensitive << clock.pos();
			//SC_CTHREAD(statemachine,clock.pos());
		}

	



	/*
	{
		sc_in<bool> clock;
		sc_in<bool> reset;
		sc_in<bool> mode;					//1: p mode 0:ICH moode
		sc_in<bool> next_group;
		sc_in<sc_int<26> > ycocg[3];
		sc_in<sc_int<15> > entry;			// index request 2,1,0
		//sc_in<sc_lv<26 * 3> >value;			// 2 1 0
		sc_in<bool>ICH_enable;



		sc_out<sc_int<26>> err[3];
		
		sc_in<sc_int<26> > value0;
		sc_in<sc_int<26> > value1;
		sc_in<sc_int<26> > value2;



		int val0,val1,val2;
		int entry_arry[3];

		int buff[3];
		int table[32] = {0,0,0,0,0,0,0,0,
						 0,0,0,0,0,0,0,0,
						 0,0,0,0,0,0,0,0,
						 0,0,0,0,0,0,0,0};
		int state;
		  //int state;
		int ycocg_in0,ycocg_in1,ycocg_in2;

		bool group;
		//
		//sc_lv<26> val0,val1,val2;
		
		
		sc_int<5> entry_arry[3];

		sc_int<26> val0;
		sc_int<26> val1;
		sc_int<26> val2;

		//assign entry0 = entry[5-1:0]
		
		entry_arry[0] = entry.read().range(4, 0);			//request 0
		entry_arry[1] = entry.read().range(9,5);			//request 1
		entry_arry[2] = entry.read().range(14,10);			//request 2
		
		//
		val0 = value0.read();					//request 0
		val1 = value1.read();					//request 1
		val2 = value2.read();					//request 2
		
		//

		int match();

		void statemachine();

		void idle();
		int statebehav();

		int shift_select();			//use in select state shift 3,2,1,0 && fill buff table -> buff
		

		int shift_update();			//use in update state just shift 3
		int buff_value();   	    //use in update state value -> buff

		int buff_table();		    //use in shift state table <- buff

		SC_CTOR(ICH_entry)
		{
			SC_METHOD(statemachine);
			//dont_initialize();
			sensitive << clock.pos();
			//SC_CTHREAD(statemachine,clock.pos());
		}
	}
	*/




};