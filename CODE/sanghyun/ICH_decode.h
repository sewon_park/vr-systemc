#include "PrefixParameter.h"
SC_MODULE(ICH_decode)
{
	//In
	sc_in<bool>	clock;
	sc_in<bool> reset;
	sc_in<bool> ICH_enable;		//1 ICH use, 0 no ICH
	sc_in<bool> mode;			//1 P-mode, ICH-mode
	sc_in<bool> newgroup;		//1 new group 0 no
	sc_in<int> group;			//number of group
	sc_in<int> linenumber;		//line number	
	sc_in<sc_int<26>> recon[PIXELS_PER_GROUP];	
	sc_in<sc_uint<5>>index[PIXELS_PER_GROUP];
	sc_in<sc_int<26>>preline[SLICE_WIDTH];		
    sc_out<sc_int<26>>ycocg_o[PIXELS_PER_GROUP];
	//sc_out<int>ichmode_end;
	//sc_out<int>pmode_end;

	sc_out<int>update_end;
	//local variable
	sc_signal<int> state;
	int index_buff[PIXELS_PER_GROUP];
	sc_int<26>recon_buff[PIXELS_PER_GROUP];
	sc_int<26> table[ICH_SIZE];	//include prefixparameter.h
	

	sc_signal<int>pmode_start,pmode_line_recon_read_done;
	sc_signal<int>pmode_table_shift_done,pmode_table_update_done;
	sc_signal<int>ichmode_start,ichmode_index_line_read_done;
	sc_signal<int>ichmode_mask_reconbuff_update,shift_update_ichmode;
	sc_signal<int>newupdate_start,group_line_read_done,update_table_new;

	sc_int<26>prebuff[SLICE_WIDTH];

	sc_signal<int> reset_r;
	sc_signal<int> ICH_enable_r;
	sc_signal<int> mode_r,newgroup_r;
	int line;
	int groupnum;

	int next_state_d ;

	// 1 => ICHmode index , 0 else
	int mask[ICH_SIZE] = {0,0,0,0,0,0,0,0,
					 0,0,0,0,0,0,0,0,
					 0,0,0,0,0,0,0,0,
					 0,0,0,0,0,0,0,0};

	int group_r,linenumber_r;
	int valid[ICH_SIZE] = { 0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0,
					0,0,0,0,0,0,0,0};
//

	//function
	void statemachine();
	void behavstate();

	//
	void line_recon_read_pmode();
	//void table_shift_pmode();			//empty
	//void table_update_pmode();

	void index_line_read();
	void mask_reconbuff_update();
	

	void group_line_read();
	

	void shift_table();
	

	void idle();



	SC_CTOR(ICH_decode)
		{
			SC_METHOD(statemachine);
			//dont_initialize();
			sensitive << clock.pos();

			SC_METHOD(behavstate);
			sensitive<<ICH_enable;
			sensitive<<mode;
			sensitive<<newgroup;
			sensitive<<pmode_start;
			sensitive<<pmode_line_recon_read_done;
			sensitive<<pmode_table_shift_done;
			sensitive<<pmode_table_update_done;
			sensitive<<ichmode_start;
			sensitive<<ichmode_index_line_read_done;
			sensitive<<ichmode_mask_reconbuff_update;
			sensitive<<shift_update_ichmode;
			sensitive<<newupdate_start;
			sensitive<<group_line_read_done;
			sensitive<<update_table_new;
			sensitive<<state;

			SC_METHOD(line_recon_read_pmode);			
			sensitive<<clock.pos();
			
			SC_METHOD(index_line_read);
			sensitive<<clock.pos();

			SC_METHOD(mask_reconbuff_update);
			sensitive<<clock.pos();

			SC_METHOD(group_line_read);
			sensitive<<clock.pos();

			SC_METHOD(shift_table);
			sensitive<<clock.pos();

		}

};