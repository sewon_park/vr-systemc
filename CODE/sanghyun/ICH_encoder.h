
SC_MODULE(ICH_entry)
{
	//In
	sc_in<bool>	clock;
	sc_in<bool> reset;
	sc_in<bool> ICH_enable;		//1 ICH use, 0 no ICH 
	sc_in<bool> mode;			//1 P-mode, 0 ICH-mode
	sc_in<bool> newgroup;		//1 new group 0 no
	sc_in<int> group;			//number of group vpos
	sc_in<int> linenumber;		//line number     hpo
	sc_in<sc_int<26>> ycocg_in[3];
	sc_in<sc_int<26>> recon[3];
	//sc_in<int> index[3];
	sc_in<sc_uint<5>> index[3];
	sc_in<sc_int<26>>preline[9];
	sc_in<int> qp;
	//
	sc_out<int> err[3];				//max over pixels in group,for abs form 0 = y, 1 = co, 2 = cg
	sc_out<int> index_o[3];
	sc_out<int> ICH_decision;

	sc_out<int>ichmode_end;
	sc_out<int>pmode_end;
	//sc_out<int>state_o;
	//local variable
	sc_signal<int> state;
	int index_buff[3];
	sc_int<26>recon_buff[3];
	sc_int<26>table[32];	//include prefixparameter.h

    int next_state_d;
	
	// valid[32] => check index
	// 1 => ICHmode index , 0 else
	int mask[32] = {0,0,0,0,0,0,0,0,
					 0,0,0,0,0,0,0,0,
					 0,0,0,0,0,0,0,0,
					 0,0,0,0,0,0,0,0};
    int line;
	int groupnum;
	int group_r,linenumber_r;
	//
	const int QuantDivisor[13] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096 };
	//const int QuantOffset[] = {0, 0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047 };
	int qlevel_luma_8bpc[16] = {0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 5, 6, 7 };
	int qlevel_chroma_8bpc[16] = {0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 8, 8, 8 };
	//int qlevel_luma_10bpc[] = {0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 7, 8, 9 };
	//int qlevel_chroma_10bpc[] = {0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 10, 10, 10 };
	//int qlevel_luma_12bpc[] = {0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 9, 10, 11 };
	//int qlevel_chroma_12bpc[] = {0, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 12, 12, 12 }; 
    int diff[3][32][3];                 //store absolute value of err
    int maxerr[3] = {0,0,0};
    int hit = 0;
    //signal
    sc_signal<int>pmode_start,pmode_line_recon_read_done;
	sc_signal<int>pmode_table_shift_done,pmode_table_update_done;
	sc_signal<int>ichmode_start,ichmode_index_line_read_done;
	sc_signal<int>ichmode_mask_reconbuff_update,shift_update_ichmode;
	sc_signal<int>newupdate_start,group_line_read_done,update_table_new;

    sc_int<26>prebuff[9];

	sc_signal<int> reset_r;
	sc_signal<int> ICH_enable_r;
	sc_signal<int> mode_r,newgroup_r;

    sc_signal<int> errcal_done,maxQperr_done,store_err_to_ff_done,compare_done;
    sc_signal<int> minfindst0_done,minfindst1_done,minfindst2_done,minfindst3_done,minfindst4_done;

    sc_signal<int> maxfindst0_done,maxfindst1_done,maxfindst2_done,maxfindst3_done,maxfindst4_done;
    //ff
    sc_signal<int>group_ff,line_ff,qp_ff;
    sc_signal<sc_int<8>>y_ff[3];
    sc_signal<sc_int<9>>co_ff[3];
    sc_signal<sc_int<9>>cg_ff[3];
    sc_signal<int>maxerr_ff[3];
    sc_signal<int>diff_ff[3][32][3];
    
    sc_signal<int>diff_buff_st0_min_ff[3][16][3];
    sc_signal<int>index_buff_st0_ff[3][16];
    
    sc_signal<int>diff_buff_st1_min_ff[3][8][3];
    sc_signal<int>index_buff_st1_ff[3][8];

    sc_signal<int>diff_buff_st2_min_ff[3][4][3];
    sc_signal<int>index_buff_st2_ff[3][4];

    sc_signal<int> diff_buff_st3_min_ff[3][2][3];
    sc_signal<int> index_buff_st3_ff[3][2];

    //sc_signal<int> diff_buff_st3_min_ff[3][0][3];
    sc_signal<int> diff_buff_st0_ff[3][16][3];
    sc_signal<int> diff_buff_st1_ff[3][8][3];
    sc_signal<int> diff_buff_st2_ff[3][4][3];
    sc_signal<int> diff_buff_st3_ff[3][2][3];
    sc_signal<int> best_buff[3][1][3];














	
	



	//function
	void idle();                //combi

    void statemachine();        //sequen

    void behavstate();          //combi
    void line_recon_read_pmode();   //se
    void table_shift_pmode();       //se
    void index_line_read();         //se
    void mask_reconbuff_update();   //se
    void shift_update_ichmode_f();  //se
    void group_line_read();         //se
    void update_table_new_f();      //se
    void shift_table();             //se

    void errcal();                  //combi
    void maxQperr();                //combi
    void store_err_to_ff();         //sequen
    void compare();                 //combi
    void minfindst0();              //se
    void minfindst1();              //se
    void minfindst2();              //se
    void minfindst3();              //se
    void minfindst4();              //se

    void maxfindst0();              //se
    void maxfindst1();              //se
    void maxfindst2();              //se
    void maxfindst3();              //se
    void maxfindst4();              //se

    


	SC_CTOR(ICH_entry)
		{   
            //sequentail
			SC_METHOD(statemachine);
            sensitive << clock.pos();

            SC_METHOD(line_recon_read_pmode);
            sensitive << clock.pos();

            SC_METHOD(table_shift_pmode);
            sensitive << clock.pos();

            SC_METHOD(index_line_read);
            sensitive << clock.pos();

            SC_METHOD(mask_reconbuff_update);
            sensitive << clock.pos();

            
            SC_METHOD(shift_update_ichmode_f);
            sensitive << clock.pos();
            
            SC_METHOD(group_line_read);
            sensitive << clock.pos();
            
            SC_METHOD(update_table_new_f);
            sensitive << clock.pos();
            
            SC_METHOD(shift_table);
            sensitive << clock.pos();
            
            SC_METHOD(minfindst0);
            sensitive << clock.pos();
            
            SC_METHOD(minfindst1);
            sensitive << clock.pos();
            
            SC_METHOD(minfindst2);
            sensitive << clock.pos();
            
            SC_METHOD(minfindst3);
            sensitive << clock.pos();
            
            SC_METHOD(minfindst4);
            sensitive << clock.pos();
            
            SC_METHOD(maxfindst0);
            sensitive << clock.pos();
            SC_METHOD(maxfindst1);
            sensitive << clock.pos();
            SC_METHOD(maxfindst2);
            sensitive << clock.pos();
            SC_METHOD(maxfindst3);
            sensitive << clock.pos();
            SC_METHOD(maxfindst4);
            sensitive << clock.pos();

            //combi
            SC_METHOD(behavstate);
            sensitive<<ICH_enable;
			sensitive<<mode;
			sensitive<<newgroup;
			sensitive<<pmode_start;
			sensitive<<pmode_line_recon_read_done;
			sensitive<<pmode_table_shift_done;
			sensitive<<pmode_table_update_done;
			sensitive<<ichmode_start;
			sensitive<<ichmode_index_line_read_done;
			sensitive<<ichmode_mask_reconbuff_update;
			sensitive<<shift_update_ichmode;
			sensitive<<newupdate_start;
			sensitive<<group_line_read_done;
			sensitive<<update_table_new;
            sensitive<<minfindst4_done;
            sensitive<<maxfindst4_done;
			sensitive<<state;

            //SC_METHOD(idle);        //DOES NEED ?
            
            SC_METHOD(errcal);
            sensitive<<update_table_new;
            sensitive<<reset;


            SC_METHOD(maxQperr);
            sensitive<<group_line_read_done;
            sensitive<<reset;

            SC_METHOD(compare);
            sensitive<<reset;
            sensitive<<store_err_to_ff_done;
            

		}

	




};