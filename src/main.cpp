#include <systemc.h>

//#include "../Header/RC_top.h"
#include "../header/codec_main.h"
int sc_main(int argc, char* argv[])
{
  sc_set_time_resolution(1, SC_PS);
  sc_time             t1(1, SC_PS);


  sc_signal <bool> ForceMpp,rst, start_en, ready, completeVlc, buffer_complete, qp_ready;
  sc_signal <int> vPos, group_count;
  sc_signal <unsigned> state_out;
  sc_signal <int> qLevel;
  sc_clock clk("clk",2,SC_PS);

  sc_signal <int> numBits, bufferFullness, sampModCnt, tgtOffsetLo, tgtOffsetHi;
  sc_signal <int> rcModelSize, initialFullnessOfs, groupsPerLine, scale_increment_interval, firstLineBpgOfs, initialDelay, pixelCount, bitsPerPixel;
  sc_signal <int> slicew, sliceWidth, groups_total, num_extra_mux_bits;
  sc_signal <int> sliceh, sliceHeight, rcEdgeFactor, quantIncrLimit0, quantIncrLimit1, new_quant;

  sc_signal <int> slice_width;
  sc_signal <int> slice_height;
  codec_main codec_main("MAIN_TOP");
  //RC_functions RF_func("FUNC");

  codec_main.qp_ready(qp_ready);
  codec_main.new_quant(new_quant);
  codec_main.rc_quant_incr_limit0(quantIncrLimit0);
  codec_main.rc_quant_incr_limit1(quantIncrLimit1);
  codec_main.rc_edge_factor(rcEdgeFactor);

  codec_main.ForceMpp(ForceMpp);
  codec_main.CLK(clk);
  codec_main.RST_N(rst);
  codec_main.start_en(start_en);

  codec_main.completeVlc(completeVlc);
  codec_main.numBits(numBits);
  codec_main.sampModCnt(sampModCnt);
  codec_main.rcModelSize(rcModelSize);
  codec_main.initial_offset(initialFullnessOfs);

  codec_main.vPos(vPos);
  codec_main.groupsPerLine(groupsPerLine);
  codec_main.scale_increment_interval(scale_increment_interval);
  codec_main.initial_xmit_delay(initialDelay);

  codec_main.qLevel(qLevel);
  codec_main.bufferFullness(bufferFullness);
  codec_main.firstLineBpgOfs(firstLineBpgOfs);
  codec_main.slice_height(slice_height);

  codec_main.bitsPerPixel(bitsPerPixel);
  codec_main.pixelCount(pixelCount);
  codec_main.slice_width(slice_width);
  codec_main.num_extra_mux_bits(num_extra_mux_bits);

  codec_main.rc_tgt_offset_lo(tgtOffsetLo);
  codec_main.rc_tgt_offset_hi(tgtOffsetHi);
 /* RC_top.CLK(clk);
  RC_top.RST_N(rst);
  RC_top.start_en(start_en);
  RC_top.qLevel(qLevel);
  RC_top.forceMpp(forceMpp);
  RC_top.numBits(numBits);
  RC_top.bufferFullness(bufferFullness);
  RC_top.completeVlc(completeVlc);
  RC_top.rc_model_size(rcModelSize);
  RC_top.sampModCnt(sampModCnt);
  RC_top.initial_offset(initialFullnessOfs);
  RC_top.vPos(vPos);
  RC_top.group_count(group_count);
  RC_top.groupsPerLine(groupsPerLine);*/
  sc_trace_file *wf = sc_create_vcd_trace_file("Output");
  sc_trace(wf, codec_main.rc_top->RST_N, "rst_n");
  sc_trace(wf, codec_main.rc_top->CLK, "Clock");
  sc_trace(wf, codec_main.rc_top->start_en, "start_en");
  sc_trace(wf, codec_main.rc_top->qLevel, "qLevel");
  sc_trace(wf, codec_main.rc_top->state_out, "State Out");
  sc_trace(wf, codec_main.rc_top->completeVlc, "completeVlc");
  sc_trace(wf, codec_main.rc_top->numBits, "numBits");
  sc_trace(wf, codec_main.rc_top->bufferFullness, "bufferFullness");
  sc_trace(wf, codec_main.rc_top->rc_func->prev_numBits, "prev_numBits");
  sc_trace(wf, codec_main.rc_top->rc_func->curr_numBits, "curr_numBits");
  sc_trace(wf, codec_main.rc_top->rc_func->codedGroupSize, "codedGroupSize");
  sc_trace(wf, codec_main.rc_top->rc_func->maxBitsPerGroup, "maxBitsPerGroup");
  sc_trace(wf, codec_main.rc_top->rc_func->bugFixCondition, "bugFixCondition");
  sc_trace(wf, codec_main.rc_top->rc_func->adjFullness, "adjFullness");
  sc_trace(wf, codec_main.rc_top->rc_func->scale, "scale");
  sc_trace(wf, codec_main.rc_top->rc_func->rcModelBufferFullness, "rcModelBufferFullness");
  sc_trace(wf, codec_main.rc_top->rc_func->throttle_offset, "throttle_offset");
  sc_trace(wf, codec_main.rc_top->range_ready, "range_ready");
  sc_trace(wf, codec_main.rc_top->rc_func->rc_model_size, "rcModelSize");
  sc_trace(wf, codec_main.rc_top->rc_func->i, "index");
  sc_trace(wf, codec_main.rc_top->rc_func->selected_range, "selected_range");
  sc_trace(wf, codec_main.rc_top->rc_func->prevRage, "prevRage");
  sc_trace(wf, codec_main.rc_top->rc_func->initial_offset, "initial_offset");
  sc_trace(wf, codec_main.rc_top->rc_func->initial_scale_value, "initial_scale_value");
  sc_trace(wf, codec_main.rc_top->rc_func->group_count, "group_count");
  sc_trace(wf, codec_main.rc_top->rc_func->vPos, "vPos");
  sc_trace(wf, codec_main.rc_top->scale_decrement_interval, "scale_decrement_interval");
  sc_trace(wf, codec_main.ForceMpp, "ForceMpp");
  sc_trace(wf, codec_main.initial_scale_value, "Initial_scale_value");
  sc_trace(wf, codec_main.scale_decrement_interval, "Scale_decrement_Interval");
  sc_trace(wf, codec_main.rc_top->rc_func->min_QP, "min_QP");
  sc_trace(wf, codec_main.rc_top->rc_func->max_QP, "max_QP");
  sc_trace(wf, codec_main.rc_top->rc_func->selected_range, "selected_range");
  sc_trace(wf, codec_main.slice_height, "slice_height");
  sc_trace(wf, codec_main.slice_width, "slice_width");
  sc_trace(wf, codec_main.first_line_bpg_ofs, "first_line_bpg_ofs");
  sc_trace(wf, codec_main.nfl_bpg_offset, "nfl_bpg_offset");
  sc_trace(wf, codec_main.rc_top->rc_func->increment, "increment");
  sc_trace(wf, codec_main.rc_top->rc_func->currentScale, "currentScale");
  sc_trace(wf, codec_main.rc_top->rc_func->scaleAdjustCounter, "scaleAdjustCounter");
  sc_trace(wf, codec_main.rc_top->rc_func->scaleIncrementStart, "scaleIncrementStart");
  sc_trace(wf, codec_main.rc_top->rc_func->current_bpg_target, "current_bpg_target");
  sc_trace(wf, codec_main.rc_top->rc_func->first_line_bpg_ofs, "First_line_bpg_ofs");
  sc_trace(wf, codec_main.rc_top->rc_func->unity_scale, "unity_scale");
  sc_trace(wf, codec_main.rc_top->rc_func->bits_per_pixel, "bits_per_pixel");
  sc_trace(wf, codec_main.rc_top->rc_func->num_pixels, "num_pixels");
  sc_trace(wf, codec_main.rc_top->rc_func->output_ready, "output_ready");
  sc_trace(wf, codec_main.rc_top->rc_func->throttleFrac, "throttleFrac");
  sc_trace(wf, codec_main.rc_top->rc_func->rcXformOffset, "rcXformOffset");

  sc_trace(wf, codec_main.target_bpp_x16, "target_bpp_x16");
  sc_trace(wf, codec_main.bits_per_pixel, "bits_per_pixel");
  sc_trace(wf, codec_main.slice_bpg_offset, "slice_bpg_offset");
  sc_trace(wf, codec_main.groups_total, "groups_total");

  
  sc_trace(wf, codec_main.rc_top->rc_func->compare_idx_max, "compare_idx_max");
  sc_trace(wf, codec_main.rc_top->rc_func->compare_idx_min, "compare_idx_min");

  
  sc_trace(wf, codec_main.groupsPerLine, "groupsPerLine");
  sc_trace(wf, codec_main.rc_top->rc_func->pixelCount, "pixelCount");
  sc_trace(wf, codec_main.rc_top->rc_func->prevPixelCount, "prevPixelCount");
  sc_trace(wf, codec_main.rc_top->rc_func->bpg_offset, "bpg_offset");
  sc_trace(wf, codec_main.rc_top->rc_func->scale, "scale");
  sc_trace(wf, codec_main.rc_top->rc_func->test, "TTTTest");
  sc_trace(wf, codec_main.rc_top->rc_func->rcModelBufferFullness, "RcModelBufferFullness");

  sc_trace(wf, codec_main.rc_top->rc_func->rcTgtBitsGroup, "rcTgtBitsGroup");
  sc_trace(wf, codec_main.rc_top->rc_func->tgtMinusOffset, "tgtMinusOffset");
  sc_trace(wf, codec_main.rc_top->rc_func->tgtPlusOffset, "tgtPlusOffset");
  sc_trace(wf, codec_main.rc_top->rc_func->incr_amount, "incr_amount");
  sc_trace(wf, codec_main.rc_top->rc_func->rcSizeGroup, "rcSizeGroup");
  sc_trace(wf, codec_main.rc_top->rc_func->rcSizeUnit[0], "rcSizeUnit[0]");
  sc_trace(wf, codec_main.rc_top->rc_func->rcSizeUnit[1], "rcSizeUnit[1]");
  sc_trace(wf, codec_main.rc_top->rc_func->rcSizeUnit[2], "rcSizeUnit[2]");

  sc_trace(wf, codec_main.rc_top->rc_func->stQp, "stQp");
  sc_trace(wf, codec_main.rc_top->rc_func->curQp, "curQp");
  sc_trace(wf, codec_main.rc_top->rc_func->prevQp, "prevQp");
  sc_trace(wf, codec_main.rc_top->rc_func->prev2Qp, "prev2Qp");
  sc_trace(wf, codec_main.rc_top->rc_func->rcSizeGroupPrev, "rcSizeGroupPrev");
  sc_trace(wf, codec_main.rc_top->rc_func->new_quant, "new_quant");
  sc_trace(wf, codec_main.rc_top->rc_func->qp_ready, "qp_ready");
  sc_trace(wf, codec_main.rc_top->rc_func->overflowAvoid, "overflowAvoid");
  
  rcEdgeFactor = 6;
  quantIncrLimit0 = 11;
  quantIncrLimit1 = 11;

  tgtOffsetHi = 3;
  tgtOffsetLo = 3;
  num_extra_mux_bits = 0;
  pixelCount = 1;
  bitsPerPixel = 8.0;
  initialFullnessOfs = 6144;
  initialDelay = 170;
  slice_height = 2;
  slice_width = 6;
  // slice_width+2 / 3
  groupsPerLine = 48;

  rcModelSize = 8192;
  firstLineBpgOfs = -1;

  rst.write(0);
  sc_start(t1);
  rst.write(1);
  sc_start(t1);

  start_en.write(1);
  sc_start(t1);
  start_en.write(0);




  sc_start(t1*10);
  start_en.write(1);
  sc_start(t1*2);
  start_en.write(0);
  sc_start(t1 * 2);
  completeVlc.write(1);
  numBits.write(7000);
  sc_start(t1*5);
  completeVlc.write(0);
  numBits.write(0);
  sc_start(t1*20);


  start_en.write(1);
  completeVlc.write(1);
  numBits.write(600);
  sc_start(t1 * 2);
  completeVlc.write(0);
  start_en.write(0);
  sc_start(t1 * 30);

  start_en.write(1);
  completeVlc.write(1);
  numBits.write(300);
  sc_start(t1 * 5);
  completeVlc.write(0);
  start_en.write(0);

  sc_start(t1 * 40);
  start_en.write(1);
  sc_start(t1 * 5);
  start_en.write(0);
  sc_start(t1 * 30);
  completeVlc.write(1);
  sc_start(t1 * 5);
  completeVlc.write(0);
  sc_start(t1 * 50);

  sc_start(t1 * 40);
  start_en.write(1);
  sc_start(t1 * 5);
  start_en.write(0);
  sc_start(t1 * 30);
  completeVlc.write(1);
  numBits.write(3000);
  sc_start(t1 * 5);
  completeVlc.write(0);
  sc_start(t1 * 50);

  sc_start(t1 * 40);
  start_en.write(1);
  sc_start(t1 * 5);
  start_en.write(0);
  sc_start(t1 * 30);
  completeVlc.write(1);
  numBits.write(1000);
  sc_start(t1 * 5);
  completeVlc.write(0);
  sc_start(t1 * 50);
  sc_stop();

  sc_close_vcd_trace_file(wf);
  return(0);
}
