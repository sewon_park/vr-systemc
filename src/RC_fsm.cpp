#include "../header/RC_fsm.h"
#include "../header/RC_functions.h"


void RC_fsm::state_machine()
{
	sc_uint<3> state_tmp;

	bool BufferComplete;
	//BufferComplete=buffer_complete.read();
	switch(curr_state)
	{
		case initial:
			if(start_en==1){
				curr_state= BufferTracker;
			}
			state_tmp=0;
			state_out.write((unsigned)state_tmp);
			break;
		case BufferTracker:
			curr_state= CompleteVlc;
			state_tmp=1;
			state_out.write((unsigned)state_tmp);
			break;
		case CompleteVlc:
			if(buffer_complete.read()==1){
				curr_state= LinearTrans;
				cout << "Complete to calculate BufferFullness" << endl;
			}
			state_tmp=2;
			state_out.write((unsigned)state_tmp);
			break;
		case LinearTrans:
			curr_state = LongTerm;

			state_tmp=3;
			state_out.write((unsigned)state_tmp);
			break;
		case LongTerm:
			if (range_ready.read() == 1) {
				cout << "Complete to calculate range of QP" << endl;
				curr_state = ShortTerm;
			}
			state_tmp=4;
			state_out.write((unsigned)state_tmp);
			break;
		case ShortTerm:
			curr_state= initial;
			state_tmp=5;
			state_out.write((unsigned)state_tmp);
			break;
		default:
			curr_state= initial;
			state_tmp=0;
			state_out.write((unsigned)state_tmp);
			break;

	}
}
