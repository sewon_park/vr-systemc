#include "../header/RC_functions.h"
#include "../header/parameter.h"


void RC_functions::BufferTracker()
{
  int curr_state;
  int adj_tmp;
  sc_int<8> sample_tmp;
  curr_state = state.read();
  // reset functionality
  if(rst_n.read()==true) {
    sample_tmp   = 0;
  }

  // default settings
  
  //output_ready.write(false);

  // cycle behavior could be as well a case statement
  switch (curr_state) {
	case 0 :
		forceMpp = 0;
		break;
	case 1 :
		for (int j = 0; j < NUM_COMPONENTS; j++)
			rcSizeGroup += rcSizeUnit[j];
		adjFullness.write(bufferFullness);
		maxBitsPerGroup = (bits_per_pixel - 20) >> 3;
		bugFixCondition = (bits_per_pixel * slice_width-1) & 0xf;   // Fractional bit left at end of slice

		if (adjFullness  < maxBitsPerGroup)  // Force MPP is possible in VBR only at end of line to pad chunks to byte boundaries
			forceMpp = 1;
		/*
		if( (bugFixCondition && (numBitsChunk + maxBitsPerGroup + 8 == chunk_size * 8)) ||
		(numBitsChunk + maxBitsPerGroup + 8 > chunk_size * 8))
		{
		adjFullness -= 8;
		if (adjFullness  < maxBitsPerGroup - 3)  // Force MPP is possible in VBR only at end of line to pad chunks to byte boundaries
		forceMpp = 1;
		}
		else if((!vbr_enable) && (pixelCount >= initial_xmit_delay))  // underflow isn't possible if we're not removing bits
		{
		if (adjFullness  < maxBitsPerGroup - 3)
		forceMpp = 1;
		}*/
		break;
	case 2 :
		if (completeVlc) {
			curr_numBits.write(numBits);
			prev_numBits.write(curr_numBits);
		}
		forceMpp = 0;
		break;
	case 3 :
		qLevel_tmp = qLevel_tmp + 1;
		forceMpp = 0;
		break;
	case 4 :
		qLevel_tmp = qLevel_tmp + 1;
		forceMpp = 0;
		break;
	case 5 :
		qLevel_tmp = qLevel_tmp + 1;
		forceMpp = 0;
		//qLevel.write(qLevel_tmp);
		break;
	default :
		forceMpp = 0;
		break;
	}
}
void RC_functions::RefreshBufferFullness()
{
	int curr_state;
	sc_int<8> sample_tmp;
	curr_state = state.read();
	// reset functionality
	if (rst_n.read() == false) {
	}

	// cycle behavior could be as well a case statement
	switch (curr_state) {
	case 0:
		break;
	case 1:
		break;
	case 2:
		if (completeVlc) {
			cout << "complete VLC " << endl;

		}
		break;
	case 3:
		codedGroupSize = prev_numBits - curr_numBits;
		bufferFullness = bufferFullness - (prev_numBits - curr_numBits);
		break;
	case 4:

		break;
	case 5:
		break;
	default:
		break;
	}
}
void RC_functions::calculate()
{
	int prev_pixel;
	int curr_state;
	unity_scale = (1 << RC_SCALE_BINARY_POINT);
	curr_state = state.read();
	if (rst_n.read() == false) {
		output_ready.write(0);
	}
	// cycle behavior could be as well a case statement
	switch (curr_state) {
	case 0:
		output_ready.write(0);
		break;
	case 1:
		break;
	case 2:
		if (completeVlc) {
			if (group_count == 0) {
				currentScale = initial_scale_value;
				scaleAdjustCounter = 1;
			}
			else if ((vPos == 0) && (currentScale > unity_scale))  // Reduce scale at beginning of slice
			{
				scaleAdjustCounter++;
				if (scaleAdjustCounter >= scale_decrement_interval)
				{
					scaleAdjustCounter = 0;
					currentScale--;
				}
			}
			else if (scaleIncrementStart)
			{
				scaleAdjustCounter++;
				if (scaleAdjustCounter >= scale_increment_interval)
				{
					scaleAdjustCounter = 0;
					currentScale++;
				}
			}

			if (vPos == 0)
			{
				current_bpg_target=first_line_bpg_ofs;
				increment = -(first_line_bpg_ofs << OFFSET_FRACTIONAL_BITS); // -4096
			}
			else {
				current_bpg_target = -(nfl_bpg_offset >> OFFSET_FRACTIONAL_BITS);
				increment = current_bpg_target;
			}
			if (pixelCount < initial_xmit_delay)
			{

				if (pixelCount == 0) {
					num_pixels = PIXELS_PER_GROUP;
				}
				else {
					num_pixels = pixelCount - prevPixelCount; // 1
				}

				num_pixels = MIN(initial_xmit_delay - pixelCount, num_pixels); //1
				next_trigger();
				increment  -= (bits_per_pixel * num_pixels) << (OFFSET_FRACTIONAL_BITS - 4); // -4096 - (128 << 7);

			}
			else
			{
				if (scale_increment_interval && !scaleIncrementStart && (vPos > 0) && (rcXformOffset > 0))
				{
					currentScale = 9;
					scaleAdjustCounter = 0;
					scaleIncrementStart = 1;
				}
			}


			output_ready=1;
		}
		break;
	case 3:
		//throttleFrac += increment;
		
		output_ready.write(0);

		break;
	case 4:
		output_ready.write(0);

		break;
	case 5:
		output_ready.write(0);

		break;
	default:
		output_ready.write(0);

		break;
	}
}

void RC_functions::LinearTrans_Initial()
{
	int curr_state;
	curr_state = state.read();
	if (rst_n.read() == false) {
		scale = 0;
		throttleFrac = 0;
		throttle_offset = 0;
	}
	switch (curr_state) {
	case 0:
		break;
	case 1:
		break;
	case 2:
		break;
	case 3:
		cout << "Linear Transfrom initial state" << endl;
		current_bpg_target -= slice_bpg_offset >> OFFSET_FRACTIONAL_BITS;
		increment = increment + slice_bpg_offset;
		throttleFrac = throttleFrac+increment;
		rcXformOffset = rcXformOffset + throttleFrac >> OFFSET_FRACTIONAL_BITS;
		throttleFrac = throttleFrac & ((1 << OFFSET_FRACTIONAL_BITS) - 1);
		prevPixelCount = pixelCount;
		scale.write(currentScale);
		throttle_offset = rcXformOffset;
		throttle_offset = throttle_offset - rc_model_size;
		break;
	case 4:
		break;
	case 5:
		break;
	default:
		scale = 0;
		break;
	}
}

void RC_functions::LinearTrans()
{
	int curr_state;
	curr_state = state.read();
	if (rst_n.read() == false) {
		rcModelBufferFullness = 0;
	}
	// cycle behavior could be as well a case statement
	switch (curr_state) {
	case 0:
		break;
	case 1:
		break;
	case 2:
		break;
	case 3:
		bpg_offset.write(current_bpg_target);
		rcModelBufferFullness = (scale * (throttle_offset + bufferFullness)) >> RC_SCALE_BINARY_POINT;
		break;
	case 4:
		break;
	case 5:
		break;
	default:
		break;
	}
}



void RC_functions::LongTerm()
{
	int curr_state;
	bool flag;
	int stQp;
	int curQp;
	int bpg;
	int j = 14;

	curr_state = state.read();
	if (rst_n.read() == false) {
		curQp = 0;
		stQp = 0;
		min_QP = 0;
		max_QP = 0;
		selected_range = 0;
		curr_state = 0;
		range_ready.write(0);
	}
	// cycle behavior could be as well a case statement
	switch (curr_state) {
	case 0:
		i.write(0);
		range_ready.write(0);
		break;
	case 1:
		i.write(0);
		range_ready.write(0);
		break;
	case 2:
		i.write(0);
		range_ready.write(0);
		break;
	case 3:
		i.write(0);
		range_ready.write(0);
		break;
	case 4:
		int idx;
		flag = false;
		compare_idx_max = rc_buf_thresh[13] - rc_model_size;
		compare_idx_min = rc_buf_thresh[0] - rc_model_size;
		for (int j = 14; j > 0; --j) {
			i.write(j);
			overflowAvoid = (bufferFullness + throttle_offset > OVERFLOW_AVOID_THRESHOLD);
			//cout <<j<< ","<<rcModelBufferFullness <<": cacluate " << (rc_buf_thresh[j - 1] - rc_model_size) << endl;
			if ((rcModelBufferFullness > rc_buf_thresh[j - 1] - rc_model_size)) {
				idx = j;
				flag = true;
				break;
			}	
		}
		if (flag == false)
			idx = 0;
		selected_range = prevRage;
		prevRage=idx;
		min_QP = range_min_qp[selected_range];
		max_QP = range_max_qp[selected_range];
		bpg = bits_per_pixel * sampModCnt;
		bpg = (bpg + 8) >> 4;
		cout << "Range of prevQp is :" << prevRage << "  selected: "<<selected_range<<endl;
		range_ready.write(1);
		break;
	case 5:
		i.write(0);
		range_ready.write(0);
		break;
	default:
		i.write(0);
		range_ready.write(0);
		break;
	}
}
void RC_functions::ShortTerm_store()
{
	int curr_state;
	curr_state = state.read();
	if (rst_n.read() == false) {
	}
	// cycle behavior could be as well a case statement
	switch (curr_state) {
	case 0:
		break;
	case 1:
		break;
	case 2:
		break;
	case 3:
		prev2Qp = prevQp;
		break;
	case 4:
		prevQp = stQp;
		break;
	case 5:
		rcSizeGroupPrev = rcSizeGroup;
		break;
	default:
		break;
	}
}
void RC_functions::ShortTerm()
{
	int curr_state;
	curr_state = state.read();
	if (rst_n.read() == false) {
	}
	// cycle behavior could be as well a case statement
	switch (curr_state) {
	case 0:
		break;
	case 1:
		qp_ready = false;
		break;
	case 2:
		break;
	case 3:
		break;
	case 4:
		break;
	case 5:
		new_quant = stQp;
		qp_ready = true;
		break;
	default:
		qp_ready = false;
		break;
	}
}
void RC_functions::ShortTerm_initial()
{
	int curr_state;
	curr_state = state.read();
	if (rst_n.read() == false) {
	}
	// cycle behavior could be as well a case statement
	switch (curr_state) {
	case 0:
		break;
	case 1:
		break;
	case 2:
		break;
	case 3:
		break;
	case 4:
		break;
	case 5:
		incr_amount = (codedGroupSize - rcTgtBitsGroup) >> 1;
		rcTgtBitsGroup = MAX(0, range_bpg_offset[selected_range] + 4);
		tgtMinusOffset = MAX(0, rcTgtBitsGroup - rc_tgt_offset_lo);
		tgtPlusOffset = MAX(0, rcTgtBitsGroup + rc_tgt_offset_hi);
		if (rcSizeGroup == PIXELS_PER_GROUP)
		{
			stQp = MAX(min_QP / 2, prevQp - 1);
		}
		else if ((codedGroupSize < tgtMinusOffset) && (rcSizeGroup < tgtMinusOffset))
		{
			stQp = MAX(min_QP, (prevQp - 1));
		}
		// avoid increasing QP immediately after edge
		else if ((bufferFullness >= 64) && codedGroupSize > tgtPlusOffset)  // over budget - increase qp
		{
			curQp = MAX(prevQp, min_QP);
			if (prev2Qp == curQp)   // 2nd prev grp == prev grp 
			{
				if ((rcSizeGroup * 2) < (rcSizeGroupPrev*rc_edge_factor)) {
					stQp = MIN(max_QP, (curQp + incr_amount));
				}
				else {
					stQp = curQp;
				}
			}
			else if (prev2Qp < curQp)
			{
				if (((rcSizeGroup * 2) < (rcSizeGroupPrev*rc_edge_factor) && (curQp < rc_quant_incr_limit0))) {
					stQp = MIN(max_QP, (curQp + incr_amount));
				}
				else {
					stQp = curQp;
				}
			}
			else if (curQp < rc_quant_incr_limit1)
			{
				stQp = MIN(max_QP, (curQp + incr_amount));
			}
			else
			{
				stQp = curQp;
			}
		}
		else {
			stQp = prevQp;
		}
		if (overflowAvoid) {
			stQp = range_max_qp[NUM_BUF_RANGES - 1];
			cout << "overflowAvoid value : " << overflowAvoid << endl;
		}
		cout << "Result. stQp value : " << stQp << endl;
		cout << "Result. rcTgtBitsGroup " << rcTgtBitsGroup << endl;
		break;
	default:
		break;
	}
}


void RC_functions::Initial()
{
	int i;
	int default_threshold[] = { 896, 1792, 2688, 3584, 4480, 5376, 6272, 6720, 7168, 7616, 7744, 7872, 8000, 8064 }; // -7296 ~ ~128
	//rcModelSize = 920; //812
	int default_minqp[] = { 0, 0, 1, 1, 3, 3, 3, 3, 3, 3, 5, 5, 5, 7, 13 };
	int default_maxqp[] = { 4, 4, 5, 6, 7, 7, 7, 8, 9, 10, 11, 12, 13, 13, 15 };
	int default_rcofs[] = { 2, 0, 0, -2, -4, -6, -8, -8, -8, -10, -10, -12, -12, -12, -12 };


	rcTgtBitsGroup=0;
	tgtMinusOffset=0;
	tgtPlusOffset=0;
	incr_amount = 0;
	stQp = 0;
	rcSizeGroup = 0;
	throttleFrac = 0;
	currentScale = 0;
	scaleAdjustCounter = 0;
	scaleIncrementStart = false;
	rcXformOffset = initial_offset;
	increment = 0;
	current_bpg_target = 0;
	for (i = 0; i<15; ++i)
	{
		range_bpg_offset[i] = default_rcofs[i];
		range_min_qp[i] = default_minqp[i];
		range_max_qp[i] = default_maxqp[i];
		if (i<14)
			rc_buf_thresh[i] = default_threshold[i];
	}


}