#include "systemc.h"
#include "../header/codec_main.h"

void codec_main::initial()
{
	for (int i = 0; i<MAX_UNITS_PER_GROUP; i++) {
		rcSizeUnit[i] = 1;
	}
	groups_total = groupsPerLine*slice_height;
	wait();
	first_line_bpg_ofs.write(firstLineBpgOfs);
	groups_per_line.write(groupsPerLine);
	initial_scale_value = 8 * rcModelSize / (rcModelSize - initial_offset);
	target_bpp_x16 = (int)(bitsPerPixel * 16 + 0.5);
	slice_bpg_offset= (int)ceil((double)(1 << OFFSET_FRACTIONAL_BITS) *(rcModelSize - initial_offset + num_extra_mux_bits) / (groups_total));
	wait();
	if (first_line_bpg_ofs < 0)
	{
		if (slice_height >= 8) {
			first_line_bpg_ofs = 12 * ((int)(0.09 * MIN(34, slice_height - 8)));
		}
		else {
			first_line_bpg_ofs = 2 * (slice_height - 1); //10

		}
		
	}
	bits_per_pixel = target_bpp_x16;
	wait();
	if (slice_height > 1)
		nfl_bpg_offset = (int)ceil((double)(first_line_bpg_ofs << OFFSET_FRACTIONAL_BITS) / (slice_height - 1));
	else
		nfl_bpg_offset = 0;
	scale_decrement_interval = groups_per_line / (initial_scale_value - 8);
}