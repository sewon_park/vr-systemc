#ifndef _IMAGE_H
#define _IMAGE_H
#include<string>
//#include"rgbslice.h"
#include<cstdio>
#include<stdio.h>
#include<iostream>
#include <math.h> 
#include <stdlib.h>
#include <string.h>
#include<systemc.h>

//using namespace std;

struct image : sc_module {
	sc_in_clk clk;
	sc_in<char*> fname_in;
	sc_in<bool> img_en;
	sc_out<int> w_out;
	sc_out<int> h_out;
//	sc_out<int**>rout;
//	sc_out<int**>gout;
//	sc_out<int**>bout;
	sc_out<unsigned int*>rs;
	sc_out<unsigned int*>gs;
	sc_out<unsigned int*>bs;
	sc_out<bool>rgb_en;
	sc_in<bool>stop;
	int hh = 0;
	int ii = 0;
	int send_en = 0;
	int counter = 0;
	unsigned int** r;
	unsigned int** g;
	unsigned int** b;
	bool start;
	bool finish;
	FILE *fp=NULL;
	
	void gettoken(FILE *fp, char *token, char*line, int *pos);
	

	void readppm();
	enum STATE
	{
		IDLE,
		CAL,
		WAIT
	};
	STATE state = IDLE;
	void statemch()
	{
		switch (state)
		{
		case image::IDLE:
			if (img_en.read() == 1)
			{
				start = 1;
				readppm();
				state = CAL;

			}
			break;
		case image::CAL:
			if (stop.read() == 1)
			{
				state = WAIT;
			}
			else
			{
				readppm();
				if (finish == 1)
				{
					start = 0;
					finish = 0;
					counter = 0;
					state = IDLE;
				}
			}

			break;
		case image::WAIT:
			if (stop.read() != 1)
			{
				readppm();
				if (finish == 1)
				{
					start = 0;
					finish = 0;
					counter = 0;
					state = IDLE;
				}
				else 
				state = CAL;

			}
		default:
			break;
		}
	}
	SC_CTOR(image) {
		
		
		SC_METHOD(statemch); // declare converter as SC_METHOD and 
		dont_initialize();
		sensitive << clk.pos();	   // make it sensitive to Img_en
	}



};












#endif
