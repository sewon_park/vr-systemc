#include "image.h"
#include<systemc.h>

void image::gettoken(FILE *fp, char *token, char*line, int *pos)
{
	char c;
	int count = 0;

	c = line[(*pos)++];
	while ((c == '\0') || (c == ' ') || (c == '\t') || (c == 10) || (c == 13) || (c == '#'))
	{
		if (c == '\0' || c == '\n' || c == '#')
		{
			fgets(line, 1000, fp);
			*pos = 0;
		}
		c = line[(*pos)++];
	}
	while (count < 999 && !((c == '\0') || (c == ' ') || (c == '\t') || (c == 10) || (c == 13) || (c == '#')))
	{
		token[count++] = c;
		c = line[(*pos)++];
	}
	token[count] = '\0';
}
void image::readppm()
{
	char magicnum[128];
	char line[1000];
	char token[1000];

	int bits;
	int maxval;
	int pos = 0;

	int i, j;
	int gray;


	char* fname;


	int w;
	int h;


	if (send_en == 0)
	{
		//fname = "brian_kernighan.ppm";

		
		
			r = new unsigned int*[513];
			g = new unsigned int*[513];
			b = new unsigned int*[513];
			for (int q = 0; q < 513; q++) {
				r[q] = new unsigned int[513];
				g[q] = new unsigned int[513];
				b[q] = new unsigned int[513];
			}

			if (start == 1)
			{


				if (fp == NULL)
				{
					fname = fname_in.read();
					fp = fopen(fname, "rb");



					fgets(line, 1000, fp);
					//	cout << line << endl;

					gettoken(fp, token, line, &pos);
					if (token[0] != 'P')
					{
						cout << "Incorrect file type." << endl;

					}
					strcpy(magicnum, token);
					//	cout << magicnum;
					gettoken(fp, token, line, &pos);
					w = atoi(token);
					w_out.write(w);
					gettoken(fp, token, line, &pos);
					h = atoi(token);
					h_out.write(h);
					gettoken(fp, token, line, &pos);
					maxval = atoi(token);
					//	cout << maxval;
					//	cout << w << "'" << h << endl;
					//	cout << maxval << endl;

					if (maxval <= 255)  bits = 8;
					else cout << "PPM read error" << endl;
					//	cout << bits;
					//	cout << magicnum[1];
					if (magicnum[1] == '2')
						for (i = 0; i < h; i++)
							for (j = 0; j < w; j++)
							{
								fscanf(fp, "&d", &gray);
								r[i][j] = gray;
								g[i][j] = gray;
								b[i][j] = gray;

							}
					else if (magicnum[1] == '3')
					{
						int c, v;
						i = 0; j = 0; c = 0;
						while (i < h)
						{
							char *rest;
							do
							{
								int sz = -1;
								do
								{
									sz++;
									line[sz] = fgetc(fp);
									if (feof(fp))
										line[++sz] = '\n';
								} while ((line[sz] != '\n') && ((sz < 900) || ((line[sz] >= '0') && (line[sz] <= '9'))));
								line[sz + 1] = '\0';
							} while (line[0] == '#');
							rest = line;
							while ((rest[0] != '\0') && ((rest[0] < '0') || (rest[0] > '9'))) rest++;
							while (rest[0] != '\0')
							{
								v = 0;
								while ((rest[0] >= '0') && (rest[0] <= '9'))
								{
									v = 10 * v + (rest[0] - '0');
									rest++;
								}
								if (c == 0)      r[i][j] = v;
								else if (c == 1) g[i][j] = v;
								else if (c == 2) b[i][j] = v;
								c++;
								if (c > 2)
								{
									c = 0; j++;
									if (j >= w)
									{
										j = 0; i++;
									}
								}
								while ((rest[0] != '\0') && ((rest[0] < '0') || (rest[0] > '9'))) rest++;
							}
						}
					}
					else if (magicnum[1] == '5') // PGM binary
						for (i = 0; i < h; i++)
							for (j = 0; j < w; j++)
							{
								gray = (unsigned char)fgetc(fp);  // Gray value
								if (maxval > 255)
									gray = (gray << 8) + (unsigned char)fgetc(fp);
								r[i][j] = gray;
								g[i][j] = gray;
								b[i][j] = gray;
							}
					//	cout << magicnum[1]<<" "<<h<<" "<<w<<endl;

					else
					{
						for (int i = 0; i < 3; i++)
						{
							for (int j = 0; j < 512; j++)
							{
								r[i][j] = (unsigned char)fgetc(fp);

								g[i][j] = (unsigned char)fgetc(fp);

								b[i][j] = (unsigned char)fgetc(fp);

							}
							r[i][512] = 0;
							g[i][512] = 0;
							b[i][512] = 0;

						}
					}
				}
				else
				{
					if (counter == 170)
					{
						for (int i = 0; i < 2; i++)
						{
							for (int j = 0; j < 512; j++)
							{
								r[i][j] = (unsigned char)fgetc(fp);

								g[i][j] = (unsigned char)fgetc(fp);

								b[i][j] = (unsigned char)fgetc(fp);
							}
							r[i][512] = 0;
							g[i][512] = 0;
							b[i][512] = 0;
						}
						for (int j = 0; j < 513; j++)
						{
							r[2][j] = 0;

						}

					}
					else
					{
						for (int i = 0; i < 3; i++)
						{
							for (int j = 0; j < 512; j++)
							{
								r[i][j] = (unsigned char)fgetc(fp);

								g[i][j] = (unsigned char)fgetc(fp);

								b[i][j] = (unsigned char)fgetc(fp);

							}
							r[i][512] = 0;
							g[i][512] = 0;
							b[i][512] = 0;

						}
					}
				}

				send_en = 1;
			}
			else
			{
				rgb_en.write(0);
				rs.write(NULL);
				gs.write(NULL);
				bs.write(NULL);
				send_en = 0;
			}
		
	}
	if (send_en == 1)

	{
		unsigned int *rr = new unsigned int[3];
		unsigned int *gg = new unsigned int[3];
		unsigned int *bb = new unsigned int[3];
		rr[0] = r[ii][hh];
		rr[1] = r[ii][hh + 1];
		rr[2] = r[ii][hh + 2];
		gg[0] = g[ii][hh];
		gg[1] = g[ii][hh + 1];
		gg[2] = g[ii][hh + 2];
		bb[0] = b[ii][hh];
		bb[1] = b[ii][hh + 1];
		bb[2] = b[ii][hh + 2];

		rgb_en.write(1);
		rs.write(rr);
		gs.write(gg);
		bs.write(bb);

		ii++;
		if (ii == 3)
		{
			if (hh == 510)
			{
				hh = 0;
				ii = 0;
				counter++;
				send_en = 0;
			}
			else
			{
				ii = 0;
				hh = hh + 3;
			}
			//	cout << "r_in:" << rr[0] << " " << rr[1] << " " << rr[2] << endl;

		}
		if (counter == 171)
		{
			if (fp == NULL)
			{
				cout << "Err" << endl;
			}
			else
			{
				fclose(fp);
				rgb_en.write(0);
				rs.write(NULL);
				gs.write(NULL);
				bs.write(NULL);
				fp = NULL;
				finish = 1;
			}
		}


	}






	//			for (int ii = 0; ii < 513 * 513; ii++)

	//				cout << "("<<ii<<")"<<r_[ii] << endl;



}
