
#include<systemc.h>
#ifndef BUFFER_H
#define BUFFER_H


struct buffer : sc_module {
	sc_in<bool> en;
	sc_in_clk clk;
	sc_in<unsigned int*> ycocg_in;
	sc_in<bool> sig_en;

	
	sc_out<unsigned int*>line_out;
	sc_out<bool> line_en;
	sc_out<bool>stop;
	bool sig_in = 0;
	int k = 0;
	int l = 1;
	int jj = 0;
	int pix_num = 0;

	int p = 0;
	unsigned int* in = new unsigned int[3];
	unsigned int* out = new unsigned int[9];
	unsigned int *buffer_ = new unsigned int[9*5];
	bool buffer_full = 0;

	void buff();
				  //Constructor
	SC_CTOR(buffer) {
		
		SC_METHOD(buff);

		// declare converter as SC_METHOD and 
		dont_initialize();
		sensitive << clk.pos() ;	   // make it sensitive to positive clock edge
	}

};

#endif