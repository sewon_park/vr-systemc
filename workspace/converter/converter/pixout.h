#ifndef _PIXOUT_H
#define _PIXOUT_H
#include<string>
//#include"rgbslice.h"
#include<cstdio>
#include<stdio.h>
#include<iostream>
#include <math.h> 
#include <stdlib.h>
#include <string.h>
#include<systemc.h>

//using namespace std;


struct line2pix : sc_module {
	sc_in<bool> line_en;
	sc_in<unsigned int*>line_in;
	sc_in_clk clk;
	sc_out<bool> sig_en;
	sc_out <sc_uint<26>>pixel_out;
	sc_out<bool> pixel_en;
	unsigned int* in = new unsigned int[9];
	int p = 0;
	bool send_en = 0;


void pixout()
	{
		if (send_en == 0)
		{
			pixel_en.write(0);
			sig_en.write(1);
			if (line_en.read() == 1)
			{
				sig_en.write(0);
				in = line_in.read();
				send_en = 1;

			}
		}
		if (send_en == 1)
		{
			pixel_en.write(1);
			
			pixel_out.write(in[p]);
			p++;
			if (p == 9)
			{
				p = 0;
				send_en = 0;
			}
		}


	}

	SC_CTOR(line2pix) {


		SC_METHOD(pixout); // declare converter as SC_METHOD and 
		dont_initialize();
		sensitive << clk.pos();
	//	sensitive << pixel_en;// make it sensitive to Img_en
	}



};












#endif
