#include "image.h"
#include <stdio.h>
#include <iostream>
#include <systemc.h>
#include<bitset>
#include "ycocg2rgb.h"
#include "rgb2ycocg.h"
#include "buffer.h"
#include "line2pixel.h"

using namespace std;

int sc_main(int, char*[])
{
	sc_signal<bool>img_en;
	sc_signal<char*> fname;
	sc_signal<int> w_in;
	sc_signal<int> h_in;

//   sc_signal<unsigned int**> r_in;
//	sc_signal<unsigned int**> g_in;
//	sc_signal<unsigned int**> b_in;
	sc_signal<unsigned int*> rs;
	sc_signal<unsigned int*> gs;
	sc_signal<unsigned int*> bs;
	sc_signal<bool>rgb_en;
	sc_clock clk;
	sc_signal<unsigned int*> ycocg;

	sc_signal<unsigned int*> r_out;
	sc_signal<unsigned int*> g_out;
	sc_signal<unsigned int*> b_out;
	sc_signal<bool>ycocg_en;
	sc_signal<bool> sig_en;

	sc_signal<unsigned int*>line_out;
	sc_signal<unsigned int*>ycocg_com;
	sc_signal<int>buffer_out_en;
//	sc_signal<int> yyy;
//	sc_signal<int>cooo;

//	sc_signal<int>cggg;

	sc_signal<bool>line_en;
	sc_signal<unsigned int*> pixel_out;
	sc_signal<bool>pixel_en;
	sc_signal<bool>ppm_en;
	sc_signal<bool>stop;
	fname = "mandrill.ppm";
	image IMG("image");
	IMG(clk,fname,img_en, w_in, h_in,rs,gs,bs,rgb_en,stop);
	
	convertrgb CVTRGB("convertrgb");
	CVTRGB(rs, gs, bs,rgb_en, clk, ycocg,ycocg_en,stop);
	
	buffer BUFF("buffer");
	BUFF(ycocg_en,clk, ycocg, sig_en,line_out,line_en,stop);
//	buffer_out BUFF_OUT("buffer_out");
//	BUFF_OUT(buffer_en, clk, ycocg_out,buffer_out_en, ycocg_com);

//	convertycocg CVTYCOCG("convertycocg");
//	CVTYCOCG(ycocg_com, buffer_out_en,clk, r_out, g_out, b_out);
	line2pixel LINE2PIXEL("line2pixel");
	LINE2PIXEL(line_en, clk, line_out, pixel_out, pixel_en,sig_en);
	
	convertycocg CVTYCOCG("convertycocg");
	CVTYCOCG(pixel_out, pixel_en, clk, ppm_en);
	//Initialize simulation
	img_en.write(1);
	sc_start(0, SC_NS);
//	cout << rs[0] << " " << gs[0] << " " << bs[0] << endl;




//	cout << r_in[3][3];

	img_en.write(0);
	for (int i = 0; i <999999; i++) {
		
		
		sc_start(1, SC_NS);
	//	sig_en.write(1);
		cout << "ppm_en:" << ppm_en << endl;
	//if(pixel_en.read()==1)
		//for(int j=0; j<3; j++)
			
			//cout << pixel_out[j] << endl;
		
		cout << " pixel_enable:" << pixel_en << endl;
		if (ppm_en.read() == 1)
			break;
		
		
		
		

	}
	
		
	
	return 0;
}