#include "systemc.h"
#include "rgb.h"
#include <stdio.h>
#include <iostream>
#include "image.h"
int sc_main(int, char *[])
{
	//Signals
	sc_signal<int> in_r;
	sc_signal<int> in_g;
	sc_signal<int> in_b;
	sc_signal<int> out_y;
	sc_signal<int> out_co;
	sc_signal<int> out_cg;
	


	


	//Clock
	sc_signal<bool>   clk;

	rgb RGB("rgb");               //instance of `numgen' module
	RGB(in_r, in_g, in_b, clk, out_y, out_co, out_cg);                //Positional port binding
	

	

	in_r = 16;
	in_g = 15;
	in_b = 5;



	
	


	
	
	sc_start(0, SC_NS);               //Initialize simulation
	for (int i = 0; i < 50; i++) {
		clk.write(1);
		sc_start(10, SC_NS);
		clk.write(0);
		sc_start(10, SC_NS);
	
	}

	cout << out_y << ',' << out_co << ',' << out_cg;
	
	return 0;
}