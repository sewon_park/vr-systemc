#include "ycocg2rgb.h"
#include<systemc.h>
void convertycocg::rgbout()
{

	if (start == 1 && ycocg_en.read()==1)
	{
		ycocg_ = ycocg.read();
		for (int l = 0; l < 3; l++)
		{
			cout << ycocg_[l] << endl;
			y = sc_uint<26>(ycocg_[l]).range(25, 18);
			co = sc_uint<26>(ycocg_[l]).range(17, 9);
			cg = sc_uint<26>(ycocg_[l]).range(8, 0);

			int cscco = co - (1 << bits_per_component);
			int csccg = cg - (1 << bits_per_component);
			int t = y - (csccg >> 1);
			int cscg = csccg + t;
			int cscb = t - (cscco >> 1);
			int cscr = cscco + cscb;

			unsigned int r_out;
			if (cscr < 0) r_out = 0;
			else if (cscr > 255) r_out = 255;
			else r_out = cscr;
			r_[i] = r_out;
			unsigned int g_out;
			if (cscg < 0) g_out = 0;
			else if (cscg > 255) g_out = 255;
			else g_out = cscg;
			g_[i] = g_out;
			unsigned int b_out;
			if (cscb < 0) b_out = 0;
			else if (cscb > 255) b_out = 255;
			else b_out = cscb;
			b_[i] = b_out;



			//		cout << "rgb_out:"<<r_[i] << " " << g_[i] << " " << b_[i] << endl;
			i++;
		}
		cout << "i:" << i << endl;
		if (i >= 3 * 513)
		{
			cnt++;
			if (cnt == 171)
			{
				finish = 1;
				writeppm();
				ppm_en.write(1);
				i = 0;
			}
			else
			{
				writeppm();
				ppm_en.write(0);
				i = 0;
			}
		

		}
		
	}
	



}
void convertycocg::writeppm()
{

	fname = "ppm_out_3.ppm";
	if (fp == NULL)
	{
		fp = fopen(fname, "wb");
		if (fp == NULL)
			cout << "Err///" << endl; // Error condition
		int i, j;
		int bits = 8;
		fprintf(fp, "P6\n");
		fprintf(fp, "%d %d\n", 512, 512);
		if (bits == 8)
			fprintf(fp, "255\n");


		else
		{
			printf("Unsupported bit depth for PPM output\n");
			exit(1);
		}

		//	for (i = 0; i < 512; i++)
		//		for (j = 0; j < 512; j++)
		//		{

		//			fputc((unsigned char)r[i][j] & 0xff, fp);

		//			fputc((unsigned char)g[i][j] & 0xff, fp);

		//			fputc((unsigned char)b[i][j] & 0xff, fp);
		//		}

	
			for (jj = 0; jj <  7; jj = jj + 3)
			{
				for (h = jj; h < jj + 1531; h = h + 9)
				{
					for (i = h; i < h + 3; i++)
					{
						if (h == jj + 1530 && i == h + 2) break;
						fputc((unsigned char)r_[i] & 0xff, fp);
						fputc((unsigned char)g_[i] & 0xff, fp);
						fputc((unsigned char)b_[i] & 0xff, fp);
					}
				}
			}
		
	}
	else
	{
		
			for (jj = 0; jj < 7; jj = jj + 3)
			{
				if (finish==1 && jj == 6) break;
				for (h = jj; h < jj + 1531; h = h + 9)
				{
					for (i = h; i < h + 3; i++)
					{
						if (h == jj + 1530 && i == h + 2) break;
						fputc((unsigned char)r_[i] & 0xff, fp);
						fputc((unsigned char)g_[i] & 0xff, fp);
						fputc((unsigned char)b_[i] & 0xff, fp);
					}
				}
			}
		
	}
	if (finish == 1)
	{
		fclose(fp);
		fp = NULL;
	}
}
