#include<systemc.h>
#ifndef CONVERTRGB_H
#define CONVERTRGB_H


struct convertrgb : sc_module {
	sc_in<unsigned int*> r;
	sc_in<unsigned int*> g;
	sc_in<unsigned int*> b;
	sc_in<bool> rgb_en;
	sc_in_clk clk;
	sc_out<unsigned int*> ycocg;
	sc_out<bool> en;
	sc_in<bool>stop;
//	sc_out<unsigned int*>yy;
//	sc_out<unsigned int*>coo;
//	sc_out<unsigned int*>cgg;
//	sc_out<int>yyy;
//	sc_out<int>cooo;
//	sc_out<int>cggg;
	
	unsigned int * rr = new unsigned int[3];

	unsigned int * gg = new unsigned int[3];

	unsigned int * bb = new unsigned int[3];
	unsigned int* ycocg_ = new unsigned int[3];
	unsigned int* y_ = new unsigned int[3];
	unsigned int* co_ = new unsigned int[3];
	unsigned int* cg_ = new unsigned int[3];
	int l = 0;

	void ycocgout();
	enum STATE
	{
		CAL,
		WAIT,
		
	};
	STATE state = CAL;
	void statemch()
	{
		switch (state)
		{
		case convertrgb::CAL:
			if (stop.read() == 1)
			{
				if (rgb_en.read() == 1)
				{
					ycocgout();
				}
				state = WAIT;
			}
			else
			{
				
					ycocgout();
					en.write(1);
					ycocg.write(ycocg_);
			
				
			}
			break;
		case convertrgb::WAIT:
			if (stop.read() != 1)
			{
				en.write(1);
				ycocg.write(ycocg_);
				state = CAL;
			}

			break;
		default:
			break;
		}
	}
					  //Constructor
	SC_CTOR(convertrgb) {
		SC_METHOD(statemch);
		
		// declare converter as SC_METHOD and 
		dont_initialize();
		sensitive << clk.pos();	   // make it sensitive to positive clock edge
	}

};

#endif