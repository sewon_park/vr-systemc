#include<systemc.h>
#ifndef CONVERTYCOCG_H
#define CONVERTYCOCG_H
#include <amp.h>
using namespace Concurrency::direct3d;


struct convertycocg : sc_module {
	sc_in<unsigned int*> ycocg;
	sc_in<bool> ycocg_en;
	sc_in_clk clk;
	sc_out<bool> ppm_en;

	//	sc_out<int>yyy;
	//	sc_out<int>cooo;
	//	sc_out<int>cggg;
	unsigned int*r_ = new unsigned int[3 * 513];
	unsigned int*g_ = new unsigned int[3 * 513];
	unsigned int*b_ = new unsigned int[3 * 513];
	int i = 0;
	int cnt = 0;
	
	unsigned int* ycocg_=new unsigned int[3];
	int h = 0;
	int jj = 0;

	sc_uint<8> y;
	sc_uint<9> co;
	sc_uint<9> cg;
	int bits_per_component = 8;
	bool  start = 0;
	bool finish = 0;
	char* fname;
	FILE *fp=NULL;
	void rgbout();
	void writeppm();

	enum STATE
	{
		IDLE,
		CAL
	};
	STATE state = IDLE;
	void statemch()
	{
		switch (state)
		{
		case convertycocg::IDLE:
			if (ycocg_en.read() == 1)
			{
				start = 1;
				rgbout();
				state = CAL;
			}
			break;
			
		case convertycocg::CAL:
			rgbout();
			if (finish == 1)
			{
				finish = 0;
				start = 0;
				i = 0;
				cnt = 0;
				state = IDLE;
				cout << "go IDLE" << endl;
			}
			break;
		default:
			break;
		}
	}
	SC_CTOR(convertycocg) {
		
		SC_METHOD(statemch);

		// declare converter as SC_METHOD and 
		dont_initialize();
		sensitive << clk.pos();	   // make it sensitive to positive clock edge
	}

};

#endif