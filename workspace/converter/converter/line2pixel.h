
#include<systemc.h>
#ifndef PIXEL_H
#define PIXEL_H


struct line2pixel : sc_module {
	sc_in<bool> en;
	sc_in_clk clk;
	sc_in<unsigned int*> ycocg_in;
	


	sc_out<unsigned int*> pixel_out;
	sc_out<bool> pixel_en;
	sc_out<bool> sig_en;
	int k = 0;
	int l = 0;
	int jj = 0;
	int o = 0;
	int p = 0;
	bool ready = 0;
	int h = 0;
	int first = 0;
	unsigned int* in = new unsigned int[9];
	unsigned int *buffer_ = new unsigned int[9];
	unsigned int* out=new unsigned int[3];
	void buff();
				  //Constructor
	SC_CTOR(line2pixel) {

		SC_METHOD(buff);

		// declare converter as SC_METHOD and 
		dont_initialize();
		sensitive << clk.pos();	   // make it sensitive to positive clock edge
	}

};

#endif